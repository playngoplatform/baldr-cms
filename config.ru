# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
# This file is used by Rack-based servers to start the application.

require_relative 'config/environment'

run Rails.application