# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_18_090401) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "email", default: ""
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "locale", default: "en", null: false
    t.string "timezone", default: "Europe/Stockholm", null: false
    t.index ["username"], name: "index_admin_users_on_username", unique: true
  end

  create_table "admin_users_roles", id: false, force: :cascade do |t|
    t.integer "admin_user_id"
    t.integer "role_id"
    t.index ["admin_user_id", "role_id"], name: "index_admin_users_roles_on_admin_user_id_and_role_id"
    t.index ["admin_user_id"], name: "index_admin_users_roles_on_admin_user_id"
    t.index ["role_id"], name: "index_admin_users_roles_on_role_id"
  end

  create_table "banners", force: :cascade do |t|
    t.integer "status", default: 1
    t.integer "action", default: 0
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer "size", default: 0
    t.string "object_id"
    t.index ["action"], name: "index_banners_on_action"
    t.index ["status"], name: "index_banners_on_status"
  end

  create_table "bonus_limits", force: :cascade do |t|
    t.integer "bonus_id"
    t.string "currency", null: false
    t.integer "min", null: false
    t.integer "max", default: 0
    t.index ["bonus_id"], name: "index_bonus_limits_on_bonus_id"
  end

  create_table "bonus_translations", force: :cascade do |t|
    t.integer "bonus_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.text "content"
    t.text "description"
    t.string "meta_title"
    t.string "meta_description"
    t.string "promo"
    t.index ["bonus_id"], name: "index_bonus_translations_on_bonus_id"
    t.index ["locale"], name: "index_bonus_translations_on_locale"
  end

  create_table "bonuses", force: :cascade do |t|
    t.integer "status", default: 0
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer "position", default: 0
    t.integer "mode", default: 0
    t.string "code", default: ""
    t.boolean "fixed_amount", default: false
    t.string "page_id", null: false
    t.boolean "registration", default: false
    t.index ["position"], name: "index_bonuses_on_position"
  end

  create_table "carousels", force: :cascade do |t|
    t.boolean "live", default: false
    t.integer "action", default: 0
    t.string "object_id", null: false
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer "category_id"
    t.integer "mode", default: 0
    t.index ["action"], name: "index_carousels_on_action"
    t.index ["category_id"], name: "index_carousels_on_category_id"
    t.index ["live"], name: "index_carousels_on_live"
    t.index ["object_id"], name: "index_carousels_on_object_id"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "data_fingerprint"
    t.string "type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "email_template_translations", force: :cascade do |t|
    t.integer "email_template_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "subject"
    t.text "body"
    t.index ["email_template_id"], name: "index_email_template_translations_on_email_template_id"
    t.index ["locale"], name: "index_email_template_translations_on_locale"
  end

  create_table "email_templates", force: :cascade do |t|
    t.string "action", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["action"], name: "index_email_templates_on_action"
  end

  create_table "endorphina_games", force: :cascade do |t|
    t.string "token", null: false
    t.string "player", null: false
    t.integer "player_id", null: false
    t.string "currency", null: false
    t.string "game_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "transaction_id"
    t.index ["player"], name: "index_endorphina_games_on_player"
    t.index ["player_id"], name: "index_endorphina_games_on_player_id"
    t.index ["token"], name: "index_endorphina_games_on_token"
  end

  create_table "endorphina_rounds", force: :cascade do |t|
    t.integer "user_id"
    t.integer "round_id"
    t.bigint "gia_round_id", default: 100
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "transaction_id", null: false
    t.index ["round_id"], name: "index_endorphina_rounds_on_round_id"
    t.index ["user_id"], name: "index_endorphina_rounds_on_user_id"
  end

  create_table "exchange_rates", force: :cascade do |t|
    t.string "from", null: false
    t.string "to", null: false
    t.float "rate", limit: 24, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["from"], name: "index_exchange_rates_on_from"
    t.index ["to"], name: "index_exchange_rates_on_to"
  end

  create_table "game_categories", force: :cascade do |t|
    t.integer "position", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code", null: false
    t.index ["position"], name: "index_game_categories_on_position"
  end

  create_table "game_category_translations", force: :cascade do |t|
    t.integer "game_category_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["game_category_id"], name: "index_game_category_translations_on_game_category_id"
    t.index ["locale"], name: "index_game_category_translations_on_locale"
  end

  create_table "game_providers", force: :cascade do |t|
    t.boolean "mode", default: true
    t.string "code", null: false
    t.string "name", null: false
    t.index ["code"], name: "index_game_providers_on_code"
    t.index ["mode"], name: "index_game_providers_on_mode"
  end

  create_table "game_translations", force: :cascade do |t|
    t.integer "game_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_alt"
    t.string "meta_title"
    t.string "meta_description"
    t.index ["game_id"], name: "index_game_translations_on_game_id"
    t.index ["locale"], name: "index_game_translations_on_locale"
  end

  create_table "games", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "popular", default: false
    t.integer "category_id", default: 3
    t.string "game_id", null: false
    t.boolean "is_desktop", default: true
    t.boolean "is_mobile", default: true
    t.integer "width", default: 1024, null: false
    t.integer "height", default: 768, null: false
    t.boolean "enabled", default: true
    t.boolean "locked", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "provider_id"
    t.boolean "old", default: false
    t.index ["category_id"], name: "index_games_on_category_id"
    t.index ["game_id"], name: "index_games_on_game_id"
    t.index ["name"], name: "index_games_on_name"
    t.index ["popular"], name: "index_games_on_popular"
    t.index ["provider_id"], name: "index_games_on_provider_id"
  end

  create_table "geo_locations", force: :cascade do |t|
    t.boolean "status"
    t.string "country_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hosts", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "enabled", default: true
    t.string "description"
    t.string "ancestry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ancestry"], name: "index_hosts_on_ancestry"
    t.index ["enabled"], name: "index_hosts_on_enabled"
    t.index ["name"], name: "index_hosts_on_name"
  end

  create_table "locale_translations", force: :cascade do |t|
    t.integer "locale_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "content"
    t.index ["locale"], name: "index_locale_translations_on_locale"
    t.index ["locale_id"], name: "index_locale_translations_on_locale_id"
  end

  create_table "locales", force: :cascade do |t|
    t.integer "mode", null: false
    t.index ["mode"], name: "index_locales_on_mode"
  end

  create_table "page_translations", force: :cascade do |t|
    t.integer "page_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.text "content"
    t.string "meta_title"
    t.string "meta_description"
    t.index ["locale"], name: "index_page_translations_on_locale"
    t.index ["page_id"], name: "index_page_translations_on_page_id"
  end

  create_table "pages", force: :cascade do |t|
    t.string "name", null: false
    t.integer "category", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_pages_on_name"
  end

  create_table "payments_deposits", force: :cascade do |t|
    t.string "transaction_id", null: false
    t.string "auth_token", null: false
    t.string "account_id", default: ""
    t.decimal "amount", precision: 8, scale: 2
    t.integer "user_id"
    t.string "currency"
    t.string "type"
    t.integer "state", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "fee", precision: 8, scale: 2
    t.decimal "paymentiq_amount", precision: 8, scale: 2
    t.string "login"
    t.integer "level_id"
    t.string "method_uuid", limit: 36, null: false
    t.json "extras"
    t.string "bonus_code"
    t.string "success_url", null: false
    t.string "failure_url", null: false
    t.index ["account_id"], name: "index_payments_deposits_on_account_id"
    t.index ["state"], name: "index_payments_deposits_on_state"
    t.index ["transaction_id"], name: "index_payments_deposits_on_transaction_id", unique: true
    t.index ["user_id"], name: "index_payments_deposits_on_user_id"
  end

  create_table "payments_limits", force: :cascade do |t|
    t.integer "method_id"
    t.boolean "status", default: false
    t.integer "level_id", null: false
    t.string "level_name", null: false
    t.string "currency", null: false
    t.integer "min", default: 0
    t.integer "max", default: 0
    t.integer "min_with_fee", default: 0
    t.decimal "fee", precision: 8, scale: 2, default: "0.0"
    t.index ["method_id"], name: "index_payments_limits_on_method_id"
  end

  create_table "payments_logs", force: :cascade do |t|
    t.integer "service", default: 0
    t.integer "method", default: 0
    t.json "data"
    t.string "url"
    t.string "logable_type"
    t.integer "logable_id"
    t.integer "player_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "state", default: 0
    t.index ["logable_type", "logable_id"], name: "index_payments_logs_on_logable_type_and_logable_id"
    t.index ["player_id"], name: "index_payments_logs_on_player_id"
  end

  create_table "payments_method_translations", force: :cascade do |t|
    t.integer "payments_method_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.text "deposit_first"
    t.text "deposit_second"
    t.text "description"
    t.text "withdrawal_to_one"
    t.text "withdrawal_to_many"
    t.string "account_required"
    t.index ["locale"], name: "index_payments_method_translations_on_locale"
    t.index ["payments_method_id"], name: "index_payments_method_translations_on_payments_method_id"
  end

  create_table "payments_methods", force: :cascade do |t|
    t.string "uuid", limit: 36, null: false
    t.string "paymentiq_name", null: false
    t.integer "paymentiq_provider_id", null: false
    t.boolean "status", default: false, null: false
    t.integer "mode", default: 0, null: false
    t.decimal "fee", precision: 8, scale: 2, default: "0.0"
    t.text "params"
    t.integer "position", default: 0
    t.boolean "request_account", default: false
    t.text "currency"
    t.string "class_name", null: false
    t.boolean "asynchronous", default: false
    t.index ["position"], name: "index_payments_methods_on_position"
    t.index ["uuid"], name: "index_payments_methods_on_uuid"
  end

  create_table "payments_withdrawals", force: :cascade do |t|
    t.string "transaction_id", null: false
    t.string "auth_token", null: false
    t.string "account_id", default: ""
    t.decimal "amount", precision: 8, scale: 2
    t.decimal "paymentiq_amount", precision: 8, scale: 2
    t.integer "user_id"
    t.string "login"
    t.string "method_uuid", limit: 36, null: false
    t.string "bonus_code", limit: 40
    t.json "extras"
    t.string "currency"
    t.string "type"
    t.integer "state", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_payments_withdrawals_on_account_id"
    t.index ["method_uuid"], name: "index_payments_withdrawals_on_method_uuid"
    t.index ["state"], name: "index_payments_withdrawals_on_state"
    t.index ["transaction_id"], name: "index_payments_withdrawals_on_transaction_id", unique: true
    t.index ["user_id"], name: "index_payments_withdrawals_on_user_id"
  end

  create_table "players", force: :cascade do |t|
    t.string "login", null: false
    t.string "nickname"
    t.integer "risk_level", default: 0, null: false
    t.text "note"
    t.integer "host_id"
    t.string "currency", null: false
    t.integer "level_id", default: 0
    t.decimal "total_balance", precision: 8, scale: 2
    t.decimal "bonus_balance", precision: 8, scale: 2
    t.decimal "withdrawable_balance", precision: 8, scale: 2
    t.decimal "bonus_wagered_fraction", precision: 8, scale: 2
    t.decimal "wagering_requirement", precision: 12, scale: 2
    t.boolean "protection_limit_set", default: false
    t.index ["host_id"], name: "index_players_on_host_id"
    t.index ["login"], name: "index_players_on_login"
    t.index ["risk_level"], name: "index_players_on_risk_level"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.integer "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "guid", null: false
    t.integer "player_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["player_id"], name: "index_sessions_on_player_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "var", null: false
    t.text "value"
    t.integer "thing_id"
    t.string "thing_type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true
  end

end
