# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
env_name = (Rails.env.production? || Rails.env.beta?) ? 'production' : 'stage'
files = Dir["db/fixtures/hosts/#{env_name}/*"]

ActiveRecord::Base.transaction do
  files.each do |file|
    seeds = HashWithIndifferentAccess.new(YAML::load_file(file))
    seeds[:hosts].each do |h|
      if h[:parent]
        parent = Host.find_by_name(h[:parent])
      else
        parent = nil
      end
      host = Host.find_or_initialize_by(name: h[:name])
      host.enabled = h[:enabled]
      host.description = h[:description]
      host.parent = parent
      host.save!
    end
  end
end