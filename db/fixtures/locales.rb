# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
def import_locale(file, lang)
  data = File.read(file)
  I18n.locale = lang
  ActiveRecord::Base.transaction do
    t = Locale.find_or_initialize_by(mode: :frontend)
    t.content = data
    t.save!
  end
end

locale_file = Rails.root.join('db', 'fixtures', 'locales', 'en.json')
import_locale(locale_file, :en)

locale_file = Rails.root.join('db', 'fixtures', 'locales', 'ru.json')
import_locale(locale_file, :ru)

locale_file = Rails.root.join('db', 'fixtures', 'locales', 'pl.json')
import_locale(locale_file, :pl)