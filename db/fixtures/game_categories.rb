# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
# Game Category
#

I18n.locale = :en

GameCategory.seed(:id,
                  { id: 3, code: 'last_played', name: 'Last played', position: 1 },
                  { id: 3, code: 'last_played', locale: 'ru', name: 'Прежние игры' },
                  { id: 20, code: 'favorite', name: 'Favorites', position: 10 },
                  { id: 20, code: 'favorite', locale: 'ru', name: 'Избранные' },
                  { id: 6, code: 'popular', name: 'Popular', position: 20 },
                  { id: 6, code: 'popular', locale: 'ru', name: 'Популярные' },
                  { id: 4, code: 'new_game', name: 'New games', position: 30 },
                  { id: 4, code: 'new_game', locale: 'ru', name: 'Новые игры' },
                  { id: 10, code: 'live', name: 'Live', position: 40 },
                  { id: 10, code: 'live', locale: 'ru', name: 'Live' },
                  { id: 8, code: 'slot', name: 'Slots', position: 50 },
                  { id: 8, code: 'slot', locale: 'ru', name: 'Слоты' },
                  { id: 7, code: 'roulette', name: 'Roulette', position: 60 },
                  { id: 7, code: 'roulette', locale: 'ru', name: 'Рулетка' },
                  { id: 1, code: 'black_jack', name: 'Black jack', position: 70 },
                  { id: 1, code: 'black_jack', locale: 'ru', name: 'Блэк Джек' },
                  { id: 2, code: 'classic', name: 'Classic', position: 80 },
                  { id: 2, code: 'classic', locale: 'ru', name: 'Классик' },
                  { id: 9, code: 'table_game', name: 'Table games', position: 90 },
                  { id: 9, code: 'table_game', locale: 'ru', name: 'Столы' },
                  { id: 5, code: 'other', name: 'Other', position: 100 },
                  id: 5, code: 'other', locale: 'ru', name: 'Другие'
                 )
