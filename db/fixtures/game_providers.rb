# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
Game.where(provider_id: nil).update_all(provider_id: 1)
