# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateGameCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :game_categories do |t|
      t.integer :position, default: 0, index: true
      t.timestamps
    end
  end
end
