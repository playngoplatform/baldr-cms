# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateBonus < ActiveRecord::Migration[5.2]
  def change
    create_table :bonuses do |t|
      t.integer  :status, default: 0
      t.datetime :start_date
      t.datetime :end_date
      t.integer  :position, default: 0, index: true
      t.integer  :mode, default: 0
      t.string   :code, default: ''
      t.boolean  :fixed_amount, default: false
      t.string   :page_id, null: false
    end

    reversible do |dir|
      dir.up do
        Bonus.create_translation_table! title: :string,
                                        content: :text,
                                        description: :text,
                                        meta_title: :string,
                                        meta_description: :string
      end

      dir.down do
        Bonus.drop_translation_table!
      end
    end
  end
end
