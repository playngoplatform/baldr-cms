# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class MigrateGameProviders < ActiveRecord::Migration[5.2]
  def up
    remove_column :games, :provider
    add_reference :games, :provider
  end

  def down
    remove_reference :games, :provider
    add_column :games, :provider, :integer, default: 0
  end
end
