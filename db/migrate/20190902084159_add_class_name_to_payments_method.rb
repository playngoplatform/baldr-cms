# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddClassNameToPaymentsMethod < ActiveRecord::Migration[5.2]
  def change
    add_column :payments_methods, :class_name, :string, null: false
  end
end
