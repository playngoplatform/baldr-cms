# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreatePaymentsLimits < ActiveRecord::Migration[5.2]
  def change
    create_table :payments_limits do |t|
      t.references :method
      t.boolean :status, default: false
      t.integer :level_id, null: false
      t.string :level_name, null: false
      t.string :currency, null: false
      t.integer :min
      t.integer :max
      t.integer :min_with_fee, default: 0
      t.decimal :fee, precision: 8, scale: 2, default: 0
    end
  end
end
