# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddDefaultToPaymentLimitMinAndMax < ActiveRecord::Migration[5.2]
  def change
    change_column_default :payments_limits, :min, 0
    change_column_default :payments_limits, :max, 0
  end
end
