# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class ChangeAuthTokenToPaymentsDeposit < ActiveRecord::Migration[5.2]
  def up
    remove_index :payments_deposits, :auth_token
  end

  def down
    add_index :payments_deposits, :auth_token, unique: true
  end
end
