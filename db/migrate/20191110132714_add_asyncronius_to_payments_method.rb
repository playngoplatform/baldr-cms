# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddAsyncroniusToPaymentsMethod < ActiveRecord::Migration[5.2]
  def change
    add_column :payments_methods, :asynchronous, :boolean, default: false
  end
end
