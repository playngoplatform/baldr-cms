# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class ChangeTransactionIdInEndorphinaGames < ActiveRecord::Migration[5.2]
  def up
    remove_column :endorphina_games, :transaction_id
    add_column :endorphina_games, :transaction_id, :string, allow_nil: true
  end

  def down
    remove_column :endorphina_games, :transaction_id
    add_column :endorphina_games, :transaction_id, :string, null: false
  end
end
