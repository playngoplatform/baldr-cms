# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddProtectionLimitSetToPlayers < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :protection_limit_set, :boolean, default: false
  end
end
