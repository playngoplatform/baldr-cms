# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateExchangeRates < ActiveRecord::Migration[5.2]
  def change
    create_table :exchange_rates do |t|
      t.string   :from, null: false, index: true
      t.string   :to, null: false, index: true
      t.float    :rate, limit: 24, null: false
      t.timestamps
    end
  end
end
