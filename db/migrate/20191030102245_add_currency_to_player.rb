# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddCurrencyToPlayer < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :currency, :string, null: false
  end
end
