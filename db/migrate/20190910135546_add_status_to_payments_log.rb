# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddStatusToPaymentsLog < ActiveRecord::Migration[5.2]
  def change
    add_column :payments_logs, :state, :integer, default: 0, index: true
  end
end
