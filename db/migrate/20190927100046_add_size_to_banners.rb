# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddSizeToBanners < ActiveRecord::Migration[5.2]
  def change
    add_column :banners, :size, :integer, default: 0, index: true
  end
end
