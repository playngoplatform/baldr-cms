# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateBonusLimits < ActiveRecord::Migration[5.2]
  def change
    create_table :bonus_limits do |t|
      t.references :bonus
      t.string  "currency",             null: false
      t.integer "min",                  null: false
      t.integer "max",      default: 0
    end
  end
end
