# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddBalancesToPlayer < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :amount_real, :decimal, precision: 8, scale: 2
    add_column :players, :amount_bonus, :decimal, precision: 8, scale: 2
  end
end
