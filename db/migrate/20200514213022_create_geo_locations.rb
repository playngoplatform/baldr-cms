# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateGeoLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :geo_locations do |t|
      t.boolean :status
      t.string :country_code

      t.timestamps
    end
  end
end
