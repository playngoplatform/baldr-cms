# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddLocaleTimeZoneToAdminUser < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_users, :locale, :string, null: false, default: 'en'
    add_column :admin_users, :timezone, :string, null: false, default: 'Europe/Stockholm'
  end
end
