# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddWagerBalanceToPlayer < ActiveRecord::Migration[5.2]
  def up
    remove_columns :players, :amount_real, :amount_bonus
    add_column :players, :total_balance, :decimal, precision: 8, scale: 2
    add_column :players, :bonus_balance, :decimal, precision: 8, scale: 2
    add_column :players, :withdrawable_balance, :decimal, precision: 8, scale: 2
    add_column :players, :wagering_requirement, :decimal, precision: 8, scale: 2
    add_column :players, :bonus_wagered_fraction, :decimal, precision: 8, scale: 2
  end

  def down
    remove_columns :players, :total_balance, :bonus_balance, :withdrawable_balance, :wagering_requirement, :bonus_wagered_fraction
    add_column :players, :amount_real, :decimal, precision: 8, scale: 2
    add_column :players, :amount_bonus, :decimal, precision: 8, scale: 2
  end
end