# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateHosts < ActiveRecord::Migration[5.2]
  def change
    create_table :hosts do |t|
      t.string :name, null: false, index: true
      t.boolean :enabled, default: true, index: true
      t.string :description
      t.string :ancestry, index: true
      t.timestamps
    end
  end
end
