# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateEndorphinaTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :endorphina_tokens do |t|
      t.string :token, null: false, index: true
      t.string :player, null: false, index: true
      t.integer :player_id, null: false, index: true
      t.string :currency, null: false
      t.string :game_id
      t.timestamps
    end
  end
end
