# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddUrlsToDeposit < ActiveRecord::Migration[5.2]
  def change
    add_column :payments_deposits, :success_url, :string, null: false
    add_column :payments_deposits, :failure_url, :string, null: false
  end
end
