# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateBanners < ActiveRecord::Migration[5.2]
  def change
    create_table :banners do |t|
      t.integer :status, default: 1, index: true
      t.integer :action, default: 0, index: true
      t.string :object_id, null: false, index: true
      t.datetime :start_date, null: true
      t.datetime :end_date, null: true

    end
  end
end
