# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateEndorphinaRounds < ActiveRecord::Migration[5.2]
  def change
    create_table :endorphina_rounds do |t|
      t.integer :user_id, index: true
      t.integer :round_id, index: true
      t.bigint :gia_round_id, default: 100
      t.timestamps
    end
  end
end
