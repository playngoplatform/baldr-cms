# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class GamesTranslations < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Game.create_translation_table! image_alt: :string,
                                       meta_title: :string,
                                       meta_description: :string
      end

      dir.down do
        Game.drop_translation_table!
      end
    end
  end
end
