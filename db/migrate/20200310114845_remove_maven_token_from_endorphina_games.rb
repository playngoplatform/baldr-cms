# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class RemoveMavenTokenFromEndorphinaGames < ActiveRecord::Migration[5.2]
  def change
    remove_column :endorphina_games, :maven_token
  end
end
