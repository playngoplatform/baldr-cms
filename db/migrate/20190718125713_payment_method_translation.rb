# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class PaymentMethodTranslation < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Payments::Method.create_translation_table! name: :string,
                                                   deposit_first: :text,
                                                   deposit_second: :text,
                                                   description: :text,
                                                   withdrawal_to_one: :text,
                                                   withdrawal_to_many: :text,
                                                   account_required: :string

      end

      dir.down do
        Payments::Method.drop_translation_table!
      end
    end
  end
end
