# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddRegistrationToBonus < ActiveRecord::Migration[5.2]
  def change
    add_column :bonuses, :registration, :boolean, default: false
  end
end
