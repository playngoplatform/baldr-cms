# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class TruncateGames < ActiveRecord::Migration[5.2]
  def up
    Game.destroy_all
  end
end
