# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class TruncateCarousels < ActiveRecord::Migration[5.2]
  def up
    Carousel.destroy_all
  end
end
