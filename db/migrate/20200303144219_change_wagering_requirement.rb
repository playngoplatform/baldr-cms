# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class ChangeWageringRequirement < ActiveRecord::Migration[5.2]
  def up
    remove_column :players, :wagering_requirement
    add_column :players, :wagering_requirement, :decimal, precision: 12, scale: 2
  end

  def down
    remove_column :players, :wagering_requirement
    add_column :players, :wagering_requirement, :decimal, precision: 8, scale: 2
  end  
end
