# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddExtraAttrsToPyemntDeposit < ActiveRecord::Migration[5.2]
  def change
    add_column :payments_deposits, :method_uuid, :string, null:false, index: true
    add_column :payments_deposits, :fee, :decimal, precision: 8, scale: 2
    add_column :payments_deposits, :paymentiq_amount, :decimal, precision: 8, scale: 2
    add_column :payments_deposits, :login, :string, index: true
    add_column :payments_deposits, :level_id, :integer
  end
end
