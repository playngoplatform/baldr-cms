# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddCodeToGameCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :game_categories, :code, :string, null: false, index: true
  end
end
