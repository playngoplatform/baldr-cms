# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.string :name, null: false, index: true
      t.boolean :popular, default: false, index: true
      t.integer :category, default: 3
      t.integer :provider, default: 0, index: true
      t.string :game_id, null: false, index: true
      t.boolean :is_desktop, default: true
      t.boolean :is_mobile, default: true
      t.integer :width, default: 1024, null: false
      t.integer :height, default: 768, null: false
      t.boolean :enabled, default: true
      t.boolean :locked, default: false
      t.timestamps
    end
  end
end
