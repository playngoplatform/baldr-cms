# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddOldToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :old, :boolean, default: false, index: true
  end
end
