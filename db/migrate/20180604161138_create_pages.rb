# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :name, null: false, index: true, uniq: true
      t.integer :category, default: 0
      t.timestamps
    end
    reversible do |dir|
      dir.up do
        Page.create_translation_table! title: :string,
                                       content: :text,
                                       meta_title: :string,
                                       meta_description: :string
      end

      dir.down do
        Page.drop_translation_table!
      end
    end
  end
end
