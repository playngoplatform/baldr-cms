# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreatePaymentsWithdrawals < ActiveRecord::Migration[5.2]
  def change
    create_table :payments_withdrawals do |t|
      t.string :transaction_id, null: false
      t.string :auth_token, null: false
      t.string :account_id, default: '', index: true
      t.decimal :amount, precision: 8, scale: 2
      t.decimal :paymentiq_amount, precision: 8, scale: 2
      t.references :user
      t.string :login
      t.string :method_uuid, limit: 36, null: false, index: true
      t.string :bonus_code, limit: 40
      t.json :extras
      t.string :currency
      t.string :type
      t.integer :state, default: 0, null: false, index: true
      t.timestamps
      t.index [ :transaction_id ], unique: true
    end
  end
end
