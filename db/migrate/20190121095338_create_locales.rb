# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateLocales < ActiveRecord::Migration[5.2]
  def change
    create_table :locales do |t|
      t.integer :mode, null: false, index: true
    end
    reversible do |dir|
      dir.up do
        Locale.create_translation_table! content: :text
      end

      dir.down do
        Locale.drop_translation_table!
      end
    end
  end
end
