# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddUserLevelToPlayer < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :level_id, :integer, default: 0
  end
end
