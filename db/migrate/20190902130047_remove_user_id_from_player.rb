# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class RemoveUserIdFromPlayer < ActiveRecord::Migration[5.2]
  def up
    remove_index :players, :user_id
    remove_column :players, :user_id
  end

  def down
    add_column :players, :user_id, :integer, null: false, index: true
  end
end
