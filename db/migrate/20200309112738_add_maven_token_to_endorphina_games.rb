# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddMavenTokenToEndorphinaGames < ActiveRecord::Migration[5.2]
  def change
    add_column :endorphina_games, :maven_token, :string, null: false, index: true
  end
end
