# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateGameProviders < ActiveRecord::Migration[5.2]
  def change
    create_table :game_providers do |t|
      t.boolean :mode, default: true, index: true
      t.string :code, null: false, index: true
      t.string :name, null: false
    end
  end
end
