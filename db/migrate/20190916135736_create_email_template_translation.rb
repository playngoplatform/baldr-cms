# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateEmailTemplateTranslation < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        EmailTemplate.create_translation_table! subject: :string, body: :text
      end

      dir.down do
        EmailTemplate.drop_translation_table!
      end
    end
  end
end
