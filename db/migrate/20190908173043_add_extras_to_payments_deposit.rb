# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddExtrasToPaymentsDeposit < ActiveRecord::Migration[5.2]
  def change
    add_column :payments_deposits, :extras, :json
  end
end
