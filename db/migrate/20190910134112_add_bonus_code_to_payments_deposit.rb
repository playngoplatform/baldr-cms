# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddBonusCodeToPaymentsDeposit < ActiveRecord::Migration[5.2]
  def change
    add_column :payments_deposits, :bonus_code, :string
  end
end
