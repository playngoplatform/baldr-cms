# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateEmailTemplates < ActiveRecord::Migration[5.2]
  def change
    create_table :email_templates do |t|
      t.string :action, null: false, index: true
      t.timestamps
    end
  end
end
