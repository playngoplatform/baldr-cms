# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddHostToPlayer < ActiveRecord::Migration[5.2]
  def change
    add_reference :players, :host
  end
end
