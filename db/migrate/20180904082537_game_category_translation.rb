# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class GameCategoryTranslation < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        GameCategory.create_translation_table! name: :string
      end

      dir.down do
        GameCategory.drop_translation_table!
      end
    end
  end
end
