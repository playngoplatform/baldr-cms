# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class RenameNameToObjectIdInCarousel < ActiveRecord::Migration[5.2]
  def change
    rename_column :carousels, :name, :object_id
  end
end
