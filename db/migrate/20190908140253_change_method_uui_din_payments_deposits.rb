# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class ChangeMethodUuiDinPaymentsDeposits < ActiveRecord::Migration[5.2]
  def up
    remove_column :payments_deposits, :method_uuid
    add_column :payments_deposits, :method_uuid, :string, limit: 36, null: false, index: true
  end

end
