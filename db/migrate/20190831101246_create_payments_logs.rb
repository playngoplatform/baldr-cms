# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreatePaymentsLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :payments_logs do |t|
      t.integer  :service, default: 0
      t.integer  :method, default: 0
      t.json :data
      t.string :url
      t.references :logable, polymorphic: true
      t.references :player
      t.timestamps
    end
  end
end
