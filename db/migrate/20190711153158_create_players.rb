# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.integer :user_id, null: false, index: true
      t.string :login, null: false, index: true
      t.string :nickname
      t.integer :risk_level, null: false, default: 0, index: true
      t.text :note
    end
  end
end
