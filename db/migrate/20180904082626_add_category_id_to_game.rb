# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddCategoryIdToGame < ActiveRecord::Migration[5.2]
  def change
    rename_column :games, :category, :category_id
    add_index :games, :category_id
  end

end
