# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreatePaymentsMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :payments_methods do |t|
      t.string :uuid, limit: 36, null: false, index: true
      t.string :paymentiq_name, null: false
      t.integer :paymentiq_provider_id, null: false
      t.boolean :status, null: false, default: false
      t.integer :mode, null: false, default: 0
      t.decimal :fee, precision: 8, scale: 2, default: 0
      t.text :params
      t.integer :position, default: 0, index: true
      t.boolean :request_account, default: false
    end
  end
end
