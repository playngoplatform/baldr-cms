# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class ChangeObjectIdinBanners < ActiveRecord::Migration[5.2]
  def up
    remove_column :banners, :object_id
    add_column :banners, :object_id, :string
  end

end
