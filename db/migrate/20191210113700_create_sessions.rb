# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.string :guid, null: false
      t.references :player
      t.timestamps
    end
  end
end
