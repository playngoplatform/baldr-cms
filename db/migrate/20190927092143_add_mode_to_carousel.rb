# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddModeToCarousel < ActiveRecord::Migration[5.2]
  def change
    add_column :carousels, :mode, :integer, default: 0, index: true
  end
end
