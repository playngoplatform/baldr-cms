# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CreateCarousels < ActiveRecord::Migration[5.2]
  def change
    create_table :carousels do |t|
      t.boolean :live, default: false, index: true
      t.integer :action, default: 0, index: true
      t.string :name, null: false, index: true
      t.datetime :start_date
      t.datetime :end_date
    end
  end
end
