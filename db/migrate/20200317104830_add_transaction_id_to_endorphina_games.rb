# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddTransactionIdToEndorphinaGames < ActiveRecord::Migration[5.2]
  def change
    add_column :endorphina_games, :transaction_id, :string, null: false
  end
end
