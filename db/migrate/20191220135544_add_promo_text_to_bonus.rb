# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AddPromoTextToBonus < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Bonus.add_translation_fields! promo: :string
      end

      dir.down do
        remove_column :bonus_translations, :promo
      end
    end
  end
end
