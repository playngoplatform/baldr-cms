# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class RenameEndorpinaTokens < ActiveRecord::Migration[5.2]
  def up
    rename_table :endorphina_tokens, :endorphina_games
  end

  def down
    rename_table :endorphina_games, :endorphina_tokens
  end
end
