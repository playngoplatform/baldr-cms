# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
# Roles

Role.create(name: 'admin')

# Admin User
admin_password = Rails.env.development? ? 'password' : Rails.application.credentials.dig(Rails.env.to_sym, :admin_password)

admin = AdminUser.create(
  username: 'admin',
  email: 'admin@example.com',
  password: admin_password,
  password_confirmation: admin_password,
  locale: 'en'
)
admin.add_role 'admin'
admin.save
