source 'https://rubygems.org'
ruby '2.6.4'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.2.4.1'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
#
gem 'uglifier', '>= 1.3.0'

gem 'bootsnap', '>= 1.1.0', require: false

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'hiredis'
gem 'redis'
gem 'redis-namespace'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails'
  gem 'rspec-rails', '~> 3.8'
  gem 'rspec_junit_formatter'
  gem 'shoulda-matchers'
  # Use Puma as the app server
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'puma', '~> 3.7'
end

group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'simplecov', require: false
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'

  gem 'bullet'
  gem 'debase'
  gem 'ruby-debug-ide'

  gem 'brakeman', require: false
  gem 'dotenv-rails'
  gem 'rubocop', '~> 0.74.0', require: false
end

# group :production do
#   gem 'passenger'
# end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'exception_notification'
gem 'slack-notifier'

gem 'whenever', require: false

# api
gem 'grape'
gem 'grape-swagger'
gem 'grape-swagger-entity'
gem 'grape-swagger-rails'
gem 'grape_logging'

gem 'globalize', '~> 5.3.0'
gem 'migration_data'
gem 'seed-fu', '~> 2.3'

gem 'activeadmin'
gem 'activeadmin-globalize', git: 'https://github.com/casino-2go/activeadmin-globalize.git', branch: 'develop'
gem 'activeadmin_addons'
gem 'activeadmin_settings_cached'
gem 'aws-sdk-s3'
gem 'cancancan', '~> 2.0'
gem 'ckeditor', '~> 4.3'
gem 'devise'
gem 'mini_magick'
gem 'rolify'
gem 'sassc-rails'

# Exchange rates
gem 'money-rails', '~>1.12'
gem 'russian_central_bank', git: 'https://github.com/casino-2go/russian_central_bank'

# logs
gem 'timber', '~> 3.0'
gem 'timber-rails', '~> 1.0'

gem 'aasm'
gem 'countries'
gem 'country_select', '~> 4.0'
gem 'rest-client', '~> 2.1'
gem 'savon', '~> 2.12.0'

gem 'activemerchant', git: 'https://github.com/casino-2go/active_merchant', branch: 'master'

# Email processing
gem 'griddler', git: 'https://github.com/thoughtbot/griddler'
gem 'griddler-ses', git: 'https://github.com/casino-2go/griddler-ses'
gem 'nokogiri'

gem 'ancestry'

# Local gems.
gem 'maven', path: './lib/player'
gem 'cashier', path: './lib/cashier'
gem 'nets', path: './lib/nets'
gem 'playngo', path: './lib/playngo'
gem 'thunderkick', path: './lib/thunderkick'
gem 'elk', path: './lib/elk'
gem 'netent', path: './lib/netent'
# gem 'nemid', path: './lib/nemid'
