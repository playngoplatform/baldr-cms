# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end