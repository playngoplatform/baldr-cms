# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
RSpec.configure do |config|
  config.include RSpec::Rails::RequestExampleGroup,
                 type: :request, file_path: %r{spec/api}
end
