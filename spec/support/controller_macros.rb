# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module ControllerMacros
  def login_as_admin
      before(:each) do
        @request.env["devise.mapping"] = Devise.mappings[:admin]
        sign_in FactoryBot.create(:admin)
      end
  end
end