# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :banner, class: Banner do
    action { 0 }
    trait :big do
      size { 10 }
    end
    trait :small do
      size { 0 }
    end
    trait :hugo do
      object_id { 'hugosadventure' }
    end
    trait :published do
      status { 0 }
    end
    trait :draft do
      status { 1 }
    end
  end
end
