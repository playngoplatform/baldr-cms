# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :admin, class: AdminUser do
    username { 'admin' }
    email { 'bill@microsoft.com' }
    password { '123456' }
    locale { 'ru' }
    timezone { 'Europe/Moscow' }
  end
  factory :admin2, class: AdminUser do
    username { 'admin2' }
    email { 'bill2@microsoft.com' }
    password { '123456' }
    locale { 'ru' }
    timezone { 'Europe/Moscow' }
  end

  factory :invalid_admin, class: AdminUser do
    username { 'admin3' }
    email { 'microsoft.com' }
    password { '1456' }
    locale { 'ru' }
    timezone { 'Europe/Moscow' }
  end
end