# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :rate_rub_to_usd, class: ExchangeRate do
    from { 'RUB' }
    to { 'USD' }
    rate { 0.0154161 }
  end
end