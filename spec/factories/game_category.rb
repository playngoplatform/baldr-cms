# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :game_category_slot, class: GameCategory do
    id { 1 }
    code { 'slot' }
    position { 50 }
    factory :game_category_slot_with_translations do
      after(:create) do |gc|
        gc.set_translations(
          ru: { name: 'Слоты' },
          en: { name: 'Slots' }
        )
      end
    end
  end
end
