# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :player_test14, class: Player do
    id { 187 }
    login { 'test14@itrade.support' }
    nickname { 'test14' }
    host_id { 1 }
    currency { 'RUB' }
    level_id { 0 }
  end
end
