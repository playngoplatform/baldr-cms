# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :game_provider_playngo, class: GameProvider do
    id { 1 }
    code { 'playngo' }
    name { 'PlayNGo' }
  end
end
