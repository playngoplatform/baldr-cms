# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :game_hugo, class: Game do
    id { 1 }
    name { "Hugo's Adventure" }
    popular { true }
    association :category, factory: :game_category_slot
    association :provider, factory: :game_provider_playngo
    game_id { 'hugosadventure' }
    is_desktop { true }
    is_mobile { true }
    width { 1024 }
    height { 768 }
    factory :game_hugo_with_translations do
      after(:create) do |game|
        game.set_translations(
          ru: { image_alt: "Hugo's Adventure",
                meta_title: "Hugo's Adventure",
                meta_description: "Hugo's Adventure" },
          en: { image_alt: "Hugo's Adventure",
                meta_title: "Hugo's Adventure",
                meta_description: "Hugo's Adventure" },
          pl: { image_alt: "Hugo's Adventure",
                meta_title: "Hugo's Adventure",
                meta_description: "Hugo's Adventure" }
        )
      end
    end
  end
end
