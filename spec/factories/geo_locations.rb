# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :geo_location do
    enabled { false }
    country { "DK" }
  end
end
