# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :bonus_welcome, class: Bonus do
    id { 1 }
    status { 1 }
    position { 1 }
    mode { 0 }
    code { 'WELCOME' }
    page_id { 'welcome_bonus_100' }
    registration { 0 }
    factory :bonus_welcome_with_translations do
      after(:create) do |bonus|
        bonus.set_translations(
          ru: { title: 'Welcome bonus 100% ru',
                content: 'Welcome Bonus Content ru',
                description: 'Welcome Bonus Description ru',
                meta_title: 'Welcome Bonus Meta Title ru',
                meta_description: 'Welcome Bonus Meta Description ru',
                promo: 'Welcome Bonus Promo ru' },
          en: { title: 'Welcome bonus 100% en',
                content: 'Welcome Bonus Content en',
                description: 'Welcome Bonus Description en',
                meta_title: 'Welcome Bonus Meta Title en',
                meta_description: 'Welcome Bonus Meta Description en',
                promo: 'Welcome Bonus Promo en' },
          pl: { title: 'Welcome bonus 100% pl',
                content: 'Welcome Bonus Content en',
                description: 'Welcome Bonus Description pl',
                meta_title: 'Welcome Bonus Meta Title pl',
                meta_description: 'Welcome Bonus Meta Description pl',
                promo: 'Welcome Bonus Promo pl' }
        )
      end
    end
  end
end
