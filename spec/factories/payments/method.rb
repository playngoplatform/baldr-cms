# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
FactoryBot.define do
  factory :default_limit_for_deposit_rub, class: 'Payments::Limit' do
    status { :enabled }
    level_id { 0 }
    level_name { 'Any' }
    currency { 'RUB' }
    min { 500 }
    max { 25_000 }
  end

  factory :default_limit_for_deposit_eur, class: 'Payments::Limit' do
    status { :enabled }
    level_id { 0 }
    level_name { 'Any' }
    currency { 'EUR' }
    min { 10 }
    max { 500 }
  end

  factory :default_limit_for_deposit_uah, class: 'Payments::Limit' do
    status { :enabled }
    level_id { 0 }
    level_name { 'Any' }
    currency { 'UAH' }
    min { 200 }
    max { 800 }
  end

  factory :default_limit_for_withdrawal_rub, class: 'Payments::Limit' do
    status { :enabled }
    level_id { 0 }
    level_name { 'Any' }
    currency { 'RUB' }
    min { 500 }
    max { 25_000 }
  end

  factory :default_limit_for_withdrawal_eur, class: 'Payments::Limit' do
    status { :enabled }
    level_id { 0 }
    level_name { 'Any' }
    currency { 'EUR' }
    min { 30 }
    max { 500 }
  end

  factory :default_limit_for_withdrawal_uah, class: 'Payments::Limit' do
    status { :enabled }
    level_id { 0 }
    level_name { 'Any' }
    currency { 'UAH' }
    min { 500 }
    max { 16_000 }
  end
end
