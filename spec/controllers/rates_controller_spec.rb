# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

RSpec.describe RatesController, type: :controller do

  # this lets us inspect the rendered results
  render_views

  let(:page) { Capybara::Node::Simple.new(response.body) }

  before(:all) do
    @rate = create(:rate_rub_to_usd)
  end

  login_as_admin

  describe "GET index" do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end
    it "should render the expected columns" do
      get :index
      expect(page).to have_content(@rate.from)
      expect(page).to have_content(@rate.to)
      expect(page).to have_content(@rate.rate)
    end
  end

end