# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

RSpec.describe BonusesController, type: :controller do
  # this lets us inspect the rendered results
  render_views

  let(:page) { Capybara::Node::Simple.new(response.body) }

  before do
    @bonus = create(:bonus_welcome_with_translations)
  end

  login_as_admin

  describe 'GET index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'should render the expected columns' do
      get :index
      expect(page).to have_content(I18n.t("bonus_status.#{@bonus.status}"))
      expect(page).to have_content(@bonus.page_id)
      expect(page).to have_content(@bonus.title)
      expect(page).to have_content(I18n.t("bonus_mode.#{@bonus.mode}"))
      expect(page).to have_content(@bonus.code)
      expect(page).to have_content(@bonus.position)
    end
  end
end
