# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

RSpec.describe AdminUsersController, type: :controller do

  # this lets us inspect the rendered results
  render_views

  let(:page) { Capybara::Node::Simple.new(response.body) }

  before(:all) do
    @admin = build(:admin)
  end

  login_as_admin

  describe "GET index" do

    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it "should render the expected columns" do
      get :index
      expect(page).to have_content(@admin.username)
      expect(page).to have_content(@admin.email)
      expect(page).to have_content(@admin.locale)
      expect(page).to have_content(@admin.timezone)
    end
  end

  describe "POST create" do
    context "with valid params" do
      it "creates a new AdminUser" do
        expect {
          post :create, params: { admin_user: attributes_for(:admin2) }
        }.to change(AdminUser, :count).by(1)
      end

      it "redirects to the created admin_user" do
        post :create, params: { admin_user: attributes_for(:admin2) }
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(admin_user_path(AdminUser.last))
      end

      it 'should create the admin_user' do
        post :create, params: { admin_user: attributes_for(:admin2) }
        admin_user = AdminUser.last

        expect(admin_user.username).to eq(attributes_for(:admin2)[:username])
        expect(admin_user.email).to eq(attributes_for(:admin2)[:email])
        expect(admin_user.locale).to eq(attributes_for(:admin2)[:locale])
        expect(admin_user.timezone).to eq(attributes_for(:admin2)[:timezone])
      end
    end

    context "with invalid params" do
      it 'invalid_attributes return http success' do
        post :create, params: { admin_user: attributes_for(:invalid_admin) }
        expect(response).to have_http_status(:success)
      end

      it 'invalid_attributes do not create a AdminUser' do
        expect do
          post :create, params: { admin_user: attributes_for(:invalid_admin) }
        end.not_to change(AdminUser, :count)
      end
    end
  end
end