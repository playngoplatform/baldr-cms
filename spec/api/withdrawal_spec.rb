# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'
require_relative 'shared/authorized'
require_relative 'shared/unauthorized'

describe API::Withdrawal do
  include Rack::Test::Methods
  include_context 'unauthorized'
  let(:wirecard_withdrawal) { create(:wirecard_withdrawal) }

  context 'GET /api/withdrawal' do
    include_context 'unauthorized'
    it 'returns an error if user unauthorized' do
      get '/api/withdrawal'
      expect(last_response.status).to eq(401)
    end

    context 'user authorized' do
      include_context 'authorized'

      let!(:host) { create(:host_stage) }
      let!(:player) { create(:player_test14) }

      it 'returns an empty array if there are no methods' do
        get '/api/withdrawal'
        expect(last_response.body).to eq([].to_json)
      end

      it 'returns an empty array if there are no limits for the method' do
        create(:wirecard_withdrawal)
        get '/api/withdrawal'
        expect(last_response.body).to eq([].to_json)
      end

      it 'returns an empty array if the method is not eligible' do
        create(:wirecard_withdrawal_with_limits, currency: ['XXX'])
        get '/api/withdrawal'
        expect(last_response.body).to eq([].to_json)
      end

      it 'returns a list of methods for withdrawal' do
        create(:deposit_wirecard_for_user_test14, :confirmed)
        wirecard = create(:wirecard_withdrawal_with_limits)
        limits = wirecard.limit(player.level_id, player.currency)
        result = wirecard.to_hash(player).merge(limits)
        get '/api/withdrawal'
        expect(last_response.body).to eq([result].to_json)
      end

    end

    it 'returns an empty array if the user didnt make any deposits'
    it 'returns a list of methods for withdrawal'
  end

  context 'post /api/withdrawal' do

    it 'returns an error if user unauthorized' do
      post '/api/withdrawal', method: wirecard_withdrawal.uuid, amount: 100
      expect(last_response.status).to eq(401)
    end

    context 'validation' do
      it 'returns an error if method is invalid' do
        post '/api/withdrawal', method: '', amount: 100
        expect(last_response.body).to include('method is invalid')
      end

      it 'returns an error if bonus_code is invalid ' do
        post '/api/withdrawal', method: wirecard_withdrawal.uuid, amount: 100, bonus_code: 'with space'
        expect(last_response.body).to include('bonus_code is invalid')
      end

      it 'returns an error if amount is below zero' do
        post '/api/withdrawal', method: wirecard_withdrawal.uuid, amount: -10
        expect(last_response.body).to include('amount does not have a valid value')
      end

      it 'returns an error if amount is above zero' do
        post '/api/withdrawal', method: wirecard_withdrawal.uuid, amount: 0
        expect(last_response.body).to include('amount does not have a valid value')
      end

      it 'returns an error if amount is above 1000000' do
        post '/api/withdrawal', method: wirecard_withdrawal.uuid, amount: 1_000_001
        expect(last_response.body).to include('amount does not have a valid value')
      end

      it 'returns an error if the amount does not within the limits'

    end
  end

  context 'POST /api/withdrawal' do
    context 'successful' do
      include_context 'authorized'

      before do
        create(:host_stage)
        create(:player_test14)
        create(:wirecard_withdrawal)
        create(:deposit_wirecard_for_user_test14, :confirmed)
      end
      it 'is created' do
        authorized = Result.new(true, value: 'token')
        allow(Paymentiq::VerifyAndAuthorize).to receive(:call).and_return(authorized)
        expect(Paymentiq::VerifyAndAuthorize.call).to eq(authorized)

        post '/api/withdrawal', method: wirecard_withdrawal.uuid, amount: 1500
        expect(Payments::Withdrawal.count).to eq(1)
      end
    end
  end
end
