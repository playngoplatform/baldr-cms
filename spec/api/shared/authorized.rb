# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
# TODO: refactor it!
RSpec.shared_context 'authorized' do
  before do
    Grape::Endpoint.before_each do |endpoint|
      allow(endpoint).to receive('authenticate!').and_return true
      allow(endpoint).to receive(:current_player).and_return(build_stubbed(:player_test14))
      allow(endpoint).to receive(:current_session).and_return(user_id: build(:player_test14).id)
    end
  end
end
