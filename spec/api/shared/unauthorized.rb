# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
# TODO: refactor it!
RSpec.shared_context 'unauthorized' do
  before do
    Grape::Endpoint.before_each do |endpoint|
      allow(endpoint).to receive('authenticate!').and_call_original
      allow(endpoint).to receive(:current_player).and_call_original
      allow(endpoint).to receive(:current_session).and_call_original
    end
  end
end
