# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'
require_relative 'shared/authorized'
require_relative 'shared/unauthorized'

describe API::Deposit do
  include Rack::Test::Methods
  let(:wirecard_deposit) { create(:wirecard_deposit) }
  let(:success_url) { 'https://google.com' }
  let(:failure_url) { success_url }

  let(:params) do
    { method: wirecard_deposit.uuid,
      amount: 500,
      success_url: success_url,
      failure_url: failure_url }
  end

  context 'GET /api/deposit' do
    include_context 'unauthorized'
    it 'returns an error if user unauthorized' do
      get '/api/deposit'
      expect(last_response.status).to eq(401)
    end

    context 'user authorized' do
      include_context 'authorized'

      before do
        header 'Auth-Token', 'auth_token'
      end

      let!(:host) { create(:host_stage) }
      let!(:player) { create(:player_test14) }


      it 'returns an empty array if there are no methods' do
        get '/api/deposit'
        expect(last_response.body).to eq([].to_json)
      end

      it 'returns an empty array if there are no limits for the method' do
        create(:wirecard_deposit)
        get '/api/deposit'
        expect(last_response.body).to eq([].to_json)
      end

      it 'returns an empty array if the method is not eligible' do
        create(:wirecard_deposit_with_limits, currency: ['XXX'])
        get '/api/deposit'
        expect(last_response.body).to eq([].to_json)
      end

      it 'returns a list of methods for deposit' do
        wirecard = create(:wirecard_deposit_with_limits)
        limits = wirecard.limit(player.level_id, player.currency)
        result = wirecard.to_hash(player).merge(limits)
        get '/api/deposit'
        expect(last_response.body).to eq([result].to_json)
      end
    end
  end

  context 'POST /api/deposit' do
    include_context 'unauthorized'


    it 'returns an error if user unauthorized' do
      post '/api/deposit', params
      expect(last_response.status).to eq(401)
    end
    context 'params validation' do
      it 'returns an error if method is invalid' do
        params[:method] = ''
        post '/api/deposit', params
        expect(last_response.body).to include('method is invalid')
      end

      it 'returns an error if bonus_code is invalid ' do
        params[:bonus_code] = 'with space'
        post '/api/deposit', params
        expect(last_response.body).to include('bonus_code is invalid')
      end

      it 'returns an error if amount is below zero' do
        params[:amount] = - 10
        post '/api/deposit', params
        expect(last_response.body).to include('amount does not have a valid value')
      end

      it 'returns an error if amount is above zero' do
        params[:amount] = 0
        post '/api/deposit', params
        expect(last_response.body).to include('amount does not have a valid value')
      end

      it 'returns an error if amount is above 1000000' do
        params[:amount] = 1_000_001
        post '/api/deposit', params
        expect(last_response.body).to include('amount does not have a valid value')
      end

      it 'returns an error if the amount does not within the limits'

      it 'returns an error if the deposit cant be authorized by Maven API'

    end
    context 'user authorized' do
      include_context 'authorized'
      before do
        create(:host_stage)
        create(:player_test14)
      end
      it 'is created' do
        authorized = Result.new(true, value: 'token')
        allow(Paymentiq::VerifyAndAuthorize).to receive(:call).and_return(authorized)
        expect(Paymentiq::VerifyAndAuthorize.call).to eq(authorized)
        allow_any_instance_of(Payments::Deposits::Wirecard).to receive(:url).and_return('url')

        post '/api/deposit', params
        expect(Payments::Deposit.count).to eq(1)
        expect(last_response.body).to eq('url'.to_json)
      end
    end
  end
end
