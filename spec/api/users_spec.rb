# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

describe API::Users do

  context 'POST /api/users/verify' do
    it 'returns an error if an email_id is incorrect'
    it 'returns an error if a user_id is incorrect'
    it 'returns true if a user is verified'
    it 'returns false if a user is not verified'
  end
end