# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

describe API::Common do

  context 'GET /api/common/ping' do
    it 'returns pong' do
      get '/api/common/ping'
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)).to eq 'pong'
    end
  end
end