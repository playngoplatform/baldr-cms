# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

RSpec.describe BonusLimit, type: :model do
  it { should belong_to(:bonus) }
end
