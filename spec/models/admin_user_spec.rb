# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

RSpec.describe AdminUser, type: :model do

  context 'Valid Factory' do
    it "has a valid factory" do
      expect(build(:admin)).to be_valid
    end

  end

  context "validations" do
    before { create(:admin) }
    context "presence" do
      it { should validate_presence_of :username }
      it { should validate_presence_of :email }
    end
  end

end
