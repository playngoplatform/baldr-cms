# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

RSpec.describe Payments::Withdrawal, type: :model do
  context 'validations' do
    it { should validate_presence_of :transaction_id }
    it { should validate_presence_of :auth_token }
    it { should validate_presence_of :account_id }
    it { should validate_presence_of :amount }
    it { should validate_presence_of :user_id }
    it { should validate_presence_of :currency }
    it { should validate_presence_of :paymentiq_amount }
    it { should validate_presence_of :method_uuid }
    it { should validate_presence_of :login }
    it { should validate_numericality_of :amount}
    it { should validate_numericality_of :paymentiq_amount}

    it { should define_enum_for(:state) }
    it { should define_enum_for(:state).with_values({ not_authorized: 0,
                                               authorized: 10,
                                               confirmed: 20,
                                               needs_to_approve: 30,
                                               refused: 40,
                                               pending: 50,
                                               cancelled: 60
                                             }) }
  end
  context 'states' do
    context 'valid transitions' do
      it { is_expected.to have_state(:not_authorized) }
      it { is_expected.to transition_from(:not_authorized).to(:authorized).on_event(:authorize) }
      it { is_expected.to transition_from(:authorized).to(:needs_to_approve).on_event(:needs_to_approve) }
      it { is_expected.to transition_from(:needs_to_approve).to(:pending).on_event(:pending) }
      it { is_expected.to transition_from(:pending).to(:needs_to_approve).on_event(:cancel) }
      it { is_expected.to transition_from(:needs_to_approve).to(:cancelled).on_event(:refuse) }
      it { is_expected.to transition_from(:pending).to(:confirmed).on_event(:confirm) }
    end

    context 'invalid transitions' do

      it 'from state :not_authorized to :confirmed' do
        expect { subject.confirm! }.to raise_error(AASM::InvalidTransition)
      end

      it 'from state :needs_to_approve to :confirmed' do
        subject.state = :needs_to_approve
        expect { subject.confirm! }.to raise_error(AASM::InvalidTransition)
      end

      it 'from state :authorized to :confirmed' do
        subject.state = :authorized
        expect { subject.confirm! }.to raise_error(AASM::InvalidTransition)
      end

      it 'from state :confirmed to :pending' do
        subject.state = :confirmed
        expect { subject.pending! }.to raise_error(AASM::InvalidTransition)
      end

      it 'from state :confirmed to :needs_to_approve' do
        subject.state = :confirmed
        expect { subject.needs_to_approve! }.to raise_error(AASM::InvalidTransition)
      end

    end
  end
end
