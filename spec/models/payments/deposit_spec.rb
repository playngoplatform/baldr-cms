# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

RSpec.describe Payments::Deposit, type: :model do
  context 'validations' do
    it { should validate_presence_of :transaction_id }
    it { should validate_presence_of :auth_token }
    it { should validate_presence_of :amount }
    it { should validate_presence_of :user_id }
    it { should validate_presence_of :currency }
    it { should validate_presence_of :paymentiq_amount }
    it { should validate_presence_of :method_uuid }
    it { should validate_presence_of :login }
    it { should validate_presence_of :success_url }
    it { should validate_presence_of :failure_url }
    it { should validate_numericality_of :amount }
    it { should validate_numericality_of :paymentiq_amount }

    it { should define_enum_for(:state) }
    it {
      should define_enum_for(:state).with_values(not_authorized: 0,
                                                 authorized: 10,
                                                 confirmed: 20,
                                                 cancelled: 30,
                                                 pending: 40)
    }
  end
  context 'states' do
    context 'valid transitions' do
      it { is_expected.to have_state(:not_authorized) }
      it { is_expected.to transition_from(:not_authorized).to(:authorized).on_event(:authorize) }
      it { is_expected.to transition_from(:authorized).to(:pending).on_event(:pending) }
      it { is_expected.to transition_from(:pending).to(:cancelled).on_event(:cancel) }
      it { is_expected.to transition_from(:pending).to(:confirmed).on_event(:confirm) }
    end

    context 'invalid transitions' do
      it 'from state :not_authorized to :confirmed' do
        expect { subject.confirm! }.to raise_error(AASM::InvalidTransition)
      end

      it 'from state :authorized to :confirmed' do
        subject.state = :authorized
        expect { subject.confirm! }.to raise_error(AASM::InvalidTransition)
      end

      it 'from state :confirmed to :pending' do
        subject.state = :confirmed
        expect { subject.pending! }.to raise_error(AASM::InvalidTransition)
      end
    end
  end
end
