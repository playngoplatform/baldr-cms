# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
require 'rails_helper'

RSpec.describe Banner, type: :model do
  context 'validations' do
    it { should define_enum_for(:action) }
    it { should define_enum_for(:action).with_values(play: 0, bonus: 10, nothing: 20) }
    it { should define_enum_for(:size) }
    it { should define_enum_for(:size).with_values(small: 0, big: 10) }
    it { should define_enum_for(:status) }
    it { should define_enum_for(:status).with_values(published: 0, draft: 1) }
    it { should allow_value('', nil).for(:object_id) }
    it 'is valid if object_id game or bonus' do
      game = create(:game_hugo)
      bonus = create(:bonus_welcome)
      subject.object_id = game.game_id
      expect(subject).to be_valid
      subject.object_id = bonus.page_id
      expect(subject).to be_valid
    end
    it 'not valid if object_id is not game or bonus' do
      subject.object_id = 'wrong_id'
      expect(subject).to_not be_valid
    end
  end

  describe '#to_hash' do
    it 'returns valid value' do
      banner = build(:banner, :big, :hugo, :published)
      allow(banner).to receive(:thumbnail_url).and_return('')
      expect(banner.to_hash).to eq(action: banner.action,
                                   size: banner.size,
                                   object_id: banner.object_id,
                                   thumbnail: banner.thumbnail_url)
    end
  end

  describe '.active' do
    context 'not empty response' do
      it 'returns list if :today BETWEEN start_date AND end_date' do
        banner1  = create(:banner, :big, :published, start_date: Date.yesterday, end_date: Date.tomorrow)
        banner2  = create(:banner, :small, :published, start_date: Date.yesterday, end_date: Date.tomorrow)
        expect(Banner.active).to eq([banner1, banner2])
      end
      it 'returns list if start_date IS NULL AND end_date >= :today' do
        banner1  = create(:banner, :big, :published, end_date: Date.tomorrow)
        banner2  = create(:banner, :small, :published, end_date: Date.tomorrow)
        expect(Banner.active).to eq([banner1, banner2])
      end
      it 'returns list if start_date <= :today AND end_date IS NULL' do
        banner1  = create(:banner, :big, :published, start_date: Date.yesterday)
        banner2  = create(:banner, :small, :published, start_date: Date.yesterday)
        expect(Banner.active).to eq([banner1, banner2])
      end
      it 'returns list if start_date IS NULL AND end_date IS NULL' do
        banner1  = create(:banner, :big, :published)
        banner2  = create(:banner, :small, :published)
        expect(Banner.active).to eq([banner1, banner2])
      end
    end
    context 'empty response' do
      it 'returns empty list if there are no published bonuses' do
        create(:banner, :big, :draft)
        create(:banner, :small, :draft)
        expect(Banner.active).to be_empty
      end
      it 'returns empty list if :today not in BETWEEN start_date AND end_date' do
        create(:banner, :big, :published, start_date: 2.day.ago, end_date: Date.yesterday)
        create(:banner, :small, :published, start_date: 2.day.ago, end_date: Date.yesterday)
        expect(Banner.active).to be_empty
      end
    end
  end
end
