# Baldr

Baldr is a CMS and gateway service for creating online casino sites

## Versions
Ruby 2.6

Rails 5.2.4.1


## Setup

### Create Rails credentials file
* Run 'rails credentials:edit'

### Create database
* Run 'rails db:create', 'rails db:migrate', 'rails db:seed', 'rails db:seed_fu'
