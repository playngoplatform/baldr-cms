# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Cashier
  module V1
  end

  class Engine < ::Rails::Engine
    initializer "cashier.config", :before=> :load_config_initializers do |app|
      app.config.plugins.cashier_module = Cashier
      CashierConfig = Struct.new(:endpoint, :wsdl_url, :username, :password)
      app.config.cashier = CashierConfig.new
      app.config.cashier.endpoint = ENV['CASHIER_ENDPOINT'] || Rails.application.credentials.dig(:development, :cashier, :endpoint)
      app.config.cashier.wsdl_url = ENV['CASHIER_WSDL_URL'] || Rails.application.credentials.dig(:development, :cashier, :wsdl_url)
      app.config.cashier.username = ENV['CASHIER_USERNAME'] || Rails.application.credentials.dig(:development, :maven, :username)
      app.config.cashier.password = ENV['CASHIER_PASSWORD'] || Rails.application.credentials.dig(:development, :maven, :password)
    end
  end
end
