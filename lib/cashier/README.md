# Cashier
This gem handles the SOAP calls to Cashier service from the CMS.

## Usage
Use the Cashier gem to perform SOAP calls to the Cashier service. This is used for fetching player transactions.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'cashier', git: 'https://bitbucket.org/playngoplatform/gem-cashier'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install cashier
```

## License
Proprietary software
