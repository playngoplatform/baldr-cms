# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Cashier
  module V1
    class GetServiceVersionService < CommonService

      def call
        super(:get_service_version) do |result|
          Result.new(true, data: result.dig(:service_version_response, :version))
        end
      end
    end
  end
end