# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Cashier
  module V1
    class NetsDepositService < CommonService
      def call(params)
        super(
          :nets_deposit,
          Session: params[:session],
          Amount: {
            Value: params[:amount][:value],
            Currency: params[:amount][:currency]
          },
          SelfHosted: params[:self_hosted],
          BonusCode: params[:bonus_code],
          RedirectURL: params[:redirect_url],
          RedirectOnError: params[:redirect_on_error]
        ) do |result|
          Result.new(true, result.dig(:nets_deposit_response, :redirect_url))
        end
      end
    end
  end
end