# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Cashier
  module V1
    class CashierErrorService < Service
      def call(code)
        I18n.t("cashier.error_message.#{code}", raise: true)
      rescue I18n::MissingTranslationData
        Rails.logger.info "Cashier error: #{code}"
        I18n.t('cashier.error_message.undefined', code: code)
      end
    end
  end
end
