# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Cashier
  module V1
    class GetOrdersService < CommonService
      def call(params)
        super(
          :get_orders,
          session: params[:session],
          skip: params[:skip] || 0,
          limit: params[:limit] || 50,
          start_date: params[:from],
          end_date: params[:to]
        ) do |result|
          filtered = Array.wrap(result.dig(:get_orders_response, :orders, :order_info))
          filtered.map! { |h| h[:provider] = h.dig(:order_sub_type, :external_id); h }
          filtered.map! do |h|
            h[:currency] = h.dig(:amount, :currency)
            h[:amount] = h.dig(:amount, :value)
            h[:start_date] = h[:start_date].to_datetime
            h[:end_date] = h[:end_date].to_datetime if h[:end_date]
            h
          end
          filtered.map! { |h| h.except(:id, :type, :external_id, :message, :order_sub_type) }
          filtered
        end
      end
    end
  end
end
