# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Cashier
  module V1
    class NetsNotifyService < CommonService
      def call(transaction_id, response_code)
        super(
          :nets_notify,
          TransactionId: transaction_id,
          ResponseCode: response_code
        ) do |result|
          Result.new(true)
        end
      end
    end
  end
end