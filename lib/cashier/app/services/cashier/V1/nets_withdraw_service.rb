# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Cashier
  module V1
    class NetsWithdrawService < CommonService
      def call(params)
        super(
          :nets_withdraw,
          Session: params[:session],
          Amount: {
            Value: params[:amount][:value],
            Currency: params[:amount][:currency]
          }
        ) do |result|
          Result.new(true, result.dig(:nets_withdraw_response, :order_id))
        end
      end
    end
  end
end
