namespace :money do
  desc 'Load todays rates'
  task update_rates: :environment do
    Money.default_bank.update_rates
  end
end
