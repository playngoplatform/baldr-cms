namespace :seeds do
  desc 'Sync Locales'
  task locales: :environment do
    SeedFu.seed("#{Rails.root}/db/fixtures/", /locales/)
  end
end