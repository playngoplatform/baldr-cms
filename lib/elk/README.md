# Elk
This is a gem for generate the player url for game provider Elk.

## Usage
Just install the gem and call:

```ruby
::Elk::V1::PlayUrlService.call(
    session,
    desktop,
    game_id,
    current_player,
    mode
)
```

## Installation
The following environment variables needs to be set:
ELK_HOSTNAME
ELK_OPERATOR_ID

Add this line to your application's Gemfile:

```ruby
gem 'elk'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install elk
```

## License
Proprietary software