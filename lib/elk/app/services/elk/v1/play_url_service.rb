# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Elk
  module V1
    class PlayUrlService < Service
      def call(session, desktop, game_id, current_player, mode)
        ticket = nil
        play_real_game = mode == 'real' && session
        if play_real_game
          ticket = ::PlayerService.play({
            session: session,
            product: 'Elk',
            game_id: game_id,
            language: I18n.locale
          })
        end
        default_params = {
          game_id: game_id,
          operator_id: ENV['ELK_OPERATOR_ID'],
          hostname: ENV['ELK_HOSTNAME']
        }

        if desktop
          if ticket
            url = Elk::V1::config['games']['elk']['desktopEndpointReal']
            params = {
              token: ticket,
              language: I18n.locale,
              currency: current_player.currency
            }
          else
            url = Elk::V1::config['games']['elk']['desktopEndpointDemo']
            params = {}
          end
        else
          if ticket
            url = Elk::V1::config['games']['elk']['mobileEndpointReal']
            params = {
              token: ticket,
              language: I18n.locale,
              currency: current_player.currency
            }
          else
            url = Elk::V1::config['games']['elk']['mobileEndpointDemo']
            params = {}
          end
        end
        {
          type: 'html',
          value: format(url, params.merge(default_params))
        }
      end
    end
  end
end
    