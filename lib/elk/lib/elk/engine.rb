# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Elk
  module V1
    def self.setup
      root_path = File.expand_path("../../../", __FILE__)
      @config = YAML::load_file(Elk::Engine.root.join("config").to_s + "/settings.yml")
    end

    def self.config
      Rails.env.development? ? @config['development'] : @config['production']
    end  
  end

  class Engine < ::Rails::Engine
    raise "Missing env variables for production" if Rails.env.production? && (!ENV['ELK_OPERATOR_ID'].present? || !ENV['ELK_HOSTNAME'].present?)
    Elk::V1.setup
    initializer "elk.config", :before=> :load_config_initializers do |app|
      SeedFu.fixture_paths.push(Elk::Engine.root.join("app", "db", "fixtures").to_s)
    end
  end
end
