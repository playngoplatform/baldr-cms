# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Playngo
  module V1
    class PlayUrlService < Service
      def call(session, desktop, game_id, width, height, server_url, mode)
        ticket = nil
        play_real_game = mode == 'real' && session
        if play_real_game
          ticket = ::PlayerService.play({
            session: session,
            product: 'PlaynGOCasino',
            game_id: game_id,
            language: I18n.locale
          })
        end

        params = {
          gid: game_id,
          lang: I18n.locale,
          ticket: ticket ? ticket : 'demo',
          pid: ENV['PLAYNGO_PID'],
          origin_portal: ENV['ORIGIN_PORTAL'],
          hostname: ENV['PLAYNGO_HOSTNAME']
        }

        if desktop
          if ticket
            url = Playngo::V1::config['games']['playngo']['desktopEndpointReal']
          else
            url = Playngo::V1::config['games']['playngo']['desktopEndpointDemo']
          end
        else
          if ticket
            url = Playngo::V1::config['games']['playngo']['mobileEndpointReal']
          else
            url = Playngo::V1::config['games']['playngo']['mobileEndpointDemo']
          end
        end
        {
          type: 'html',
          value: format(url, params)
        }
      end
    end
  end
end
