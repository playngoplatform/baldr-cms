# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Playngo
  module V1
    def self.setup
      root_path = File.expand_path("../../../", __FILE__)
      @config = YAML::load_file(Playngo::Engine.root.join("config").to_s + "/settings.yml")
    end

    def self.config
      Rails.env.development? ? @config['development'] : @config['production']
    end  
  end

  class Engine < ::Rails::Engine
    raise "Missing env variables for production" if Rails.env.production? && (!ENV['PLAYNGO_PID'].present? || !ENV['ORIGIN_PORTAL'].present? || !ENV['PLAYNGO_HOSTNAME'].present?)
    Playngo::V1.setup
    initializer "playngo.config", :before=> :load_config_initializers do |app|
      SeedFu.fixture_paths.push(Playngo::Engine.root.join("app", "db", "fixtures").to_s)
    end
  end
end
