# Playngo
This is the gem for integrating Play'n GO games into the CMS. This makes it possible to launch Play'n GO games.

## Usage
Just install the gem and call:

```ruby
::Playngo::V1::PlayUrlService.call(
    session,
    desktop,
    game_id,
    width,
    height,
    server_url,
    mode
)
```

## Installation
Following environment variables has to be set for production:
ORIGIN_PORTAL (Url for the portal)
PLAYNGO_PID (id for the operator)
PLAYNGO_HOSTNAME

Add this line to your application's Gemfile:

```ruby
gem 'playngo'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install playngo
```

## License
Proprietary software