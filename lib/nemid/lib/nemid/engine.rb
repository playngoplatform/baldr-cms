# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nemid
  module V1
    def self.setup
      root_path = File.expand_path("../../../", __FILE__)
      @config = YAML::load_file(Nemid::Engine.root.join("config").to_s + "/settings.yml")
    end

    def self.config
      Rails.env.development? ? @config['development'] : @config['production']
    end

    def self.set_provider_variables(env_data)
      raise "Missing credential variables for Nemid" if (!env_data['certificate'].present? || !env_data['certificate_key'].present? || !env_data['service_provider_id'].present?)
      config = Rails.env.development? ? @config['development'] : @config['production']
      config['nemid']['certificate'] = env_data['certificate']
      config['nemid']['certificate_key'] = env_data['certificate_key']
      config['nemid']['service_provider_id'] = env_data['service_provider_id']
    end
  end

  class Engine < ::Rails::Engine
    Nemid::V1::setup
    initializer "nemid.config", :before=> :load_config_initializers do |app|
      env_data = {}
      env_data['certificate'] = ENV['NEMID_CERTIFICATE'] || Rails.application.credentials.dig(:production, :nemid, :certificate)
      env_data['certificate_key'] = ENV['NEMID_CERTIFICATE_KEY'] || Rails.application.credentials.dig(:production, :nemid, :certificate_key)
      env_data['service_provider_id'] = ENV['NEMID_SERVICE_PROVIDER_ID'] || Rails.application.credentials.dig(:production, :nemid, :service_provider_id)
      Nemid::V1::set_provider_variables(env_data)
    end
  end
end
