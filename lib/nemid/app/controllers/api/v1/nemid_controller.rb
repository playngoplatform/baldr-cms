# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module V1
  class NemidController < ActionController::API
    def login_parameters
      result = ::Nemid::V1::LoginParametersService.call()
      render json: result
    end

    def validate_signature
      validate_and_extract_pid_service = ::Nemid::V1::ValidateAndExtractPidService.new
      result = validate_and_extract_pid_service.call(params[:signature])
      if (result[:pid])
        render json: result
      else
        render json: { errors: result[:error] }, status: 400
      end
    end

    def pid_cpr_match
      success = ::Nemid::V1::PidCprMatchService.call(params[:pid], params[:cpr])
      if success
        head :ok
      else
        render json: { errors: "CPR is invalid" }, status: 400
      end
    end
  end
end
