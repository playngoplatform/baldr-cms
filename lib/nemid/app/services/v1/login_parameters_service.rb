# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nemid
  module V1
    class LoginParametersService
      def self.call()
        parameters = {
          clientflow: 'oceslogin2',
          sp_cert: get_service_provider_cert(),
          timestamp: get_timestamp(),
        }
        normalized_parameters = normalize_parameters(parameters)
        parameters[:params_digest] = get_params_digest(normalized_parameters)
        parameters[:digest_signature] = get_digest_signature(normalized_parameters)
        parameters
      end

      private

      def self.get_service_provider_cert
        ::Nemid::V1::config['nemid']['certificate']
      end

      def self.get_timestamp
        timestamp = DateTime.now().utc.strftime("%F %T%z")
        base64_encode(timestamp)
      end

      def self.get_certificate_key
        key_base64 = ::Nemid::V1::config['nemid']['certificate_key']
        key_der = Base64.decode64(key_base64)
        OpenSSL::PKey::RSA.new(key_der)
      end

      def self.normalize_parameters(parameters)
        normalized_parameters = ''
        sorted_parameters = parameters.sort.to_h
        sorted_parameters.each do |key, value|
          normalized_parameters += key.to_s + value.to_s
        end
        normalized_parameters.force_encoding("utf-8")
      end

      def self.get_params_digest(parameters)
        Digest::SHA256.base64digest(parameters)
      end

      def self.get_digest_signature(parameters)
        key = get_certificate_key()
        sha256 = OpenSSL::Digest::SHA256.new
        signature = key.sign(sha256, parameters)
        base64_encode(signature)
      end

      def self.base64_encode(data)
        Base64.encode64(data).tr("\t\r\n", "")
      end
    end
  end
end
