# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nemid
  module V1
    class PidCprMatchService
      def self.call(pid, cpr)
        client = get_soap_client
        message = get_soap_message(pid, cpr)
        response = client.call(:pid, message: message, soap_action: '')
        response.hash.dig(:envelope, :body, :pid_response, :result, :pid_reply, :status_code) == '0'
      end

      def self.get_soap_client
        certificate = get_certificate
        certificate_key = get_certificate_key
        pid_service_url = ::Nemid::V1::config['nemid']['pid_service_url']
        namespaces = {
          'xmlns:java': 'java:dk.certifikat.pid.webservices' 
        }

        Savon.client do |globals|
          globals.wsdl pid_service_url + '?wsdl'
          globals.endpoint pid_service_url
          globals.env_namespace :soapenv
          globals.namespace_identifier 'loc'
          globals.namespaces namespaces
          globals.logger Rails.logger
          globals.pretty_print_xml true
          globals.log true
          globals.ssl_cert certificate
          globals.ssl_cert_key certificate_key
        end
      end

      def self.get_soap_message(pid, cpr)
        service_provider_id = ::Nemid::V1::config['nemid']['service_provider_id']
        {
          PIDRequests: {
            'java:PIDRequest': {
              'java:PID': pid,
              'java:CPR': cpr,
              'java:serviceId': service_provider_id,
              'java:b64Cert': '',
              'java:id': ''
            }
          }
        }
      end

      def self.get_certificate
        certificate_base64 = ::Nemid::V1::config['nemid']['certificate']
        certificate_der = Base64.decode64(certificate_base64)
        OpenSSL::X509::Certificate.new(certificate_der)
      end

      def self.get_certificate_key
        key_base64 = ::Nemid::V1::config['nemid']['certificate_key']
        key_der = Base64.decode64(key_base64)
        OpenSSL::PKey::RSA.new(key_der)
      end
    end
  end
end