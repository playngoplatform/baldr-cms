# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nemid
  module V1
    class ValidateAndExtractPidService
      def call(signature)
        init(signature)
        return { error: 'Signature has wrong format' } if has_errors
        return { error: 'Signature is not a logon signature' } unless is_logon_signature
        return { error: "Logonto value doesn't match the signature value" } unless is_valid_logonto
        certificates = get_certificates
        end_user_certificate = get_end_user_certificate(certificates)
        return { error: "Couldn't find the end user certificate" } if end_user_certificate.nil?
        pid = get_pid(end_user_certificate)
        return { error: "Couldn't find PID" } if pid.nil? || pid == ''
        { pid: pid }
      end

      private

      def init(signature)
        signature_xml = Base64.decode64(signature)
        @doc = Nokogiri::XML(signature_xml)
      end

      def get_certificates
        certificates_xml = @doc.xpath('//ds:X509Certificate', @doc.collect_namespaces)
        certificates = []
        certificates_xml.each do |cert_xml|
          decoded_cert = Base64.decode64(cert_xml.inner_html)
          certificates << OpenSSL::X509::Certificate.new(decoded_cert)
        end
        certificates
      end

      def get_end_user_certificate(certificates)
        certificates.each do |cert|
          return cert if cert.subject.to_s.include? 'PID:'
        end
      end

      def has_errors
        @doc.errors.length > 0
      end

      def get_request_issuer
        property = get_property('RequestIssuer')
        property.nil? ? nil : Base64.decode64(property)
      end

      def get_action
        get_property('action')
      end

      def get_uri
        reference = @doc.xpath('//ds:Reference', @doc.collect_namespaces)
        return nil if reference.nil?
        reference.attribute('URI')
      end

      def get_property(name)
        properties = @doc.xpath('//ds:SignatureProperty', @doc.collect_namespaces)
        properties.each do |property|
          continue unless property.children.length == 2
          current_name = property.children[0].content
          if current_name == name
            return property.children[1].content
          end
        end
        nil
      end

      def get_pid(certificate)
        pattern = "((OID.2.5.4.5)|(?i:serialnumber))(\s)*=(\s)*(?<ssn>([^+,\s])*)"
        matches = certificate.subject.to_s.match(pattern)
        serial_number = matches.captures.first
        serial_number["PID:".length..-1]
      end

      def is_logon_signature
        uri = get_uri
        return false unless uri.value == '#ToBeSigned'
        action = get_action
        action == 'bG9nb24' || action.downcase == 'logon'
      end

      def is_valid_logonto()
        logonto = ::Nemid::V1::config['nemid']['logonto']
        logonto_property = get_property('logonto')
        request_issuer_property = get_request_issuer
        if !logonto_property.nil? && !request_issuer_property.nil?
          return { error: 'Both logonto property and request issuer property cannot be set'}
        end
        if logonto_property.nil? && request_issuer_property.nil?
          return { error: 'Either logonto property or request issuer property must be set'}
        end
        unless logonto_property.nil?
          logonto_property == logonto
        else
          request_issuer_property == logonto
        end
      end

    end
  end
end
