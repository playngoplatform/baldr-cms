# Nemid
Plugin for authenticating a CPR using nemid

## Usage
GET login-parameters
POST validate-signature
POST pid-cpr-match

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'nemid'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install nemid
```