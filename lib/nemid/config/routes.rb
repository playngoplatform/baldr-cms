# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
Rails.application.routes.draw do
  scope module: :v1 do
    scope path: "/api/v1/nemid" do
      match "login-parameters", to: "nemid#login_parameters", as: :login_parameters, via: :get
      match "validate-signature", to: "nemid#validate_signature", as: :validate_signature, via: :post
      match "pid-cpr-match", to: "nemid#pid_cpr_match", as: :pid_cpr_match, via: :post
    end
  end
end