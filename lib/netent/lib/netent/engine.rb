# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Netent
  module V1
    def self.setup
      root_path = File.expand_path("../../../", __FILE__)
    end

    def self.config
      Rails.env.development? ? @config['development'] : @config['production']
    end  
  end

  class Engine < ::Rails::Engine
    if Rails.env.production? && (!ENV['NETENT_GAME_SERVER_URL'].present? || !ENV['NETENT_STATIC_SERVER_URL'].present? || !ENV['NETENT_GAME_INCLUSION_ENDPOINT'].present?)
      raise "Missing env variables for production"
    end
    Netent::V1.setup
    initializer "netent.config", :before=> :load_config_initializers do |app|
      SeedFu.fixture_paths.push(Netent::Engine.root.join("app", "db", "fixtures").to_s)
    end
  end
end
