# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Netent
  module V1
    class PlayUrlService < Service
      def call(session, desktop, game_id, current_player, mode)
        if (desktop)
          client_type = 'DesktopFlash'
          game_id = game_id + '_not_mobile_sw'
        else
          client_type = 'MobileHtml'
          game_id = game_id + '_mobile_html_sw'
        end
        session = nil if mode == 'demo'
        session_id = ::PlayerService.play({
          session: session,
          product: 'NetEnt',
          game_id: game_id,
          language: I18n.locale,
          practice: false,
          client_type: client_type
        })
        config = {
          gameId: game_id,
          staticServer: ENV['NETENT_STATIC_SERVER_URL'],
          gameServer: ENV['NETENT_GAME_SERVER_URL'],
          sessionId: session_id,
          language: I18n.locale,
          targetElement: 'neGameClient',
          lobbyURL: "#",
          fullScreen: false,
          width: "100%",
          height: "100%",
          enforceRatio: false,
          disableDeviceDetection: true,
          launchType: desktop ? 'iframe' : 'iframeredirect',
          casinoBrand: 'casino_brand_name',
          applicationType: 'browser',
          iframeSandbox: 'allow-scripts allow-popups allow-popups-to-escape-sandbox allow-top-navigation allow-top-navigation-by-user-activation allow-same-origin allow-forms allow-pointer-lock'
        }
        {
          type: 'js',
          url: ENV['NETENT_GAME_INCLUSION_ENDPOINT'],
          config: config
        }
      end
    end
  end
end
