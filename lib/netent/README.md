# Netent
This is a gem for generate the player url for game provider NetEnt.

## Usage
Just install the gem and call:

```ruby
::Netent::V1::PlayerService.play({
    session,
    product,
    game_id,
    language,
    practice,
    client_type
})
```

## Installation
The following environment variables needs to be set:
NETENT_GAME_SERVER_URL
NETENT_STATIC_SERVER_URL
NETENT_GAME_INCLUSION_ENDPOINT

Add this line to your application's Gemfile:

```ruby
gem 'netent'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install netent
```

## License
Proprietary software