# Thunderkick
This is a gem for generate the player url for game provider Thunderkick.

## Usage
Just install the gem and call:

```ruby
::Thunderkick::V1::PlayUrlService.call(
    current_session,
    desktop,
    game_id,
    current_player,
    mode
)
```

## Installation
The following environment variables needs to be set:
THUNDERKICK_HOSTNAME
THUNDERKICK_OPERATOR_ID

Add this line to your application's Gemfile:

```ruby
gem 'thunderkick'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install thunderkick
```

## License
Proprietary software
