# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Thunderkick
  module V1
    class PlayUrlService < Service
      def call(session, desktop, game_id, current_player, mode)
        ticket = nil
        play_real_game = mode == 'real' && session
        if play_real_game
          ticket = ::PlayerService.play({
            session: session,
            product: 'Thunderkick',
            game_id: game_id,
            language: I18n.locale
          })
        end
        default_params = {
          game_id: game_id,
          operator_id: ENV['THUNDERKICK_OPERATOR_ID'],
          hostname: ENV['THUNDERKICK_HOSTNAME']
        }

        if desktop
          if ticket
            url = Thunderkick::V1::config['games']['thunderkick']['desktopEndpointReal']
            params = {
              player_session_id: ticket,
              lang_iso: I18n.locale,
              currency_iso: current_player.currency,
              no_cache: generate_no_cache_string
            }
          else
            url = Thunderkick::V1::config['games']['thunderkick']['desktopEndpointDemo']
            params = {
              no_cache: generate_no_cache_string
            }
          end
        else
          if ticket
            url = Thunderkick::V1::config['games']['thunderkick']['mobileEndpointReal']
            params = {
              player_session_id: ticket,
              lang_iso: I18n.locale,
              currency_iso: current_player.currency
            }
          else
            url = Thunderkick::V1::config['games']['thunderkick']['mobileEndpointDemo']
            params = {}
          end
        end
        {
          type: 'html',
          value: format(url, params.merge(default_params))
        }
      end

      private

      def generate_no_cache_string
        charset = Array('A'..'Z') + Array('a'..'z') + Array(0..9)
        Array.new(17) { charset.sample }.join
      end
    end
  end
end
