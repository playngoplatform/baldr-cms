# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
  end
  
  class Engine < ::Rails::Engine
    initializer "maven.config", :before=> :load_config_initializers do |app|
      app.config.plugins.player_module = Maven
      MavenConfig = Struct.new(:endpoint, :wsdl_url, :username, :password)
      app.config.maven = MavenConfig.new
      app.config.maven.endpoint = ENV['MAVEN_ENDPOINT'] || Rails.application.credentials.dig(:development, :maven, :endpoint)
      app.config.maven.wsdl_url = ENV['MAVEN_WSDL_URL'] || Rails.application.credentials.dig(:development, :maven, :wsdl_url)
      app.config.maven.username = ENV['MAVEN_USERNAME'] || Rails.application.credentials.dig(:development, :maven, :username)
      app.config.maven.password = ENV['MAVEN_PASSWORD'] || Rails.application.credentials.dig(:development, :maven, :password)
      end
  end
end
