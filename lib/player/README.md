# Player
Plugin that communicates to the maven API for the player.

## Usage
Just install it and the CMS will be able to communicate with the maven API

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'player'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install player
```

## Contributing
Contribution directions go here.
