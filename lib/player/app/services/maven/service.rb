# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  class Service
    class << self
      def call(*args)
        new.call(*args)
      end
    end
  end
end