# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class SetUserPasswordService < Maven::V1::CommonService
      def call(token, password, client_ip)
        super(:set_user_password, token: token,
              password: password, client_ip: client_ip) do |result|
          Result.new(true)
        end
      end
    end
  end
end