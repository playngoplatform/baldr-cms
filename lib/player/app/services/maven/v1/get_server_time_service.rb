# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class GetServerTimeService < Maven::V1::CommonService
      def call
        super(:get_server_time) do |result|
          Result.new(true, data: result.dig(:get_server_time_response, :time))
        end
      end
    end
  end
end