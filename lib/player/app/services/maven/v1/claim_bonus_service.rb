# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class ClaimBonusService < Maven::V1::CommonService
      def call(session, id)
        super(
          :claim_bonus,
          session: session,
          id: id
        ) do |result|
          Result.new(true, true)
        end
      end
    end
  end
end
