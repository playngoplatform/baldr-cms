# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class SetUserProtectionLimitsService < Maven::V1::CommonService
      def call(session, limits, lock_until, lock_reason_id)
        super(
          :set_user_protection_limits,
          session: session,
          limits: {
            deposit_limits: {
              single: limits[:deposit_limits][:single],
              day: limits[:deposit_limits][:day],
              week: limits[:deposit_limits][:week],
              month: limits[:deposit_limits][:month]
            },
            loss_limits: {
              single: limits[:loss_limits][:single],
              day: limits[:loss_limits][:day],
              week: limits[:loss_limits][:week],
              month: limits[:loss_limits][:month]
            },
            transfer_limits: {
              single: limits[:transfer_limits][:single],
              day: limits[:transfer_limits][:day],
              week: limits[:transfer_limits][:week],
              month: limits[:transfer_limits][:month]
            },
            max_session_time: limits[:max_session_time],
            reality_check_frequency: limits[:reality_check_frequency]
          },
          lock_until: lock_until = nil,
          lock_reason_id: lock_reason_id = nil
        ) do |result|
          Result.new(true)
        end
      end
    end
  end
end
