# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class GetUserLevelService < Maven::V1::CommonService
      def call(session)
        super(:get_user_level, session: session) do |result|
          Result.new(true, level_id: result.dig(:get_user_level_response, :user_level).to_i)
        end
      end
    end
  end
end