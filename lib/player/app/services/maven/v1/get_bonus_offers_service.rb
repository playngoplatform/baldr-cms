# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class GetBonusOffersService < Maven::V1::CommonService

      def call(session, state = nil, skip = 0, limit = 50)
        super(
          :get_bonus_offers,
          session: session,
          state: state,
          limit: limit,
          skip: skip
        ) do |result|
          filtered = result.dig(:get_bonus_offers_response, :bonus).except!(:"@xmlns:i")
          return Result.new(true, { bonus_offers: [] }) if filtered[:bonus_offer].nil?
          filtered[:bonus_offers] = filtered[:bonus_offer].kind_of?(Array) ? filtered[:bonus_offer] : [filtered[:bonus_offer]]
          filtered[:bonus_offers].map do |offer|
            offer[:id] = offer[:id] ? offer[:id].to_i : offer[:id]
            offer[:points_used] = offer[:points_used] ? offer[:points_used].to_i : offer[:points_used]
            offer[:points_max] = offer[:points_max] ? offer[:points_max].to_i : offer[:points_max]
            offer[:points_required] = offer[:points_required] ? offer[:points_required].to_i : offer[:points_required]
            offer[:points_available] = offer[:points_available] ? offer[:points_available].to_i : offer[:points_available]
            offer[:bonus_amount] = offer[:bonus_amount] ? offer[:bonus_amount].to_i : offer[:bonus_amount]
            offer[:bonus_fraction] = offer[:bonus_fraction] ? offer[:bonus_fraction].to_i : offer[:bonus_fraction]
            offer[:wagering_req] = offer[:wagering_req] ? offer[:wagering_req].to_i : offer[:wagering_req]
            offer[:wagered] = offer[:wagered] ? offer[:wagered].to_i : offer[:wagered]
            offer[:wagering_order] = offer[:wagering_order] ? offer[:wagering_order].to_i : offer[:wagering_order]
            offer[:bonus_amount_max] = offer[:bonus_amount_max] ? offer[:bonus_amount_max].to_i : offer[:bonus_amount_max]
            offer
          end
          Result.new(true, filtered)
        end
      end
    end
  end
end
