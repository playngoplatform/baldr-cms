# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class RegisterUserService < Maven::V1::CommonService
      def call(args, client_ip, user_agent)
        super(:register_user, {
          user_info: {
            login: args[:login],
            password: args[:password],
            nickname: args[:nickname] || args[:login][0..11],
            country: args[:country],
            birthday: args[:birthdate],
            first_name: args[:first_name],
            middle_name: args[:middle_name],
            last_name: args[:last_name],
            address_1: args[:address],
            zip: args[:zip],
            city: args[:city],
            phone: args[:phone],
            gender: args[:gender],
            currency: args[:currency],
            language: args[:language],
            email: args[:email],
            client_ip: client_ip,
            email_notify: true,
            national_id: args[:national_id],
            external_id: args[:external_id]
          },
          user_agent: user_agent }) do |result|
          Result.new(true, response: result.dig(:register_user_response))
        end
      end
    end
  end
end