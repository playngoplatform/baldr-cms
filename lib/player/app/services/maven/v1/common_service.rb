# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class CommonService < Maven::Service
      def initialize
        @client = Savon.client do |globals|
          globals.wsdl Rails.application.config.maven.wsdl_url
          globals.endpoint Rails.application.config.maven.endpoint
          globals.namespace 'http://www.playngo.com/2007/06'
          globals.soap_header 'ns:Operator': {
            'ns:Login': Rails.application.config.maven.username,
            'ns:Password': Rails.application.config.maven.password
          }
          globals.pretty_print_xml true
          globals.env_namespace :soapenv

          globals.namespace_identifier 'ns'
          globals.convert_request_keys_to :none
          globals.filters %w[Password UserPassword]
          globals.open_timeout 30
          globals.logger Rails.logger
          globals.log true
        end
      end

      def call(operation, args = {})
        response = @client.call(operation, message: args.deep_transform_keys { |key| "ns:#{key.to_s.camelize}" }) do
          advanced_typecasting true
          response_parser :nokogiri
        end
        yield response.body
      rescue Savon::SOAPFault => error
        fault_code = error.to_hash.dig(:fault, :detail, :service_fault, :error_id)
        return Result.new(false, error: MavenErrorService.call(fault_code)) if fault_code

        fault_message = error.to_hash.dig(:fault, :detail, :validation_fault, :details, :validation_detail, :message)
        return Result.new(false, error: fault_message) if fault_message

        Result.new(false, error: I18n.t('maven.error_message.undefined', code: 500))
      rescue Savon::HTTPError => error
        Rails.logger.error error
        Result.new(false, error: I18n.t('maven.error_message.undefined', code: error.http.code))
      end
    end
  end
end
