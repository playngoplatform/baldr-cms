# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class MavenErrorService < Maven::Service
      def call(code)
        I18n.t("maven.error_message.#{code}", raise: true)
      rescue I18n::MissingTranslationData
        Rails.logger.info "Maven error: #{code}"
        I18n.t('maven.error_message.undefined', code: code)
      end
    end
  end
end
