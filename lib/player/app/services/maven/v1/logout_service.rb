# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class LogoutService < Maven::V1::CommonService
      def call(session)
        super(:logout, session: session) do |result|
          Result.new(true)
        end
      end
    end
  end
end