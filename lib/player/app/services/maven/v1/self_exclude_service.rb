# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class SelfExcludeService < Maven::V1::CommonService
      def call(session, lock_until)
        super(
            :set_user_protection_limits,
            session: session,
            lock_until: lock_until,
            lock_reason_id: 8
        ) do |result|
          Result.new(true)
        end
      end
    end
  end
end