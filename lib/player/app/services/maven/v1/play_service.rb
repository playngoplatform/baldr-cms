# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class PlayService < Maven::V1::CommonService
      def call(session, product, game_id, language, practice=false, client_type=nil)
        super(:play,
              session: session,
              product: product,
              game_id: game_id,
              language: language,
              practice: practice,
              client_type: client_type
              ) do |result|
          result.dig(:play_response, :session_token)
        end
      end
    end
  end
end