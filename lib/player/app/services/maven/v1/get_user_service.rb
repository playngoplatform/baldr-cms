# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class GetUserService < Maven::V1::CommonService
      def call(session)
        super(:get_user, session: session) do |result|
          user_info = result.dig(:get_user_response, :user_info)
          user_info.except!(:user_id,
                            :password,
                            :address2,
                            :address3,
                            :email_notify,
                            :campaign_code,
                            :campaign_name,
                            :time_zone,
                            :refer_a_friend,
                            :custom_data,
                            :group_id,
                            :b_tag,
                            :custom_promotional_channels,
                            :registered,
                            :external_id,
                            :national_id,
                            :street_number,
                            :street_letter,
                            :street_name,
                            :pin,
                            :client_ip,
                            :"@xmlns:i")
          user_info
        end
      end
    end
  end
end
