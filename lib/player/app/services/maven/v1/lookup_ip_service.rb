# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class LookupIpService < Maven::V1::CommonService
      def call(ip_address)
        super(:lookup_ip, ip: ip_address) do |result|
          filtered = result.dig(:lookup_ip_response, :location)
          filtered.delete(:country) if filtered[:country].strip == '-'
          filtered.delete(:country_code) if filtered[:country_code].strip == '-'
          filtered.delete(:region) if filtered[:region].strip == '-'
          filtered.delete(:city) if filtered[:city].strip == '-'
          filtered
        end
      end
    end
  end
end