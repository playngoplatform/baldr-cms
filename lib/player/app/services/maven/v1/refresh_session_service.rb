# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class RefreshSessionService < Maven::V1::CommonService
      def call(session)
        super(:refresh_session, session: session) do |_result|
          Result.new(true)
        end
      end
    end
  end
end
