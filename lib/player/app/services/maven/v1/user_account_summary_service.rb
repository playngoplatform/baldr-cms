# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class UserAccountSummaryService < Maven::V1::CommonService
      def call(session)
        super(:user_account_summary, session: session ) do |result|
          Result.new(true, result.dig(:user_account_summary_response))
        end
      end
    end
  end
end