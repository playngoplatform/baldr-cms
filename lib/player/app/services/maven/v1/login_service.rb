# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class LoginService < Maven::V1::CommonService
      def call(username, password, client_ip, user_agent)
        super(:login, user_login: username,
                        user_password: password,
                        client_ip: client_ip,
                        user_agent: user_agent) do |result|
          Result.new(true, response: result.dig(:login_response))
        end
      end
    end
  end
end