# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class GetGameHistoryService < Maven::V1::CommonService
      def call(session, from, to, skip=0, limit=50)
        super(:get_game_history,
              session: session,
              product: 'None',
              from: from,
              to: to,
              skip: skip,
              limit: limit ) do |result|
          filtered = result.dig(:get_game_history_response, :history, :game_history)
          return [] if filtered.nil?
          filtered = [filtered] if filtered.kind_of?(Hash)
          filtered.map!{ |h| h.except(:details, :id, :game_type) }
          filtered.map!{ |h| h[:started] = h[:started].to_datetime if h[:started]; h }
          filtered.map!{ |h| h[:finished] = h[:finished].to_datetime if h[:finished]; h }
          filtered.sort_by { |h| h[:sorted] }.reverse!
          filtered
        end
      end
    end
  end
end