# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class UpdateUserService < Maven::V1::CommonService
      def call(session, args)
        super(:update_user,
              session: session,
              old_password: args[:password],
              user_info:
                                    {
                                      nickname: args[:nickname],
                                      country: args[:country],
                                      first_name: args[:first_name],
                                      last_name: args[:last_name],
                                      gender: args[:gender],
                                      language: args[:language],
                                      email: args[:email],
                                      mobile_no: args[:mobile_no]

                                    }) do |result|
          Result.new(true, response: result)
        end
      end
    end
  end
end
