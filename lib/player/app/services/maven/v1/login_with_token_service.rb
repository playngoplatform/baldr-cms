# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class LoginWithTokenService < Maven::V1::CommonService
      def call(token_type, token, client_ip, user_agent)
        super(:login_with_token,
              token_type: token_type,
              token: token,
              client_ip: client_ip,
              user_agent: user_agent) do |result|
          Result.new(true, response: result.dig(:login_response))
        end
      end
    end
  end
end