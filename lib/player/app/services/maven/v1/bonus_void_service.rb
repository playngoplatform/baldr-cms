# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class BonusVoidService < Maven::V1::CommonService

      def call(session, bonus_offer_id = nil)
        super(
          :bonus_void,
          session: session,
          bonus_offer_id: bonus_offer_id
        ) do |result|
          Result.new(true, true)
        end
      end
    end
  end
end
