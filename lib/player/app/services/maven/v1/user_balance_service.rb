# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class UserBalanceService < Maven::V1::CommonService
      def call(session, product='None', currency='Unknown')
        super(:user_balance, session: session,
                        product: product,
                        currency: currency) do |result|
          Result.new(true, real_amount: result.dig(:user_balance_response, :balance, :real_amount),
                    bonus_amount: result.dig(:user_balance_response, :balance, :bonus_amount))
        end
      end
    end
  end
end