# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class GetPlayerBetWinSummaryService < Maven::V1::CommonService
      def call(session, from, to)
        super(
          :get_player_bet_win_summary,
          session: session,
          start_date_time: from,
          end_date_time: to
        ) do |result|
          mapped = result.dig(:get_player_bet_win_summary_response).except!(:"@xmlns")
          mapped[:bonus_money_bet] = mapped[:bonus_money_bet].nil? ? nil : mapped[:bonus_money_bet].to_i
          mapped[:bonus_money_win] = mapped[:bonus_money_win].nil? ? nil : mapped[:bonus_money_win].to_i
          mapped[:real_money_bet] = mapped[:real_money_bet].nil? ? nil : mapped[:real_money_bet].to_i
          mapped[:real_money_win] = mapped[:real_money_win].nil? ? nil : mapped[:real_money_win].to_i
          Result.new(true, mapped)
        end
      end
    end
  end
end
