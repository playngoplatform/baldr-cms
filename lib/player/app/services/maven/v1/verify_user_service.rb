# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class VerifyUserService < Maven::V1::CommonService
      def call(email_id, client_ip, user_id)
        super(:verify_user, email_id: email_id, ip: client_ip, user_id: user_id) do |result|
          Result.new(true)
        end
      end
    end
  end
end