# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class GetUserProtectionLimitsService < Maven::V1::CommonService
      def call(session)
        super(:get_user_protection_limits, session: session) do |result|
          current_limits = result.dig(:get_user_protection_limits_response, :current_limits).except!(:"@xmlns:i")
          pending_limits = result.dig(:get_user_protection_limits_response, :pending_limits).except!(:"@xmlns:i")
          current_limits[:deposit_limits][:day] = current_limits[:deposit_limits][:day].nil? ? nil : current_limits[:deposit_limits][:day].to_i
          current_limits[:deposit_limits][:week] = current_limits[:deposit_limits][:week].nil? ? nil : current_limits[:deposit_limits][:week].to_i
          current_limits[:deposit_limits][:month] = current_limits[:deposit_limits][:month].nil? ? nil : current_limits[:deposit_limits][:month].to_i
          current_limits[:deposit_limits][:single] = current_limits[:deposit_limits][:single].nil? ? nil : current_limits[:deposit_limits][:single].to_i
          current_limits[:loss_limits][:day] = current_limits[:loss_limits][:day].nil? ? nil : current_limits[:loss_limits][:day].to_i
          current_limits[:loss_limits][:week] = current_limits[:loss_limits][:week].nil? ? nil : current_limits[:loss_limits][:week].to_i
          current_limits[:loss_limits][:month] = current_limits[:loss_limits][:month].nil? ? nil : current_limits[:loss_limits][:month].to_i
          current_limits[:loss_limits][:single] = current_limits[:loss_limits][:single].nil? ? nil : current_limits[:loss_limits][:single].to_i
          current_limits[:transfer_limits][:day] = current_limits[:transfer_limits][:day].nil? ? nil : current_limits[:transfer_limits][:day].to_i
          current_limits[:transfer_limits][:week] = current_limits[:transfer_limits][:week].nil? ? nil : current_limits[:transfer_limits][:week].to_i
          current_limits[:transfer_limits][:month] = current_limits[:transfer_limits][:month].nil? ? nil : current_limits[:transfer_limits][:month].to_i
          current_limits[:transfer_limits][:single] = current_limits[:transfer_limits][:single].nil? ? nil : current_limits[:transfer_limits][:single].to_i
          current_limits[:max_session_time] = current_limits[:max_session_time].nil? ? nil : current_limits[:max_session_time].to_i
          current_limits[:reality_check_frequency] = current_limits[:reality_check_frequency].nil? ? nil : current_limits[:reality_check_frequency].to_i
          pending_limits[:deposit_limits][:day] = pending_limits[:deposit_limits][:day].nil? ? nil : pending_limits[:deposit_limits][:day].to_i
          pending_limits[:deposit_limits][:week] = pending_limits[:deposit_limits][:week].nil? ? nil : pending_limits[:deposit_limits][:week].to_i
          pending_limits[:deposit_limits][:month] = pending_limits[:deposit_limits][:month].nil? ? nil : pending_limits[:deposit_limits][:month].to_i
          pending_limits[:deposit_limits][:single] = pending_limits[:deposit_limits][:single].nil? ? nil : pending_limits[:deposit_limits][:single].to_i
          pending_limits[:loss_limits][:day] = pending_limits[:loss_limits][:day].nil? ? nil : pending_limits[:loss_limits][:day].to_i
          pending_limits[:loss_limits][:week] = pending_limits[:loss_limits][:week].nil? ? nil : pending_limits[:loss_limits][:week].to_i
          pending_limits[:loss_limits][:month] = pending_limits[:loss_limits][:month].nil? ? nil : pending_limits[:loss_limits][:month].to_i
          pending_limits[:loss_limits][:single] = pending_limits[:loss_limits][:single].nil? ? nil : pending_limits[:loss_limits][:single].to_i
          pending_limits[:transfer_limits][:day] = pending_limits[:transfer_limits][:day].nil? ? nil : pending_limits[:transfer_limits][:day].to_i
          pending_limits[:transfer_limits][:week] = pending_limits[:transfer_limits][:week].nil? ? nil : pending_limits[:transfer_limits][:week].to_i
          pending_limits[:transfer_limits][:month] = pending_limits[:transfer_limits][:month].nil? ? nil : pending_limits[:transfer_limits][:month].to_i
          pending_limits[:transfer_limits][:single] = pending_limits[:transfer_limits][:single].nil? ? nil : pending_limits[:transfer_limits][:single].to_i
          pending_limits[:max_session_time] = pending_limits[:max_session_time].nil? ? nil : pending_limits[:max_session_time].to_i
          pending_limits[:reality_check_frequency] = pending_limits[:reality_check_frequency].nil? ? nil : pending_limits[:reality_check_frequency].to_i
          Result.new(
            true,
            current_limits: current_limits,
            pending_limits: pending_limits
          )
        end
      end
    end
  end
end
