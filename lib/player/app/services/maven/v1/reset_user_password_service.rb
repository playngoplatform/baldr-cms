# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class ResetUserPasswordService < Maven::V1::CommonService
      def call(login)
        super(:reset_user_password, user_login: login) do |result|
          Result.new(true)
        end
      end
    end
  end
end