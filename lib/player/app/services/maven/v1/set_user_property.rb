# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  module V1
    class SetUserProperty < Maven::V1::CommonService
      def call(session, property, value)
        super(:set_user_property, session: session, property: property, value: value) do |result|
          Result.new(true)
        end
      end
    end
  end
end
