# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nets
  class Engine < ::Rails::Engine
    initializer "nets.config", :before=> :load_config_initializers do |app|
      SeedFu.fixture_paths.push(Nets::Engine.root.join("app", "db", "fixtures").to_s)
      app.config.plugins.nets_module = Nets
    end
  end
end