# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
Rails.application.routes.draw do
  scope path: "/api/deposit/nets" do
    match "notify", to: "nets#notify", as: :notify, via: :post
  end
end
