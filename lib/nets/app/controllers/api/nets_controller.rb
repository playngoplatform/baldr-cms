# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class NetsController < ActionController::API
  def notify
    response = Nets::DepositService.notify(params[:transaction_id], params[:response_code])
    if response.success?
      response
    else
      render json: { errors: response[:error] }, status: 500
    end
  end
end
