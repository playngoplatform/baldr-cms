# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nets
  class WithdrawService
    def self.withdraw(params)
      Rails.application.config.plugins.cashier_module::V1::NetsWithdrawService.call(params)
    end
  end
end
