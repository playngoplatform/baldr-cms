# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nets
  class DepositService
    def self.deposit(params)
      Rails.application.config.plugins.cashier_module::V1::NetsDepositService.call(params)
    end

    def self.notify(transaction_id, response_code)
      Rails.application.config.plugins.cashier_module::V1::NetsNotifyService.call(transaction_id, response_code)
    end
  end
end
