# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
env_name = (Rails.env.production? || Rails.env.beta?) ? 'production' : 'stage'
files = Dir[Nets::Engine.root.join("app", "db", "fixtures", "payments").to_s + "/#{env_name}/*"]
ActiveRecord::Base.transaction do
  files.each do |file|
    seeds = HashWithIndifferentAccess.new(YAML::load_file(file))
    seeds[:methods].each do |m|
      method = Payments::Method.find_or_initialize_by(uuid: m[:uuid])
      method.class_name = m[:class_name]
      method.status = m[:status]
      method.mode = m[:mode]
      method.params = m[:params].to_hash
      method.asynchronous = m[:asynchronous]
      method.fee = m.fetch(:fee, 0)
      method.position = m.fetch(:position, 0)
      method.paymentiq_name = m.fetch(:paymentiq_name, 'Empty')
      method.paymentiq_provider_id = m.fetch(:paymentiq_provider_id, '999')
      method.request_account = m.fetch(:request_account, false)
      method.currency = m.fetch(:currency, [])
      method.save!
      method.translations.delete_all if m[:locales].blank?
      m[:locales]&.each do |l|
        I18n.locale = l[:locale]
        method.name = l[:name]
        method.deposit_first = l[:deposit_first]
        method.deposit_second = l[:deposit_second]
        method.description = l[:description]
        method.withdrawal_to_one = l[:withdrawal_to_one]
        method.withdrawal_to_many = l[:withdrawal_to_many]
        method.account_required = l[:account_required]
        method.save!
      end
      next if Rails.env.production? && !method.limits.empty?
      next if m[:limits].blank?
      method.limits.delete_all
      m[:limits].each do |l|
        limit = method.limits.new
        limit.level_id = l[:level_id]
        limit.currency = l[:currency]
        limit.status = l[:status]
        limit.level_id = l[:level_id]
        limit.level_name = l[:level_name]
        limit.currency = l[:currency]
        limit.min = l[:min]
        limit.max = l[:max]
        limit.fee = m.fetch(:fee, 0)
        limit.save!
      end
    end
  end
end