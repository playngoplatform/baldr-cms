# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nets
  class Withdrawal < Payments::Withdrawal
    def withdraw(params)
      Nets::WithdrawService.withdraw(params)
    end
  end
end
