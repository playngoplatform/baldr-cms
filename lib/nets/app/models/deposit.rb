# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Nets
  class Deposit < Payments::Deposit
    def url
      ''
    end

    def deposit(params)
      Nets::DepositService.deposit(params)
    end
  end
end
