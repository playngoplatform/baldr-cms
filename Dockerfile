FROM ruby:2.6.4

# Set an environment variable to store where the app is installed to
# inside of the Docker image.
ENV INSTALL_PATH /app

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
# Set in production mode
ENV RAILS_ENV production

# Install dependencies:
# - build-essential: To ensure certain gems can be compiled
# - bundler: ensure most recent version is installed
# - nodejs: Compile assets
RUN apt-get update \
    && apt-get install -qq -y build-essential git-core curl locales vim --fix-missing --no-install-recommends \
    && curl -sL https://deb.nodesource.com/setup_6.x | bash \
    && apt-get install -qq -y nodejs \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install yarn \
    && apt-get install -qq software-properties-common \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && locale-gen \
    && mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH
COPY lib ./lib
COPY Gemfile Gemfile.lock ./ 
RUN gem install bundler && bundle install --jobs 20 --retry 5
COPY . ./
# Fix rake stuff when we have the time
#RUN $(rake secret > ../.skb)

ENTRYPOINT bin/entry bundle exec rails server
