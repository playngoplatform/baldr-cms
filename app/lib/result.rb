# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Result < SimpleDelegator
  def initialize(success, object = nil)
    @success = success
    if object.nil?
      super(result: nil)
    else
      super(object)
    end

  end

  def success?
    @success
  end

  def failure?
    !@success
  end

end