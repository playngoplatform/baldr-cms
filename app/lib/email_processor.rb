# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class EmailProcessor
  def initialize(email)
    @email = email
  end

  def process
    # Check FROM
    # return unless @email.from[:email] == 'support@....'
    #  @email.raw_html

    data = YAML.safe_load(@email.raw_text || '')

    return unless @email.subject.present?
    return unless EmailTemplate.pluck(:action).include?(@email.subject)

    # Set Language
    I18n.locale = data['locale'].strip.downcase

    template = EmailTemplate.where('lower(action) = ?', @email.subject.strip.downcase).first
    return unless template

    # Set hostname
    player = Player.includes(:host).find_by(id: data['user_id'])
    return unless player

    data['host'] = player.host.name
    data['mirror'] = player.host.mirror&.name

    body = template.body
    data.each do |key, value|
      body.gsub! "[[#{key.upcase}]]", value.to_s
    end

    Rails.logger.debug("Send email to #{data['email']}. Action: #{@email.subject}")
    TransactionMailer.action('noreply@bejoynd.com',
                             data['email'],
                             template.subject,
                             body).deliver_now
  end
end
