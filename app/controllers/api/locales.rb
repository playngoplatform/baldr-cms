# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Locales < Grape::API
    helpers SharedParams
    formatter :json, ->(object, env) { object }

    namespace :locales do

      desc 'Translations for frontend'
      params do
        use :locale
      end
      get '/' do
        locale = Locale.find_by(mode: :frontend)
        locale&.content
      end

      desc 'List of available translations'

      get 'list' do
        { locales: I18n.available_locales.map(&:to_s) }.to_json
      end
    end
  end
end
