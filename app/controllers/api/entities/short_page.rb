# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  module Entities
    class ShortPage < Grape::Entity
      expose :name, documentation: { type: "String" }
    end


  end
end