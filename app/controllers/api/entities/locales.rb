# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  module Entities
    class Locales < Grape::Entity
      expose :locale
    end
  end
end