# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  module Entities
    class Pages < Grape::Entity
      expose :games, documentation: { type: "Array"} do

      end
      expose :categories, documentation: { type: "Array"} do

      end
      expose :carousels, documentation: { type: "Array"} do

      end
      expose :providers, documentation: { type: "Array"} do

      end
      expose :settings, documentation: { type: "Array"} do

      end

    end


  end
end