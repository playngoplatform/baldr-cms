# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Banners < Grape::API
    helpers SharedParams
    namespace :images do
      desc 'Banner list'
      params do
        use :locale
      end
      get :banners do
        Banner.active.map(&:to_hash)
      end
    end
  end
end
