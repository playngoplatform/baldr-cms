# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Games < Grape::API
    helpers SharedParams
    namespace :games do
      desc 'Игры',
           headers: {
             'Auth-Token' => {
               description: 'Auth Token',
               required: false
             }
           }
      params do
        use :locale
      end
      get '/' do
        game_data = ::GameDataService.call(current_session)
        mapped_games = []
        game_data[:games].each do |game|
          mapped_game = {
            category: game[:category],
            locked: game[:locked],
            provider: game[:provider],
            name: game[:name],
            gameId: game[:game_id],
            isMobile: game[:is_mobile],
            isDesktop: game[:is_desktop],
            imageAlt: game[:image_alt],
            thumbnail: game[:thumbnail],
            width: game[:width],
            height: game[:height]
          }
          mapped_games << mapped_game
        end
        {
          games: mapped_games,
          carousels: game_data[:carousels],
          providers: game_data[:providers]
        }
      end

      desc 'Список категорий игр'

      params do
        use :locale
      end
      get :categories do
        categorie_ids = Game.where(enabled: true).pluck(:category_id).uniq
        categorie_ids += GameCategory.where(code: %w[last_played new_game popular]).pluck(:id)
        GameCategory.where(id: categorie_ids).with_translations.map(&:to_hash)
      end

      desc 'Ссылка на запуск игр',
           headers: {
             'Auth-Token' => {
               description: 'Auth Token',
               required: false
             }
           }

      params do
        requires :gameId,
                 type: String,
                 values: ->(gameId) { Game.exists?(game_id: gameId) },
                 desc: 'Game ID'
        use :locale
        requires :desktop,
                 type: Boolean,
                 default: true,
                 desc: 'Game is played on a desktop computer'
        requires :provider,
                 type: String,
                 values: { value: -> { GameProvider.pluck(:code) } },
                 default: 'playngo',
                 desc: 'Provider'
        requires :mode,
                 type: String,
                 values: %w[demo real],
                 desc: 'Game mode'
        optional :width,
                 type: Integer,
                 values: 0..7680,
                 default: 1024,
                 desc: 'Width of the game launcher'
        optional :height,
                 type: Integer,
                 values: 0..4320,
                 default: 768,
                 desc: 'Height of the game launcher'
      end

      get :play_url do
        ip_country_response = ::PlayerService.lookup_ip(request.ip)
        allowed_countries = ::GeoLocation.where(status: true).pluck(:country_code)
        current_player_session = current_session
        if current_player_session && ip_country_response && allowed_countries.length > 0
          ip_country = ip_country_response.to_hash
          server_error!('Not allowed to play from your country', 403) unless allowed_countries.include? ip_country[:country_code]
        elsif current_player_session && allowed_countries.length > 0
          server_error!(response[:error])
        elsif current_player_session && !current_player.protection_limit_set
          server_error!('Missing deposit limit', 403)
        end
        params[:serverUrl] = server_url
        params[:currentSession] = current_player_session
        params[:currentPlayer] = current_player
        begin
          GameService::generate_play_url(params)
        rescue ::GameService::NotValidGameProvider => exception
          server_error!(exception.message, 403)
        end
      end

      desc 'Get game'
      params do
        use :locale
        requires :game_id,
                 type: String,
                 values: { value: -> { Game.pluck(:game_id) } },
                 desc: 'Game id'
      end

      get :game do
        game = Game.find_by(game_id: params[:game_id])
        game.to_hash
      end
    end
  end
end
