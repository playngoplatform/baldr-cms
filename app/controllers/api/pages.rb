# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Pages < Grape::API
    helpers SharedParams
    namespace :pages do
      desc 'Page ids'

      get :ids do
        Page.pluck(:name)
      end

      desc 'Get page by id' do
      end
      params do
        use :locale
        requires :id,
                 type: String,
                 desc: 'Identifier',
                 values: { value: -> { Page.pluck(:name) } }
      end
      get :page do
        page = Page.find_by(name: params[:id])
        content = page.content.html_safe
        content.gsub! '[[SERVER_URL]]', server_url

        {
          content: ERB.new(content).result(binding),
          title: page.title,
          meta_title: page.meta_title,
          meta_description: page.meta_description
        }
      end

      desc 'Get all pages for the support section' do
      end

      params do
        use :locale
      end

      get :support do
        if current_session
          level_id = ::PlayerService.get_player_level(current_session).fetch(:level_id, 0)
          currency = current_player.currency
        else
          level_id = nil
          currency = nil
        end
        result = {}
        result[:pages] = Page.includes(:translations)
                             .where(name: %w[bonus payment_rules contacts get_password])
                             .map(&:to_hash)
        result[:deposit_limits] = DepositLimitsService.call(level_id, currency)
        result[:withdrawal_limits] = WithdrawalLimitsService.call(level_id, currency)
        result
      end
    end
  end
end
