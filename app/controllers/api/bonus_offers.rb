# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class BonusOffers < Grape::API
    helpers SharedParams
    namespace :bonus_offers do

      desc 'Get all bonus offers'
      params do
        use :locale
        optional :state, type: String, desc: 'State', default: nil
        optional :limit, type: Integer, desc: 'Limit', default: nil
        optional :skip, type: Integer, desc: 'Skip', default: nil
      end
      get '/' do
        response = ::PlayerService.get_bonus_offer({
          session: current_session,
          state: params[:state],
          skip: params[:skip],
          limit: params[:limit]
        })
        if response.success?
          bonus_offers = {}
          bonus_offers[:bonusOffers] = response[:bonus_offers].map do |offer|
            mappedOffer = {}
            mappedOffer[:id] = offer[:id]
            mappedOffer[:name] = offer[:name]
            mappedOffer[:description] = offer[:description]
            mappedOffer[:state] = offer[:state]
            mappedOffer[:activated] = offer[:activated]
            mappedOffer[:cleared] = offer[:cleared]
            mappedOffer[:expired] = offer[:expired]
            mappedOffer[:pointsUsed] = offer[:points_used]
            mappedOffer[:pointsMax] = offer[:points_max]
            mappedOffer[:pointsRequired] = offer[:points_required]
            mappedOffer[:pointsAvailable] = offer[:points_available]
            mappedOffer[:expires] = offer[:expires]
            mappedOffer[:bonusAmount] = offer[:bonus_amount]
            mappedOffer[:bonusFraction] = offer[:bonus_fraction]
            mappedOffer[:wageringReq] = offer[:wagering_req]
            mappedOffer[:wagered] = offer[:wagered]
            mappedOffer[:wageringOrder] = offer[:wagering_order]
            mappedOffer[:bonusAmountMax] = offer[:bonus_amount_max]
            mappedOffer
          end
          bonus_offers
        else
          server_error!(response[:error])
        end
      end

      desc 'Claim bonus offer by id'
      params do
        use :locale
        requires :bonusOfferId, type: Integer, desc: 'Bonus offer id'
      end
      post :claim do
        response = ::PlayerService.claim_bonus(
          current_session,
          params[:bonusOfferId],
        )
        if response.success?
          response
        else
          server_error!(response[:error])
        end
      end

      desc 'Void bonus offer by id or reject all if offer id is nil'
      params do
        use :locale
        optional :bonusOfferId, type: Integer, desc: 'Bonus offer id', default: nil
      end
      post :void do
        response = ::PlayerService.bonus_void(
          current_session,
          params[:bonusOfferId],
        )
        if response.success?
          response
        else
          server_error!(response[:error])
        end
      end
    end
  end
end
  