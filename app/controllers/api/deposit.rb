# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Deposit < Grape::API
    helpers SharedParams

    namespace :deposit do

      desc 'Available payment options', {
          headers: {
              "Auth-Token" => {
                  description: "Auth Token",
                  required: true
              }
          }
      }
      params do
        use :locale
      end
      get do
        authenticate!
        Payments::Method.deposits(current_player)
      end

      desc 'Retrieve redirect address'
      params do
        requires :method,
                 regexp: /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/,
                 values: { value: -> { Payments::Method.deposit.enabled.pluck(:uuid) } },
                 desc: 'Method'
        requires :amount,
                 type: Integer,
                 values: 1..1_000_000,
                 desc: 'Amount to deposit'
        requires :success_url,
                 type: String,
                 desc: 'URL after successful deposit',
                 default: API_URL
        requires :failure_url,
                 type: String,
                 desc: 'URL after failed deposit',
                 default: API_URL
        optional :bonus_code,
                 regexp: /^[a-zA-Z0-9]+$/,
                 desc: 'Bonus code'
      end

      post do
        authenticate!
        method = Payments::Method.find_by(uuid: params[:method])
        player = current_player
        if method.respond_to?(:class_name)
          amount = {
            value: params[:amount],
            currency: player.currency
          }
          provider = method[:class_name].constantize.new
          response = provider.deposit({
            session: current_session,
            amount: amount,
            self_hosted: false,
            bonus_code: params[:bonus_code],
            redirect_url: params[:success_url],
            redirect_on_error: false
          })
          if response.success?
            response
          else
            server_error!(response[:error])
          end
        else
          error!({ error: 'Not a valid provider' }, 500)
        end
      end

    end
  end
end
