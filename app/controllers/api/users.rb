# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Users < Grape::API
    helpers do 
      def update_balance(player)
        summary = ::PlayerService.account_summary(player.session_id)
        player.total_balance = summary[:withdrawable_balance]
        player.bonus_balance = summary[:bonus_balance]
        player.withdrawable_balance = summary[:withdrawable_balance]
        player.wagering_requirement = summary[:total_active_wagering_requirement]
        player.bonus_wagered_fraction = summary[:total_active_bonus_wagered_fraction]
        player.currency = summary[:currency]
        player.save!
      end

      def update_protection_limit_set(player)
        limits = ::PlayerService.get_player_protection_limits(player.session_id)
        deposit_limits = limits[:current_limits][:deposit_limits]
        player.protection_limit_set = !deposit_limits[:day].nil? || !deposit_limits[:week].nil? || !deposit_limits[:month].nil?
        player.save!
        limits
      end

      def valid_deposit_limits(deposit_limits)
        (!deposit_limits[:day].nil? && deposit_limits[:day] > 0) ||
        (!deposit_limits[:week].nil? && deposit_limits[:week] > 0) ||
        (!deposit_limits[:month].nil? && deposit_limits[:month] > 0)
      end

      def map_register_params_to_snake_case(params)
        {
          login: params[:login],
          password: params[:password],
          nickname: params[:nickname],
          country: params[:country],
          birthdate: params[:birthdate],
          first_name: params[:firstName],
          middle_name: params[:middleName],
          last_name: params[:lastName],
          address: params[:address],
          zip: params[:zip],
          city: params[:city],
          phone: params[:phone],
          gender: params[:gender],
          currency: params[:currency],
          language: params[:language],
          email: params[:email],
          national_id: params[:nationalId],
          external_id: params[:externalId]
      }
      end

      def get_logged_in_player_info(result)
        session_id = result[:response][:session_id]
        level_id = ::PlayerService.get_player_level(session_id).fetch(:level_id, 0)
        player = ::Player.find_or_initialize_by(id: result[:response][:user_id])
        @current_player = player
        player.login = result[:response][:user_nickname]
        player.nickname = result[:response][:user_nickname]
        player.host = Host.find_by_name(server_url) unless player.host_id
        player.level_id = level_id
        player.currency ||= 'DKK'
        player.save!
        player.session_id = session_id
        set_auth_token(session_id)
        update_protection_limit_set(player)
        update_balance(player)
        player.to_hash
      end
    end

    namespace :users do
      desc 'Login with username and password' do
        success  headers: { 'Auth-Token': { description: 'Auth token' } }
      end
      params do
        requires :username,
                 type: String,
                 desc: 'Username',
                 regexp: /[a-zA-Z0-9]+/
        requires :password,
                 type: String,
                 desc: 'Password'
      end
      post :login do
        result = ::PlayerService.login({
          username: params[:username],
          password: params[:password],
          client_ip: request.ip,
          user_agent: request.user_agent
        })
        if result.success?
          get_logged_in_player_info(result)
        else
          error!(result[:error], 403)
        end
      end

      desc 'Login with a token' do
        success  headers: { 'Auth-Token': { description: 'Auth token' } }
      end
      params do
        requires :tokenType,
                 type: String,
                 desc: 'Type of token (ex. NemID)'
        requires :token,
                 type: String,
                 desc: 'Token used to authenticate the player'
      end
      post :login_with_token do
        result = ::PlayerService.login_with_token({
          token_type: params[:tokenType],
          token: params[:token],
          client_ip: request.ip,
          user_agent: request.user_agent
        })
        if result.success?
          get_logged_in_player_info(result)
        else
          error!(result[:error], 403)
        end
      end

      desc 'Logout', {
          headers: {
              "Auth-Token" => {
                  description: "Auth Token",
                  required: true
              }
          }
      }
      post :logout do
        authenticate!
        ::PlayerService.logout(current_session)
        body false
      end

      desc 'Refresh player data', {
          headers: {
              "Auth-Token" => {
                  description: "Auth Token",
                  required: true
              }
          }
      }
      post :refresh do
        authenticate!
        update_balance(current_player)
        current_player.to_hash
      end

      desc 'Register a new player'
      params do
        with(type: String) do
          requires :login,
                   desc: 'Login',
                   regexp: /[a-zA-Z0-9]+/,
                   min_length: 1,
                   length: 64
          requires :password,
                   desc: 'Password',
                   min_length: 6,
                   length: 30
          optional :currency,
                   desc: 'Currency',
                   values: %w[RUB UAH DKK USD],
                   default: 'DKK'
          optional :language,
                   desc: 'Language',
                   values: I18n.available_locales.map(&:to_s)
          requires :email,
                   desc: 'Email',
                   regexp: /[^\s]@[^\s]/
          optional :nickname,
                   desc: 'Nickname',
                   regexp: /[a-zA-Z0-9_&]+/,
                   min_length: 2,
                   length: 12
          optional :country,
                   desc: 'Country',
                   values: ISO3166::Country.translations.map { |key, _value| key },
                   default: 'DK'
          optional :firstName,
                   desc: 'First name',
                   min_length: 2,
                   length: 50
          optional :middleName,
                   desc: 'Middle name',
                   min_length: 2,
                   length: 50
          optional :lastName,
                   desc: 'Last name',
                   min_length: 2,
                   length: 50
          optional :address,
                   desc: 'Address',
                   min_length: 2,
                   length: 50
          optional :city,
                   desc: 'City',
                   min_length: 2,
                   length: 50
          optional :zip,
                   desc: 'Zip code'
          optional :phone,
                   desc: 'Phone number',
                   min_length: 6,
                   length: 50
          optional :gender,
                   desc: 'Gender',
                   values: %w[m f]
          optional :nationalId,
                   desc: 'National ID',
                   min_length: 2
          optional :externalId,
                   desc: 'External ID',
                   min_length: 2
          optional :intendedGambling,
                   desc: 'Intended gambling per week'
        end
        optional :birthdate,
                 type: DateTime,
                 desc: 'Date of birth. Example: 1980-01-01'
        with(type: Integer) do
          optional :depositLimitSingle,
                   desc: 'Highest allowed amount in a single deposit'
          optional :depositLimitDaily,
                   desc: 'Allowed deposit amount within a day'
          optional :depositLimitWeekly,
                   desc: 'Allowed deposit amount within a week'
          optional :depositLimitMonthly,
                   desc: 'Allowed deposit amount within a month'
        end
      end
      post :register do
        error! 'Access Denied', 401 if current_session
        limits = {
          deposit_limits: {
            single: params[:depositLimitSingle],
            day: params[:depositLimitDaily],
            week: params[:depositLimitWeekly],
            month: params[:depositLimitMonthly]
          },
          loss_limits: {},
          transfer_limits: {}
        }
        error!('Deposit limits are not valid', 400) unless valid_deposit_limits(limits[:deposit_limits])
        register_params = map_register_params_to_snake_case(params)
        register_response = ::PlayerService.register_player(register_params, request.ip, request.user_agent)
        error!(register_response[:error], 403) unless register_response.success?
        player = ::Player.new(id: register_response[:response][:user_id])
        player.login = params[:login]
        player.nickname = params[:nickname] || params[:login][0..11]
        player.host = Host.find_or_create_by!(name: server_url)
        player.currency = params[:currency]
        player.save!
        login_response = ::PlayerService.login({
          login: params[:login],
          password: params[:password],
          client_ip: request.ip,
          user_agent: request.user_agent
        })
        server_error!(login_response[:error]) unless login_response.success?
        session_id = login_response[:response][:session_id]
        limit_response = ::PlayerService.set_player_protection_limits(session_id, limits, nil, nil)
        server_error!(limit_response[:error]) unless limit_response.success?
        if params[:intendedGambling]
          ::PlayerService.set_player_property(session_id, 'intendedgambling', params[:intendedGambling])
        end
        level_id = ::PlayerService.get_player_level(session_id).fetch(:level_id, 0)
        @current_player = player
        player.level_id = level_id
        player.session_id = session_id
        set_auth_token(session_id)
        update_protection_limit_set(player)
        update_balance(player)
        player.to_hash
      end

      desc 'Get fields for registration'
      get :register do
        error! 'Access Denied', 401 if current_session
        default_country_code = ::PlayerService.lookup_ip(request.ip).fetch(:country_code, 'DK')
        default_country = ISO3166::Country.find_country_by_alpha2(default_country_code).alpha2
        default_currency = currency_for_country(default_country_code)
        bonuses = [{code: '', name: I18n.t('registration.default_bonus')}]
        bonuses.concat(Bonus.active_now.where(registration: true).map { |b| { code: b.code, name: b.title } })
        {
          login: "p#{DateTime.now.to_i.to_s[0..12]}",
          password: SecureRandom.alphanumeric(6),
          currency: {
            default: default_currency,
            list: default_currencies,
            countries: default_currencies.inject(Hash.new){|k,v| k[v]=country_for_currency(v);k}
          },
          country: {
            default: default_country,
            list: default_countries,
            currencies: default_countries.inject(Hash.new){|k,v| k[v[:code]]=currency_for_country(v[:code]);k}
          },
          bonus: {
            default: Setting['default_bonus'],
            list: bonuses
          }
        }
      end

      desc 'Request reset password'
      params do
        requires :login,
                 desc: 'player login',
                 regexp: /[a-zA-Z0-9]+/,
                 min_length: 1,
                 length: 64
      end
      post :reset_password do
        response = ::PlayerService.reset_password(params[:login])
        if response.success?
          body false
        else
          server_error!(response[:error]) unless response[:error].include? 'Wrong username'
        end
      end

      desc 'Set password'
      params do
        requires :token,
                 desc: 'Token',
                 min_length: 1,
                 length: 64
        requires :password,
                 desc: 'Password',
                 min_length: 6,
                 length: 30
      end
      post :set_password do
        ::PlayerService.set_password(params[:token], params[:password], request.ip)
        body false
      end

      desc 'Email verification'
      params do
        optional :email_id,
                 desc: 'Email ID',
                 type: String
        optional :user_id,
                 desc: 'User ID',
                 type: Integer,
                 values: 1..9999999
      end
      post :verify do
        ::PlayerService.verify_player(params[:email_id], request.ip, params[:user_id])
      end

      desc 'Self Exclude player'
      params do
        requires :lock_until,
                 desc: 'Locked Until',
                 type: String
      end
      post :self_exclude do
        response = ::PlayerService.self_exclude(current_session, params[:lock_until])
        if response.success?
          response
        else
          server_error!(response[:error])
        end
      end

      desc 'Get player protection limits'
      get :protection_limits do
        response = ::PlayerService.get_player_protection_limits(current_session)
        if response.success?
          current_limits = {}
          current_limits[:depositLimits] = response[:current_limits][:deposit_limits]
          current_limits[:lossLimits] = response[:current_limits][:loss_limits]
          current_limits[:transferLimits] = response[:current_limits][:transfer_limits]
          current_limits[:maxSessionTime] = response[:current_limits][:max_session_time]
          current_limits[:realityCheckFrequency] = response[:current_limits][:reality_check_frequency]
          pending_limits = {}
          pending_limits[:depositLimits] = response[:pending_limits][:deposit_limits]
          pending_limits[:lossLimits] = response[:pending_limits][:loss_limits]
          pending_limits[:transferLimits] = response[:pending_limits][:transfer_limits]
          pending_limits[:maxSessionTime] = response[:pending_limits][:max_session_time]
          pending_limits[:realityCheckFrequency] = response[:pending_limits][:reality_check_frequency]
          limits = {
            currentLimits: current_limits,
            pendingLimits: pending_limits
          }
        else
          server_error!(response[:error])
        end
      end

      desc 'Update player protection limits'
      params do
        requires :limits,
                 desc: 'Limits'
        optional :lockUntil,
                 desc: 'Lock until',
                 default: nil
        optional :lockReasonId,
                 desc: 'Lock reason id',
                 default: nil
      end
      put :protection_limits do
        deposit_limits = params[:limits][:depositLimits]
        is_valid_deposit_limits = valid_deposit_limits(deposit_limits)
        if !is_valid_deposit_limits
          error!({ error: 'A deposit limit must be set' }, 400)
        end
        limits = {}
        limits[:deposit_limits] = params[:limits][:depositLimits]
        limits[:loss_limits] = params[:limits][:lossLimits]
        limits[:transfer_limits] = params[:limits][:transferLimits]
        limits[:max_session_time] = params[:limits][:maxSessionTime]
        limits[:reality_check_frequency] = params[:limits][:realityCheckFrequency]
        response = ::PlayerService.set_player_protection_limits(current_session, limits, params[:lockUntil], params[:lockReasonId])
        if response.success?
          current_player.protection_limit_set = is_valid_deposit_limits
          current_player.save!
          response
        else
          server_error!(response[:error])
        end
      end

      desc 'Get player bet and win summary'
      params do
        requires :from,
                 desc: 'Start time for the summary'
        requires :from,
                 desc: 'End time for the summary'
      end
      get :bet_win_summary do
        response = ::PlayerService.get_player_bet_win({
          session: current_session,
          from: params[:from],
          to: params[:to]
        })
        if response.success?
          betWinData = {}
          betWinData[:bonusMoneyBet] = response[:bonus_money_bet]
          betWinData[:bonusMoneyWin] = response[:bonus_money_win]
          betWinData[:playerCurrency] = response[:player_currency]
          betWinData[:realMoneyBet] = response[:real_money_bet]
          betWinData[:realMoneyWin] = response[:real_money_win]
          betWinData
        else
          server_error!(response[:error])
        end
      end
    end
  end
end
