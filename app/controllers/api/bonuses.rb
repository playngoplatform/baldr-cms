# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Bonuses < Grape::API
    helpers SharedParams
    namespace :bonuses do

      desc 'List of bonuses'
      params do
        use :locale
        optional :currency, type: String, desc: 'Currency'
        optional :user_id, type: Integer, desc: 'Player id from Maven API'
      end
      get :data do
        header_page = Page.find_by(category: :bonus_header)
        footer_page = Page.find_by(category: :bonus_footer)

        {
          title: header_page.try(:title),
          header: header_page.try(:content),
          footer: footer_page.try(:content),
          items: Bonus.active(currency: params[:currency],
                              user_id: params[:user_id],
                              short: true),
          meta_title: header_page.try(:meta_title),
          meta_description: header_page.try(:meta_description)
        }
      end

      desc 'Recieve bonus by id'
      params do
        use :locale
        requires :id,
                 type: String,
                 desc: 'Bonus code'
        optional :currency, type: String, desc: 'Currency'
        optional :user_id, type: Integer, desc: 'Player Id from Maven API'
      end
      get :item do
        bonus = Bonus.published.find_by(page_id: params[:id])
        bonus.present? ? bonus.to_h(currency: params[:currency], user_id: params[:user_id]) : {}
      end
    end
  end
end
