# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Common < Grape::API

    namespace :common do
      desc 'Ping'
      get :ping do
        'pong'
      end
    end
  end
end
