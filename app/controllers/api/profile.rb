# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Profile < Grape::API
    helpers SharedParams
    namespace :profile do
      desc 'Return player profile', {
          headers: {
              "Auth-Token" => {
                  description: "Auth Token",
                  required: true
              }
          }
      }
      get '/' do
        authenticate!
        user_info = ::PlayerService.get_player(current_session).except!(
          :middle_name,
          :birthday,
          :mobile_model,
          :mobile_cc,
          :region
        )
        user_info[:firstName] = user_info.delete :first_name
        user_info[:lastName] = user_info.delete :last_name
        user_info[:mobileNumber] = user_info.delete :mobile_no
        {
          userInfo: user_info,
          countries: default_countries,
          locales: I18n.available_locales.map{ |l| l.to_s.upcase }
        }
      end

      desc 'Get game history', {
          headers: {
              "Auth-Token" => {
                  description: "Auth Token",
                  required: true
              }
          }
      }
      params do
        optional :from,
                 type: DateTime,
                 desc: 'From date'
        optional :to,
                 type: DateTime,
                 desc: 'To date'
        optional :skip,
                 type: Integer,
                 desc: 'Number of records to skip',
                 default: 0
        optional :limit,
                 type: Integer,
                 desc: 'Limit of records',
                 default: 50
      end
      get :game_history do
        authenticate!
        ::PlayerService.get_game_history({
          session: current_session,
          from: params[:from],
          to: params[:to],
          skip: params[:skip],
          limit: params[:limit]
        })
      end

      desc 'Get orders', {
          headers: {
              "Auth-Token" => {
                  description: "Auth Token",
                  required: true
              }
          }
      }
      params do
        optional :to,
                 type: DateTime,
                 desc: 'To date'
        optional :skip,
                 type: Integer,
                 desc: 'Number of orders to skip',
                 default: 0
        optional :limit,
                 type: Integer,
                 desc: 'Limit orders',
                 default: 50

      end
      get :orders do
        authenticate!
        queryParams = {
          session: current_session,
          from: params[:from],
          to: params[:to],
          skip: params[:skip],
          limit: params[:limit]
        }
        response = ::CashierService::get_orders(queryParams)
        response = response.map do |order|
          mappedOrder = {}
          mappedOrder[:amount] = order[:amount]
          mappedOrder[:currency] = order[:currency]
          mappedOrder[:endDate] = order[:end_date]
          mappedOrder[:provider] = order[:provider]
          mappedOrder[:startDate] = order[:start_date]
          mappedOrder[:state] = order[:state]
          mappedOrder
        end
        response
      end

      desc 'Change profile settings', {
          headers: {
              "Auth-Token" => {
                  description: "Auth Token",
                  required: true
              }
          }
      }

      params do
        with(type: String) do
          requires :password,
                   desc: 'Password',
                   min_length: 6,
                   length: 30
          optional :language,
                   desc: 'Player language',
                   values: I18n.available_locales.map{ |l| l.to_s.upcase },
                   default: I18n.locale.to_s.upcase
          optional :email,
                   desc: 'Email',
                   regexp: /[^\s]@[^\s]/
          optional :nickname,
                   desc: 'Nickname',
                   regexp: /[a-zA-Z0-9_&]+/,
                   min_length: 2,
                   length: 12
          optional :country,
                   desc: 'Country',
                   values: ISO3166::Country.translations.map { |key, _value| key }
          optional :first_name,
                   desc: 'First name',
                   min_length: 2,
                   length: 50
          optional :last_name,
                   desc: 'Last name',
                   min_length: 2,
                   length: 50
          optional :mobile_no,
                   desc: 'Mobile phone number',
                   min_length: 6,
                   length: 30
          optional :gender,
                   desc: 'Gender',
                   values: %w[m f],
                   default: 'm'
        end
      end

      post '/' do
        authenticate!

        result = ::PlayerService.update_player(current_session, params)
        if result.success?
          'Profile is updated'
        else
          error!(result[:error], 500)
        end
      end

    end
  end
end
