# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  module SharedParams
    extend Grape::API::Helpers
    params :locale do
      optional :locale,
               type: String,
               values: I18n.available_locales.map(&:to_s),
               desc: 'If not set, then use <b>Accept-Language</b>'
    end

    params :deposit_callback do
      requires :transaction_id,
               regexp: /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/,
               values: { value: -> { Payments::Deposit.unfinished.pluck(:transaction_id) } },
               desc: 'Transaction id'
    end
  end
end
