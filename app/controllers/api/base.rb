# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Base < Grape::API
    prefix :api
    format :json

    require_dependency 'api/validators/length'
    require_dependency 'api/validators/min_length'

    use GrapeLogging::Middleware::RequestLogger,
        instrumentation_key: 'grape_logging',
        include: [GrapeLogging::Loggers::Response.new,
                  GrapeLogging::Loggers::FilterParameters.new]

    helpers do
      def server_error!(message = 'Server error.', code = 500)
        error!({ error: message }, code, 'Content-Type' => 'text/error')
      end

      def unauthorized_error!
        error!('401 Unauthorized', 401)
      end

      def extract_locale_from_header
        request.headers['Accept-Language'].try(:scan, /^[a-z]{2}/).try(:first)
      end

      def server_url
        url = request.referer || request.base_url
        url = "https://#{url}" if URI.parse(url).scheme.nil?
        host = URI.parse(url).host.downcase
        host = host.start_with?('www.') ? host[4..-1] : host
        "https://#{host}"
      end

      def auth_token
        headers['Auth-Token']
      end

      def set_auth_token(value)
        header 'Auth-Token', value
      end

      def current_player
        return nil unless auth_token
        @current_player ||= Session.find_by_guid(auth_token)&.player
      end

      def current_session
        session = current_player&.session
        return nil unless session
        return session.guid if session.updated_at > 5.minutes.ago
        result = ::PlayerService.refresh_session(session.guid)
        return nil unless result.success?
        session.touch
        session.guid
      end

      def authenticate!
        error!('401 Unauthorized', 401) unless current_session
      end

      def default_currencies
        %w[RUB EUR DKK UAH]
      end

      def default_countries
        ISO3166::Country.translations(I18n.locale).map { |k, v| { code: k, name: v } }
      end

      def currency_for_country(code)
        case code
        when 'RU'
          'RUB'
        when 'UA'
          'UAH'
        when 'DK'
          'DKK'
        else
          'EUR'
        end
      end

      def country_for_currency(currency)
        case currency
        when 'RUB'
          'RU'
        when 'UAH'
          'UA'
        when 'DKK'
          'DK'
        else
          ''
        end
      end
    end

    after_validation do
      I18n.locale =  params[:locale] || extract_locale_from_header || I18n.default_locale
    end

    mount API::Common
    mount API::Games
    mount API::Pages
    mount API::Bonuses
    mount API::Banners
    mount API::Locales
    mount API::Users
    mount API::Profile
    mount API::Deposit
    mount API::Withdrawal
    mount API::BonusOffers

    add_swagger_documentation(api_version: 'v1',
                              doc_version: '0.0.3',
                              hide_documentation_path: true,
                              add_version: true,
                              hide_format: true,
                              array_use_braces: true,
                              info: {
                                title: 'API'
                              })
  end
end
