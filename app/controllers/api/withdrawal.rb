# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module API
  class Withdrawal < Grape::API
    helpers SharedParams

    namespace :withdrawal do
      desc 'Available withdrawal methods'
      params do
        use :locale
      end
      get do
        authenticate!
        Payments::Method.withdrawals(current_player)
      end

      desc 'Create a withdrawal'
      params do
        requires :method,
                 regexp: /\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/,
                 values: { value: -> { Payments::Method.withdrawal.enabled.pluck(:uuid) } },
                 desc: 'Method'
        requires :amount,
                 type: Integer,
                 values: 1..1_000_000,
                 desc: 'Amount to withdrawal'
        optional :bonus_code,
                 regexp: /^[a-zA-Z0-9]+$/,
                 desc: 'Bonus code'
      end

      post do
        authenticate!
        method = Payments::Method.find_by(uuid: params[:method])
        error!({ error: 'Not a valid provider' }, 500) if method == nil
        player = current_player
        amount = {
          value: params[:amount],
          currency: player.currency
        }
        item = method[:class_name].constantize.new
        response = item.withdraw({ session: current_session, amount: amount})
        if response.success?
          response
        else
          server_error!(response[:error])
        end
      end
    end
  end
end
