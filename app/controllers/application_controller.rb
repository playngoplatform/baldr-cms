# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  # before_action :set_paper_trail_whodunnit
  before_action :set_notification


  def set_admin_locale
    # I18n.locale = current_user&.locale || params[:locale] || extract_locale || I18n.default_locale
    I18n.locale = current_admin_user&.locale || params[:locale] || extract_locale || I18n.default_locale
    # if request.referer
    #   url_hash = Rails.application.routes.recognize_path URI(request.referer).path
    #   url_hash[:locale] = I18n.locale
    #   redirect_to url_hash
    # end
  end

  def client_ip
    request.remote_ip
  end

  def server_url
    url = request.referer || request.base_url
    url = "https://#{url}" if URI.parse(url).scheme.nil?
    host = URI.parse(url).host.downcase
    host = host.start_with?('www.') ? host[4..-1] : host
    "https://#{host}"
  end

  # def user_for_paper_trail
  #   user_signed_in? ? current_user.try(:id) : 'Unknown user'
  # end

  def access_denied(exception)
    redirect_to root_path, alert: exception.message
  end

  private

  def extract_locale
    request.env['HTTP_ACCEPT_LANGUAGE'].try(:scan, /^[a-z]{2}/).try(:first)
  end

  def set_time_zone
    Time.zone = current_admin_user.timezone if current_admin_user
  end

  def set_notification
    request.env['exception_notifier.exception_data'] = {'Environment' => Rails.env,
                                                        'Request URL' => request.original_url,
                                                        'Request Params' => request.filtered_parameters,
                                                        'Request IP' => request.ip
    }
  end

end
