# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class CashierService
  def self.get_orders(params)
    Rails.application.config.plugins.cashier_module::V1::GetOrdersService.call(params)
  end
end