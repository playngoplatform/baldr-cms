# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class PlayerService
  def self.login(params)
    Rails.application.config.plugins.player_module::V1::LoginService.call(params[:username], params[:password], params[:client_ip], params[:user_agent])
  end

  def self.login_with_token(params)
    Rails.application.config.plugins.player_module::V1::LoginWithTokenService.call(params[:token_type], params[:token], params[:client_ip], params[:user_agent])
  end

  def self.logout(session)
    Rails.application.config.plugins.player_module::V1::LogoutService.call(session)
  end

  def self.bonus_void(session, bonus_offer_id)
    Rails.application.config.plugins.player_module::V1::BonusVoidService.call(session, bonus_offer_id)
  end

  def self.claim_bonus(session, bonus_offer_id)
    Rails.application.config.plugins.player_module::V1::ClaimBonusService.call(session, bonus_offer_id)
  end

  def self.get_bonus_offer(params)
    Rails.application.config.plugins.player_module::V1::GetBonusOffersService.call(params[:session], params[:state], params[:skip], params[:limit])
  end

  def self.get_game_history(params)
    Rails.application.config.plugins.player_module::V1::GetGameHistoryService.call(params[:session], params[:from], params[:to], params[:skip], params[:limit])
  end

  def self.get_player_bet_win(params)
    Rails.application.config.plugins.player_module::V1::GetPlayerBetWinSummaryService.call(params[:session], params[:from], params[:to])
  end

  def self.get_player_level(session)
    Rails.application.config.plugins.player_module::V1::GetUserLevelService.call(session)
  end

  def self.get_player_protection_limits(session)
    Rails.application.config.plugins.player_module::V1::GetUserProtectionLimitsService.call(session)
  end

  def self.get_player(session)
    Rails.application.config.plugins.player_module::V1::GetUserService.call(session)
  end

  def self.lookup_ip(ip)
    Rails.application.config.plugins.player_module::V1::LookupIpService.call(ip)
  end

  def self.play(params)
    Rails.application.config.plugins.player_module::V1::PlayService.call(params[:session], params[:product], params[:game_id], params[:language], params[:practice ] || false, params[:client_type] || nil)
  end

  def self.refresh_session(session)
    Rails.application.config.plugins.player_module::V1::RefreshSessionService.call(session)
  end

  def self.register_player(params, ip, user_agent)
    Rails.application.config.plugins.player_module::V1::RegisterUserService.call(params, ip, user_agent)
  end

  def self.reset_password(login)
    Rails.application.config.plugins.player_module::V1::ResetUserPasswordService.call(login)
  end

  def self.set_password(token, password, client_ip)
    Rails.application.config.plugins.player_module::V1::SetUserPasswordService.call(token, password, client_ip)
  end

  def self.self_exclude(session, lock_until)
    Rails.application.config.plugins.player_module::V1::SelfExcludeService.call(session, lock_until)
  end

  def self.set_player_property(session, property_name, propert_value)
    Rails.application.config.plugins.player_module::V1::SetUserProperty.call(session, property_name, propert_value)
  end

  def self.set_player_protection_limits(session, limits, lock_until, lock_reason_id)
    Rails.application.config.plugins.player_module::V1::SetUserProtectionLimitsService.call(session, limits, lock_until, lock_reason_id)
  end

  def self.update_player(session, params)
    Rails.application.config.plugins.player_module::V1::UpdateUserService.call(session, params)
  end

  def self.account_summary(session)
    Rails.application.config.plugins.player_module::V1::UserAccountSummaryService.call(session)
  end

  def self.verify_player(email_id, client_ip, user_id)
    Rails.application.config.plugins.player_module::V1::VerifyUserService.call(email_id, client_ip, user_id)
  end
end