# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
# Пример возвращаемых данных
# {
#     name: 'Credit Card',
#     levels: ['Base','VIP', 'VIP+']
#     currencies: [
#         { currency: 'RUB',
#           ranges: ['600..70000', '600..70000', '600..70000' ]
#         },
#         { currency: 'EUR',
#           ranges: ['600..70000', '600..70000', '600..70000' ]
#         }
#     ]
# }
#
class WithdrawalLimitsService < Service
  def call(level_id, currency)
    methods = { levels: [],
                methods: [] }
    levels = if level_id
               Payments::Limit.where(level_id: level_id).order(:level_id)
             else
               Payments::Limit.order(:level_id)
             end
    methods[:levels] = levels.pluck(:level_name).uniq.map { |l| I18n.t("maven.limits.levels.#{l.parameterize.underscore}") }
    Payments::Method. includes(:translations).withdrawal.where(status: true).each do |m|
      limits = m.limits
                   .where(status: true)
                   .order(:level_id)
                   .map do |l|
        { currency: l.currency,
          level_id: l.level_id,
          min: l.min,
          max: l.max }
      end
      methods[:methods] << {
          name: m.name,
          currencies: if level_id
                        limit = limits.detect { |l| l[:level_id] == level_id && l[:currency] == currency }
                        [{
                            currency: currency,
                            ranges: ["#{limit[:min]}-#{limit[:max]}"]
                        }]
                      else
                        result = []
                        limits.map { |l| l[:currency] }.uniq.each do |currency|
                          result << { currency: currency,
                                      ranges: limits.select { |l| l[:currency] == currency }.map { |l| "#{l[:min]}-#{l[:max]}" } }
                        end
                        result
                      end
      }
    end
    methods
  end
end
