# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class GameDataService < Service
  def call(session = nil)
    list = Game.where(enabled: true)
    data = list.map(&:to_hash)

    if session
      carousels = Carousel.alive.authorized.map(&:to_hash)
    else
      carousels = Carousel.alive.unauthorized.map(&:to_hash)
    end

    providers = GameProvider.where(mode: true)

    # Popular
    popular_games = list.popular.map { |r| r.to_hash('popular') }
    popular_games.each do |game|
      if data.none? { |item| item[:category] == 'popular' && item[:game_id] == game[:game_id]}
        data << game
      end
    end

    # New
    providers.each do |p|
      new_games = list.where(provider_id: p.id, old: false).map { |r| r.to_hash('new_game') }.take(10).sort_by { |g| g[:created_at] }
      new_games.each do |game|
        if data.none? { |item| item[:category] == 'new_game' && item[:game_id] == game[:game_id]}
          data << game
        end
      end
    end

    # Previous
    if session
      history = ::PlayerService.get_game_history({
        session: session,
        from: nil,
        to: nil,
        skip: 0,
        limit: 400
      })
      names = history.map{ |h| h[:game].strip.downcase }.uniq.take(10)
      last_played_games = list.where(name: names).map { |r| r.to_hash('last_played') }
      last_played_games.each do |game|
        if data.none? { |item| item[:category] == 'last_played' && item[:game_id] == game[:game_id]}
          data << game
        end
      end
    end

    {
      games: data,
      carousels: carousels,
      providers: providers.map(&:to_hash)
    }
  end
end
