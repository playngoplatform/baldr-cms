# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Paymentiq
  class VerifyUser < CommonService
    def call(transaction)
      result = super(transaction,
                     '/verifyuser',
                     sessionId: transaction.player.session_id,
                     userId: transaction.user_id
            )

      Result.new(result.success?, message: result[:response][:errMsg])
    end
  end
end
