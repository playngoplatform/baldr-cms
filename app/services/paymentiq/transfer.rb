# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Paymentiq
  class Transfer < CommonService
    def call(transaction)
      super(transaction,
                      '/transfer',
                     userId: transaction.user_id,
                     authCode: transaction.auth_token,
                     txAmount: format_amount(transaction.amount, transaction.deposit?),
                     txAmountCy: transaction.currency,
                     txId: transaction.transaction_id,
                     txName: transaction.transaction_name,
                     provider: transaction.method_name,
                     txTypeId: transaction.method_id,
                     attributes: {
                       bonusCode: transaction.bonus_code
                     }

      )
    end
  end
end
