# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Paymentiq
  class Cancel < CommonService
    def call(transaction)
      result = super(transaction,
                     '/cancel',
                     userId: transaction.user_id,
                     authCode: transaction.auth_token,
                     txAmount: format_amount(transaction.amount, transaction.deposit?),
                     txCurrency: transaction.currency,
                     txId: transaction.transaction_id,
                     txName: transaction.transaction_name,
                     provider: transaction.method_name,
                     txTypeId: transaction.method_id
            )

      Result.new(result.success?, message: result[:response][:errMsg])
    end
  end
end
