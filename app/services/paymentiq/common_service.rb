# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Paymentiq
  class CommonService < Service
    def initialize
      @endpoint = Setting['PaymentIQ']['endpoint']
    end

    def call(transaction, url, attributes = {})
      # Лог
      Payments::Log.paymentiq.request.create!(url: url,
                                              data: attributes,
                                              player_id: attributes[:userId],
                                              logable: transaction)
      RestClient.log = Rails.logger
      response = RestClient.post(@endpoint + url,
                                 attributes.to_json,
                                 content_type: :json,
                                 user_agent: 'rest_client',
                                 accept: :json,
                                 timeout: 60,
                                 open_timeout: 60)
      result = JSON.parse(response, symbolize_names: true).with_indifferent_access

      Rails.logger.debug(result.inspect)
      Result.new(result[:success], response: result)
    rescue Errno::ECONNREFUSED, SocketError, Net::ReadTimeout
      result = { errMsg: 'bad_gateway' }
      Result.new(false, response: result)
    rescue StandardError => e
      result = { errMsg: e.message }
      Result.new(false, response: result)
    ensure
      # Лог
      Payments::Log.paymentiq.response.create!(url: url,
                                               state: result[:success] ? :successful : :error,
                                               data: result,
                                               player_id: attributes[:userId],
                                               logable: transaction)
    end

    def format_amount(amount, deposit = true)
      deposit ? amount : amount * -1
    end
  end
end
