# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Paymentiq
  class VerifyAndAuthorize < CommonService
    def call(transaction)
      verified = VerifyUser.call(transaction)
      return verified unless verified.success?

      Authorize.call(transaction)
    end
  end
end
