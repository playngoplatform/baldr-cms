# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Paymentiq
  class Authorize < CommonService
    def call(transaction)
      result = super(transaction,
                     '/authorize',
                     userId: transaction.user_id,
                     txAmount: format_amount(transaction.amount, transaction.deposit?),
                     txAmountCy: transaction.currency,
                     txId: transaction.transaction_id,
                     txName: transaction.transaction_name,
                     provider: transaction.method_name,
                     txTypeId: transaction.method_id,
                     attributes: {
                         bonusCode: transaction.bonus_code
                     }
      )
      if result.success?
        Result.new(true, value: result.dig(:response, :authCode))
      else
        Result.new(false, value: 'not_authorized', message: result.dig(:response, :errMsg))
      end
    end
  end
end
