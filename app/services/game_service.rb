# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class GameService
  class NotValidGameProvider < StandardError; end

  def self.generate_play_url(params)
    case params[:provider]
    when 'playngo'
      ::Playngo::V1::PlayUrlService.call(
        params[:currentSession],
        params[:desktop],
        params[:gameId],
        params[:width],
        params[:height],
        params[:serverUrl],
        params[:mode]
      )
    when 'thunderkick'
      ::Thunderkick::V1::PlayUrlService.call(
        params[:currentSession],
        params[:desktop],
        params[:gameId],
        params[:currentPlayer],
        params[:mode]
      )
    when 'elk'
      ::Elk::V1::PlayUrlService.call(
        params[:currentSession],
        params[:desktop],
        params[:gameId],
        params[:currentPlayer],
        params[:mode]
      )
    when 'netent'
      ::Netent::V1::PlayUrlService.call(
        params[:currentSession],
        params[:desktop],
        params[:gameId],
        params[:currentPlayer],
        params[:mode]
      )
    else
      raise ::GameService::NotValidGameProvider, 'Not a valid game provider'
    end
  end
end
