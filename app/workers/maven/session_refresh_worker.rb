# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Maven
  class SessionRefreshWorker

    def perform(session_id, user_id)
      result = ::PlayerService.refresh_session(session_id)
      player = ::Player.find_by(id: user_id)
      if result.success?
        player.session_id = player.session_id
      else
        player.session_id = nil
      end
    rescue RuntimeError => e
      Rails.logger.error e.message
    end
  end
end
