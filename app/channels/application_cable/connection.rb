# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module ApplicationCable
  class Connection < ActionCable::Connection::Base
  end
end
