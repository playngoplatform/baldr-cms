# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class TransactionMailer < ApplicationMailer
  def action(from_email, recipient, subject, body)

    mail(from:  from_email,
         reply_to: from_email,
         to: recipient,
         subject: subject) do |format|
      format.html { render html: body.html_safe }
    end
  end
end
