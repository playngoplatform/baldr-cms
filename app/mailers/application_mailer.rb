# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
