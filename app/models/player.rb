# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Player < ApplicationRecord
  enum risk_level: { low: 0, medium: 10, high: 20, very_high: 30 }

  has_many :payment_logs, class_name: 'Payments::Log'
  belongs_to :host

  has_many :deposits,
           class_name: 'Payments::Deposit',
           foreign_key: 'user_id'
  has_many :withdrawals,
           class_name: 'Payments::Withdrawal',
           foreign_key: 'user_id'

  has_one :session

  def session_id
    session&.guid
  end

  def session_id=(value)
    if session
      session.update!(guid: value)
      session.touch
    else
      create_session!(guid: value)
    end
  end

  def to_hash
    {
      nickname: nickname,
      total_balance: total_balance,
      bonus_balance: bonus_balance,
      withdrawable_balance: withdrawable_balance,
      wagering_requirement: wagering_requirement,
      bonus_wagered_fraction: bonus_wagered_fraction,
      currency: currency,
      protection_limit_set: protection_limit_set
    }
  end
end
