# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Locale < ApplicationRecord
  enum mode: %i[frontend]

  translates :content, fallbacks_for_empty_translations: true

  active_admin_translates :content do
    validates_presence_of :content
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end

end
