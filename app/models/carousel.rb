# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Carousel < ApplicationRecord
  include Attachmentable

  enum action: { play: 0, bonus: 10 }
  enum mode: %i[unauthorized authorized]

  has_one_attached :thumbnail_ru
  has_one_attached :thumbnail_en
  has_one_attached :thumbnail_pl

  # has_attached_file :thumbnail,
  #                   styles: { thumb: ['412x320#', :png] },
  #                   path: '/casino-2.0/images/carousels/:id/:filename',
  #                   preserve_files: true
  # validates_attachment :thumbnail, content_type: { content_type: %w[image/png] }
  #

  validates_presence_of :start_date, :end_date

  validate :end_date_is_after_start_date

  scope :alive, -> { where(live: true).where('? BETWEEN start_date AND end_date', Date.current) }

  default_scope { includes(:category, thumbnail_ru_attachment: :blob, thumbnail_en_attachment: :blob, thumbnail_pl_attachment: :blob).order(:category_id) }

  belongs_to :category, class_name: 'GameCategory'

  validate :correct_object_id

  def correct_object_id
    correct_ids = Game.where(enabled: true).pluck(:game_id) + Bonus.published.pluck(:page_id)
    unless correct_ids.include? object_id
      self.errors.add(:object_id, I18n.t('activerecord.errors.models.carousel.attributes.object_id.wrong'))
    end
  end

  def to_hash
    {
      category: category.code,
      action: action,
      name: object_id,
      thumbnail: thumbnail_url
    }
  end

  private

  def end_date_is_after_start_date
    return if end_date.blank? || start_date.blank?
    errors.add(:end_date, 'cannot be before the start date') if end_date < start_date
  end
end
