# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class EmailTemplate < ApplicationRecord
  translates :subject, :body


  active_admin_translates :subject, :body do
    validates_presence_of :subject, :body
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end
end
