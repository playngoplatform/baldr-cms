# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Page < ApplicationRecord
  resourcify

  translates :title, :content, :meta_title, :meta_description

  enum category: %i[static bonus_header bonus_footer]

  active_admin_translates :title, :content, :meta_title, :meta_description do
    validates_presence_of :title, :content, :meta_title, :meta_description
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end

  def to_hash
    {
      name: name,
      title: title,
      content: content
    }
  end
end
