# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Payments
  def self.table_name_prefix
    'payments_'
  end
end
