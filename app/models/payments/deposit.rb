# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Payments
  class Deposit < ApplicationRecord
    include Transactionable
    include AASM

    serialize :extras

    enum state: { not_authorized: 0,
                  authorized: 10,
                  confirmed: 20,
                  cancelled: 30,
                  pending: 40
    }

    validates_presence_of :transaction_id,
                          :auth_token,
                          :amount,
                          :user_id,
                          :currency,
                          :paymentiq_amount,
                          :method_uuid,
                          :login,
                          :success_url,
                          :failure_url

    validates_numericality_of :amount, :paymentiq_amount

    aasm column: 'state',
         enum: true,
         logger: Rails.logger,
         use_transactions: false do

      state :not_authorized, initial: true
      state :authorized
      state :cancelled
      state :confirmed
      state :refused
      state :pending

      event :authorize do
        transitions from: :not_authorized, to: :authorized
      end

      event :pending do
        transitions from: :authorized, to: :pending
      end

      event :confirm do
        transitions from: :pending, to: :confirmed
      end

      event :cancel do
        transitions from: :pending, to: :cancelled
      end

    end

    def url; end

    def first_deposit?(user_id)
      self.class.where(state: :confirmed, user_id: user_id).blank?
    end

  end
end
