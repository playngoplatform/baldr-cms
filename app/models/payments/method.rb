# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Payments
  class Method < ApplicationRecord
    include Attachmentable

    enum mode: %i[deposit withdrawal]

    has_one_attached :thumbnail

    serialize :params
    serialize :currency, Array

    default_scope { order(:position) }

    has_many :limits,
             dependent: :destroy,
             foreign_key: 'method_id'

    has_many :deposits,
             primary_key: :uuid,
             foreign_key: :method_uuid,
             class_name: 'Payments::Deposit'

    has_many :withdrawals,
             primary_key: :uuid,
             foreign_key: :method_uuid,
             class_name: 'Payments::Withdrawal'

    scope :enabled, -> { where(status: true) }

    translates :name,
               :deposit_first,
               :deposit_second,
               :description,
               :withdrawal_to_one,
               :withdrawal_to_many,
               :account_required

    active_admin_translates :name,
                            :deposit_first,
                            :deposit_second,
                            :description,
                            :withdrawal_to_one,
                            :withdrawal_to_many,
                            :account_required do
      validates_presence_of :name
    end

    class << self
      def deposits(player)
        result = []
        deposit.enabled.find_each do |d|
          limits = d.limit(player.level_id, player.currency)
          next unless limits
          next if d.currency.any? && d.currency.exclude?(player.currency)

          result << d.to_hash(player).merge(limits)
        end
        result
      end

      def withdrawals(player)
        result = []
        withdrawal.enabled.find_each do |d|
          limits = d.limit(player.level_id, player.currency)
          next unless limits
          next if d.currency.any? && d.currency.exclude?(player.currency)

          result << d.to_hash(player).merge(limits)
        end
        result
      end
    end

    def limit(level_id, currency)
      limits.where(status: true,
                   level_id: level_id,
                   currency: currency).first&.to_hash
    end

    def provider
      class_name.constantize.new
    end

    def to_hash(player)
      if deposit?
        full_description = provider.first_deposit?(player.id) ? deposit_first : deposit_second
      else
        full_description = provider.accounts(player.id).count > 1 ? withdrawal_to_many : withdrawal_to_one
      end

      result = { id: uuid,
                 name: name,
                 description: full_description || description,
                 thumbnail: thumbnail_url }
      result[:request_account] = request_account if deposit?
      result
    end

    def asynchronous?
      asynchronous
    end
  end
end
