# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Payments
  class Limit < ApplicationRecord
    belongs_to :method, class_name: 'Payments::Method', foreign_key: 'method_id'

    validates_inclusion_of :fee, in: 0..100
    validates_numericality_of :min, only_integer: true
    validates_numericality_of :max, only_integer: true

    def to_hash
      {
        min: min,
        max: max
      }
    end


    def display_name
      "#{level_name} - #{currency}"
    end

  end
end
