# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Payments
  class Log < ApplicationRecord
    enum service: [:paymentiq, :payment_system]
    enum method: [:request, :response]
    enum state: [:successful, :error]

    belongs_to :logable, polymorphic: true
    belongs_to :player


    def error_message
      data.dig('errMsg')
    end
  end
end
