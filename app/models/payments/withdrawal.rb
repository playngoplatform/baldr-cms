# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Payments
  class Withdrawal < ApplicationRecord
    include Transactionable
    include AASM

    enum state: { not_authorized: 0,
                  authorized: 10,
                  confirmed: 20,
                  needs_to_approve: 30,
                  refused: 40,
                  pending: 50,
                  cancelled: 60 }

    validates_presence_of :transaction_id,
                          :auth_token,
                          :account_id,
                          :amount,
                          :user_id,
                          :currency,
                          :paymentiq_amount,
                          :method_uuid,
                          :login

    validates_numericality_of :amount, :paymentiq_amount

    aasm column: 'state',
         enum: true,
         logger: Rails.logger,
         use_transactions: false do
      state :not_authorized, initial: true
      state :authorized
      state :confirmed
      state :needs_to_approve
      state :refused
      state :pending
      state :cancelled

      event :authorize do
        transitions from: :not_authorized, to: :authorized
      end

      event :needs_to_approve do
        transitions from: :authorized, to: :needs_to_approve
      end

      event :pending do
        transitions from: :needs_to_approve, to: :pending
      end

      event :cancel do
        transitions from: :pending, to: :needs_to_approve
      end

      event :refuse do
        transitions from: :needs_to_approve, to: :cancelled
      end

      event :confirm do
        transitions from: :pending, to: :confirmed
      end
    end

    def accounts(_user_id)
      []
    end

    def high_risk?
      player.high? || player.very_high?
    end

    def eligible?(user_id)
      false
    end

  end
end
