# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class AdminUser < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :username, :email, :locale, :timezone
  validates_inclusion_of :locale, in: I18n.available_locales.map(&:to_s)

end
