# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
