# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
# frozen_string_literal: true

class Ckeditor::Asset < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include Ckeditor::Orm::ActiveRecord::AssetBase

  attr_accessor :data

  before_save :apply_data

  validate do
    if data.nil? || file.nil?
      errors.add(:data, :not_data_present, message: "data must be present")
    end
  end

  has_one_attached :storage_data

  def url
    #rails_blob_path(self.storage_data, only_path: true)
    return storage_data.service_url if ( Rails.env.staging? || Rails.env.production? )
    rails_blob_url(storage_data)
  end

  def styles
  end

  def content_type
    self.storage_data.content_type
  end

  def content_type=(_content_type)
    self.storage_data.content_type = _content_type
  end

  protected

  def file
    @file ||= storage_data
  end

  def blob
    @blob ||= ::ActiveStorage::Blob.find(file.attachment.blob_id)
  end

  def apply_data
    storage_data.attach(io: data, filename: data.original_filename)

    self.data_file_name = storage_data.blob.filename
    self.data_content_type = storage_data.blob.content_type
    self.data_file_size = storage_data.blob.byte_size
  end
end
