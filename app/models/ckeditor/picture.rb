# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Ckeditor::Picture < Ckeditor::Asset
  # for validation, see https://github.com/igorkasyanchuk/active_storage_validations

  def url_content
    if Rails.env.staging? || Rails.env.production?
      return storage_data.variant(resize: '800>').processed.service_url.split('?').first
      end

    rails_representation_url(storage_data.variant(resize: '800>').processed)
  end

  def url_thumb
    if Rails.env.staging? || Rails.env.production?
      return storage_data.variant(resize: '118x100').processed.service_url.split('?').first
      end

    rails_representation_url(storage_data.variant(resize: '118x100').processed)
  end
end
