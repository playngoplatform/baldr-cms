# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Attachmentable
  extend ActiveSupport::Concern

  # TODO: Refactor it!
  def thumbnail_url
    if self.respond_to?(:thumbnail)
      attachment = self.thumbnail
    else
      attachment =  send("thumbnail_#{I18n.locale}")
    end
    return '' unless attachment.attached?
    # return attachment.service_url if ( Rails.env.staging? || Rails.env.production? )
    Rails.application.routes.url_helpers.rails_blob_url(attachment)
  end

  class_methods do

  end

end
