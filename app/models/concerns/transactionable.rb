# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
module Transactionable
  extend ActiveSupport::Concern

  included do
    after_initialize :set_defaults

    belongs_to :player,
               foreign_key: :user_id,
                primary_key: :id

    has_many :logs, as: :logable

    # TODO: Разобраться почему не работает belongs_to
    has_one :payment_method,
            primary_key: :method_uuid,
            foreign_key: :uuid,
            class_name: 'Payments::Method'

    # default_scope { includes(:provider) }
    scope :unfinished, -> { where(state: %i[authorized pending]) }
  end

  def method_name
    payment_method.paymentiq_name || 'None'
  end

  def transaction_name
    payment_method.mode
  end

  def method_id
    payment_method.paymentiq_provider_id
  end

  def deposit?
    payment_method.deposit?
  end

  def paymentiq_authorized?
    result = Paymentiq::VerifyAndAuthorize.call(self)
    if result.success?
      self.auth_token = result[:value]
      save!
    end
    result.success?
  end

  def paymentiq_canceled?
    result = Paymentiq::Cancel.call(self)
    result.success?
  end

  def paymentiq_confirmed?
    result = Paymentiq::Transfer.call(self)
    result.success?
  end

  private

  def set_defaults
    return unless new_record?
    self.transaction_id = SecureRandom.uuid
    self.auth_token = 'not_authorized'
  end

  class_methods do
  end
end
