# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class GameCategory < ApplicationRecord

  has_many :games
  has_many :carousels

  translates :name

  active_admin_translates :name do
    validates_presence_of :name
  end

  default_scope { includes(:translations).order(:position) }

  def to_hash
    {
      code: code,
      name: name
    }
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end



end
