# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Game < ApplicationRecord
  include Attachmentable

  belongs_to :category, class_name: 'GameCategory'
  belongs_to :provider, class_name: 'GameProvider'

  has_one_attached :thumbnail

  scope :popular, -> { where(popular: true) }
  scope :recent, lambda {
                   where(id: GameProvider.where(mode: true).map{ |p|
                     Game.where(enabled: true, provider_id: p.id, old: false)
                         .order(created_at: :desc).take(10)
                         .pluck(:id) }.flatten
                   )
                 }

  translates :image_alt, :meta_title, :meta_description

  active_admin_translates :image_alt, :meta_title, :meta_description do
    validates_presence_of :image_alt, :meta_title, :meta_description
  end

  default_scope { includes(:translations, :category, :provider, thumbnail_attachment: :blob).order(created_at: :desc) }

  validates :name, format: { with: /\A[a-zA-Z:’'\-\d\s]+\z/ }
  validates :game_id, format: { with: /\A[a-zA-Z\d@_-]+\z/ }
  validates_uniqueness_of :game_id, scope: :provider_id

  def new_game_ids
    result = []
    GameProvider.where(mode: true).each do |p|
      result << where(enabled: true, provider_id: p.id).order(created_at: :desc).take(10).pluck(:id)
    end
    result.flatten
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end

  def to_hash(default_category = '')
    { category: default_category.empty? ? category.code : default_category,
      locked: locked?,
      provider: provider.code,
      name: name,
      game_id: game_id,
      is_mobile: is_mobile,
      is_desktop: is_desktop,
      image_alt: image_alt,
      thumbnail: thumbnail_url,
      width: width,
      height: height }
  end
end
