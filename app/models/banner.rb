# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Banner < ApplicationRecord
  include Attachmentable

  enum status: %i[published draft]
  # enum category: { main: 0 }
  enum action: { play: 0, bonus: 10, nothing: 20 }
  enum size: { small: 0, big: 10 }

  has_one_attached :thumbnail_ru
  has_one_attached :thumbnail_en
  has_one_attached :thumbnail_pl

  validate :correct_object_id
  validates_presence_of :action, :size, :status

  def correct_object_id
    return if object_id.blank?

    correct_ids = Game.where(enabled: true).pluck(:game_id) + Bonus.published.pluck(:page_id)
    unless correct_ids.include? object_id
      errors.add(:object_id, I18n.t('activerecord.errors.models.carousel.attributes.object_id.wrong'))
    end
  end

  def to_hash
    {
      action: action,
      size: size,
      object_id: object_id,
      thumbnail: thumbnail_url
    }
  end

  class << self
    def active
      published.where('(:today BETWEEN start_date AND end_date)' \
                      ' OR (start_date IS NULL AND end_date >= :today)' \
                      ' OR (start_date <= :today AND end_date IS NULL)' \
                      ' OR (start_date IS NULL AND end_date IS NULL)', today: Date.today)
    end
  end
end
