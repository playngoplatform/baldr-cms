# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Bonus < ApplicationRecord
  include Attachmentable

  enum status: { draft: 0, published: 1 }
  enum mode: { deposit1: 0, cashback: 1, deposit2: 3 }

  translates  :title,
              :content,
              :description,
              :meta_title,
              :meta_description,
              :promo

  has_one_attached :thumbnail

  default_scope { includes(:translations).order(:position) }

  has_many :limits, class_name: 'BonusLimit', dependent: :destroy

  # TODO: Refactor it (ActiveAdmin belongs_to bug)
  has_many :bonus_limits, class_name: 'BonusLimit'

  scope :active_now, -> {published.includes(:translations)
                          .where('(:today BETWEEN start_date AND end_date)' \
                              ' OR (start_date IS NULL AND end_date >= :today)' \
                              ' OR (start_date <= :today AND end_date IS NULL)' \
                              ' OR (start_date IS NULL AND end_date IS NULL)', today: Date.today)
                          .order(:position)
  }

  active_admin_translates :title, :content, :description, :meta_title, :meta_description, :promo do
    validates_presence_of :title, :content, :description, :meta_title, :meta_description, :promo
  end

  validates_presence_of :page_id

  # scope :current, -> { published.includes(:translations)
  #                          .where('(start_date <= ? AND end_date >= ?)'+
  #                                 ' OR (start_date IS NULL AND end_date IS NULL)'+
  #                                 ' OR (start_date IS NULL AND end_date >= ?)'+
  #                                 ' OR (start_date <= ? AND end_date IS NULL)', Date.today, Date.today, Date.today, Date.today)
  #                          .order(:position) }

  class << self
    def active(args = {})
      if args[:short]
        active_now.map { |b| b.to_hash_short(args) }
      else
        active_now.map { |b| b.to_h(args) }
      end
    end
  end

  def eligible?(user_id = nil)
    return false if user_id.nil?

    case mode
    when 'deposit1'
      Payments::Deposit.confirmed.where(user_id: user_id).blank?
    when 'deposit2'
      true
    when 'cashback'
      true
    else
      false
    end
  end

  def to_hash_short(args = {})
    {
      id: page_id,
      eligible: eligible?(args[:user_id]),
      code: code,
      registration: registration,
      fixed_amount: fixed_amount,
      limits: limits.map(&:to_hash),
      title: parse_templates(title),
      thumbnail: thumbnail_url,
      promo: promo,
      description: parse_templates(description)
    }
  end

  def to_h(args = {})
    limits = args[:currency].nil? ? self.limits : self.limits.where(currency: args[:currency])
    {
      id: page_id,
      title: parse_templates(title),
      content: parse_templates(content),
      thumbnail: thumbnail_url,
      promo: promo,
      description: parse_templates(description),
      code: code,
      registration: registration,
      fixed_amount: fixed_amount,
      limits: limits.map(&:to_hash),
      eligible: eligible?(args[:user_id]),
      meta_title: meta_title,
      meta_description: meta_description
    }
  end

  def cache_key
    super + '-' + Globalize.locale.to_s
  end

  private

  def parse_templates(value)
    templates = %w([[RUB_MIN]] [[RUB_MAX]] [[EUR_MIN]] [[EUR_MAX]] [[UAH_MIN]] [[UAH_MAX]])
    templates.each do |t|
      next unless value.include?(t)

      limit = limits.where(currency: t[2..4]).first
      number = t[6..8] == 'MIN' ? limit.try(:min) : limit.try(:max)
      value.gsub! t, number_with_delimiter(number)
    end
    value
  rescue StandardError
    value
  end
end
