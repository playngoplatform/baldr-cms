# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class GameProvider < ApplicationRecord
  include Attachmentable

  has_many :games

  has_one_attached :thumbnail

  default_scope { includes(thumbnail_attachment: :blob)}

  def to_hash
    {
        code: code,
        name: name,
        thumbnail: thumbnail_url
    }
  end

end
