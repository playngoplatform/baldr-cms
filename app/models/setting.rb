# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
# RailsSettings Model
class Setting < RailsSettings::Base
  source Rails.root.join('config/settings.yml')
  namespace Rails.env
end
