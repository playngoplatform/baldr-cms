# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Host < ApplicationRecord
  has_ancestry

  has_many :players

  def mirror
    subtree.where(enabled: true)&.first
  end

end
