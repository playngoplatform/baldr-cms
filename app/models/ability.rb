# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class Ability
  include CanCan::Ability

  def initialize(_user)
    can :manage, :all
    # if user.has_role? :admin
    #   can :manage, :all
    #   cannot :create, Backend::PaymentMethod
    #   cannot :create, Backend::PaymentTransaction
    #   cannot :update, Backend::PaymentTransaction
    #   cannot :destroy, Backend::PaymentTransaction
    #   cannot :destroy, Backend::PaymentMethod
    #   cannot :destroy, Backend::PaymentLimit
    #   cannot :destroy, Backend::Translation
    #   can :read, Backend::ExchangeRate
    #   cannot :update, Backend::ExchangeRate
    #   cannot :destroy, Backend::ExchangeRate
    #   can :read, Backend::PaymentLog
    #   cannot :update, Backend::PaymentLog
    #   cannot :destroy, Backend::PaymentLog
    # else
    #   if user.has_role?(:content)
    #     can :read, ActiveAdmin::Page, name: 'Dashboard'
    #     can :manage, Backend::Page
    #     cannot :destroy, Backend::Page
    #     can :manage, Backend::EmailTemplate
    #     cannot :destroy, Backend::EmailTemplate
    #     can :manage, Backend::Translation
    #     cannot :destroy, Backend::Translation
    #     can :manage, Backend::News
    #     can :manage, Backend::Bonus
    #     can :manage, Backend::Tournament
    #     can :manage, Backend::Image
    #     can :manage, Backend::Advertisement
    #   end
    #   if user.has_role?(:support)
    #     can :read, ActiveAdmin::Page, name: 'Dashboard'
    #     can :manage, Backend::Page
    #     can :manage, Backend::LostWallet
    #     cannot :destroy, Backend::LostWallet
    #     can :read, Backend::Game
    #     can :read, Backend::PaymentLimit
    #     can :read, Backend::BonusLimit
    #     can :read, Backend::Image
    #     can :read, Backend::Player
    #     # can :import, Payment
    #   end
    #   if user.has_role?(:games)
    #     can :manage, Backend::Game
    #     can :manage, Backend::Maintenance
    #   end
    #   if user.has_role?(:finance)
    #     can :manage, Backend::PaymentMethod
    #     can :manage, Backend::PaymentLimit
    #     can :manage, Backend::Tournament
    #     can :manage, Backend::Bonus
    #     can :manage, Backend::BonusLimit
    #     can :read, Backend::PaymentTransaction
    #   end
    # end
  end
end
