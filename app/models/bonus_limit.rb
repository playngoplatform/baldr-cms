# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
class BonusLimit < ApplicationRecord
  belongs_to :bonus, class_name: 'Bonus'

  validates :min, numericality: { only_integer: true, greater_than: 0 }
  validates :max, numericality: { only_integer: true, greater_than: 0 }

  def to_hash
    {
      currency: currency,
      min: min,
      max: max
    }
  end
end
