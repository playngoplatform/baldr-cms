# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Payments::Deposit, as: 'Deposit' do

  menu priority: 11, parent: 'finances', url: ->{ deposits_path(locale: I18n.locale)}

  config.sort_order = 'updated_at_desc'

  config.batch_actions = false

  actions :all, except: [:new, :create, :destroy, :update, :edit]

  includes :payment_method, :player

  scope :confirmed, default: true do |scope|
    scope.confirmed
  end

  scope :pending do |scope|
    scope.where(state: [:authorized, :pending])
  end

  scope :refused do |scope|
    scope.where(state: [:not_authorized, :cancelled])
  end

  scope :all do |scope|
    scope.all
  end

  action_item :back_transaction, only: :show  do
    link_to I18n.t('transaction_action.back'), deposits_path
  end

  controller do
    def update
      if params['player']
        attrs = params.permit('player': [:id, :risk_level, :note])
        ::Player.update(attrs['player'][:id], attrs['player'])
      end
      redirect_to resource_path(params[:id])
    end
    def scoped_collection
      Payments::Deposit.includes(:payment_method)
    end
  end

  index download_links: false do
    if params['scope'] == 'all'
      column :state do |m|
        translate("transaction_state.#{m.state}")
      end
    end

    column :method_name do |m|
      m.method_name
    end
    column :user_id
    column :login
    column :account_id
    column :amount
    column :currency
    column :created_at
    column :updated_at

    actions

  end

  filter :login, filters: [:contains]
  filter :account_id, filters: [:contains]
  filter :created_at, as: :date_range, datepicker_options: {dateFormat: 'dd/mm/yy'}


  show title: I18n.t('activerecord.models.deposit.one')  do |transaction|
    columns do
      column do
        attributes_table do
          row :state do |t|
            translate("transaction_state.#{t.state}")
          end
          row :transaction_id
          row :method_name
          row :user_id
          row :login
          row :account_id
          row :fee
          row :amount
          row :paymentiq_amount
          row :created_at
          row :updated_at

        end
      end
      column do
        div style: 'padding: 11px;' do # Hack
          active_admin_form_for deposit.player, url: deposit_path, method: :patch, builder: ActiveAdmin::FormBuilder do |f|
            f.inputs I18n.t('common.player_info') do
              f.input :id, as: :hidden
              f.input :risk_level
              f.input :note
            end
            f.actions
          end
        end
      end
    end
    tabs do
      tab I18n.t('common.payment_logs') do
        paginated_collection Payments::Log.where(logable_type: 'Payments::Deposit',logable_id: resource.id).page(params[:page]).per(40), download_links: false, pagination_total: false do
          table_for collection do |l|
            l.column(I18n.t('activerecord.attributes.payments/log.created_at')) {|i| i.created_at}
            l.column(I18n.t('activerecord.attributes.payments/log.service')) {|i| i.service}
            l.column(I18n.t('activerecord.attributes.payments/log.method')) {|i| i.method}
            l.column(I18n.t('activerecord.attributes.payments/log.url')) {|i| i.url}
            l.column(I18n.t('activerecord.attributes.payments/log.data')) {|i| i.data}

          end
        end
      end
      tab I18n.t('common.deposits') do
        paginated_collection deposit.player.deposits.confirmed.includes(:payment_method).page(params[:page]).per(20), download_links: false, pagination_total: false do
          table_for collection do |t|
            t.column(I18n.t('activerecord.attributes.payments/deposit.user_id')) { |tr| tr.user_id }
            t.column(I18n.t('activerecord.attributes.payments/deposit.method_name')) { |tr| tr.method_name }
            t.column(I18n.t('activerecord.attributes.payments/deposit.account_id')) { |tr| tr.account_id }
            t.column I18n.t('activerecord.attributes.payments/deposit.paymentiq_amount') do |tr|
              "#{number_to_currency(tr.paymentiq_amount.abs, unit: tr.currency, format: '%n' )} #{tr.currency}"
            end

            t.column(I18n.t('activerecord.attributes.payments/deposit.amount')) do |tr|
              "#{number_to_currency(tr.amount, unit: tr.currency, format: '%n' )} #{tr.currency}"
            end
            t.column(I18n.t('activerecord.attributes.payments/deposit.fee')) { |tr| tr.fee }
            t.column(I18n.t('activerecord.attributes.payments/deposit.created_at')) { |tr| tr.created_at }
          end
        end
      end
      tab I18n.t('common.withdrawals') do
        paginated_collection deposit.player.withdrawals.confirmed.includes(:payment_method).page(params[:page]).per(20), download_links: false, pagination_total: false do
          table_for collection do |t|
            t.column(I18n.t('activerecord.attributes.payments/deposit.user_id')) { |tr| tr.user_id }
            t.column(I18n.t('activerecord.attributes.payments/deposit.method_name')) { |tr| tr.method_name }
            t.column(I18n.t('activerecord.attributes.payments/deposit.account_id')) { |tr| tr.account_id }
            t.column I18n.t('activerecord.attributes.payments/deposit.paymentiq_amount') do |tr|
              "#{number_to_currency(tr.paymentiq_amount.abs, unit: tr.currency, format: '%n' )} #{tr.currency}"
            end

            t.column(I18n.t('activerecord.attributes.payments/deposit.amount')) do |tr|
              "#{number_to_currency(tr.amount, unit: tr.currency, format: '%n' )} #{tr.currency}"
            end
            # t.column(I18n.t('activerecord.attributes.payments/deposit.fee')) { |tr| tr.fee }
            t.column(I18n.t('activerecord.attributes.payments/deposit.created_at')) { |tr| tr.created_at }
          end
        end
      end
    end
  end
end