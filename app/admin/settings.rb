# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register_page 'Settings' do

  menu priority: 999, parent: 'system', url: -> { settings_path(locale: I18n.locale) }

  active_admin_settings_page(
    title: I18n.t('menu.settings'),
    template: 'active_admin/settings/index'
  )

end