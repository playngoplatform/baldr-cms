# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Payments::Withdrawal, as: 'Withdrawal' do
  menu priority: 12, parent: 'finances', url: -> { withdrawals_path(locale: I18n.locale) }

  config.sort_order = 'updated_at_desc'

  config.batch_actions = false

  actions :all, except: %i[new create destroy update edit]

  includes :payment_method

  scope :needs_to_approve, default: true, &:needs_to_approve

  scope :confirmed, &:confirmed

  scope :pending do |scope|
    scope.where(state: %i[authorized pending])
  end

  scope :refused do |scope|
    scope.where(state: %i[not_authorized cancelled])
  end

  scope :all, &:all

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), withdrawals_path
  end

  member_action :approve do
    result = resource.execute!
    if result.success?
      resource.pending!
      resource.confirm! unless resource.payment_method.asynchronous?
      redirect_to resource_path
    else
      redirect_to({ action: :show }, alert: result[:message])
    end
  end

  member_action :refuse do
    result = Paymentiq::Cancel.call(resource)
    if result.success?
      resource.refuse!
      redirect_to resource_path
    else
      redirect_to({ action: :show }, alert: result[:message])
    end
  end

  action_item :approve,
              only: :show,
              if: proc { withdrawal.needs_to_approve? },
              confirm: I18n.t('common.high_risk_confirm') do
    if withdrawal.high_risk?
      link_to I18n.t('transaction_action.approve'),
              approve_withdrawal_path(withdrawal),
              data: { confirm: I18n.t('common.high_risk_confirm') }
    else
      link_to I18n.t('transaction_action.approve'), approve_withdrawal_path(withdrawal)
    end
  end

  action_item :refuse, only: :show, if: proc { withdrawal.needs_to_approve? } do
    link_to I18n.t('transaction_action.refuse'),
            refuse_withdrawal_path(withdrawal),
            data: { confirm: I18n.t('common.refuse_confirm') }
  end

  index download_links: false do
    if params['scope'] == 'all'
      column :state do |m|
        translate("transaction_state.#{m.state}")
      end
    end

    column :method_name, &:method_name
    column :user_id
    column :login
    column :account_id
    column :amount
    column :currency
    column :created_at
    column :updated_at

    actions
  end

  filter :login, filters: [:contains]
  filter :account_id, filters: [:contains]
  filter :created_at, as: :date_range, datepicker_options: { dateFormat: 'dd/mm/yy' }

  show title: I18n.t('activerecord.models.withdrawal.one') do |_transaction|
    columns do
      column do
        attributes_table do
          row :state do |t|
            translate("transaction_state.#{t.state}")
          end
          row :transaction_id
          row :method_name
          row :user_id
          row :login
          row :account_id
          row :fee
          row :amount
          row :paymentiq_amount
          row :created_at
          row :updated_at
        end
      end
      column do
        div style: 'padding: 11px;' do # Hack
          @player = withdrawal.player || ::Player.create(id: withdrawal.user_id, login: withdrawal.login)
          active_admin_form_for @player, url: withdrawal_path, method: :patch, builder: ActiveAdmin::FormBuilder do |f|
            f.inputs I18n.t('common.player_info') do
              f.input :id, as: :hidden
              f.input :risk_level
              f.input :note
            end
            f.actions
          end
        end
      end
    end
    tabs do
      tab I18n.t('common.payment_logs') do
        paginated_collection Payments::Log.where(logable_type: 'Payments::Withdrawal',
                                                 logable_id: resource.id)
                                 .page(params[:page]).per(40), download_links: false, pagination_total: false do
          table_for collection do |l|
            l.column(I18n.t('activerecord.attributes.payments/log.created_at'), &:created_at)
            l.column(I18n.t('activerecord.attributes.payments/log.service'), &:service)
            l.column(I18n.t('activerecord.attributes.payments/log.method'), &:method)
            l.column(I18n.t('activerecord.attributes.payments/log.url'), &:url)
            l.column(I18n.t('activerecord.attributes.payments/log.data'), &:data)
          end
        end
      end
      tab I18n.t('common.deposits') do
        paginated_collection withdrawal.player.deposits.confirmed.includes(:payment_method).page(params[:page]).per(20), download_links: false, pagination_total: false do
          table_for collection do |t|
            t.column(I18n.t('activerecord.attributes.payments/deposit.user_id')) { |tr| tr.user_id }
            t.column(I18n.t('activerecord.attributes.payments/deposit.method_name')) { |tr| tr.method_name }
            t.column(I18n.t('activerecord.attributes.payments/deposit.account_id')) { |tr| tr.account_id }
            t.column I18n.t('activerecord.attributes.payments/deposit.paymentiq_amount') do |tr|
              "#{number_to_currency(tr.paymentiq_amount.abs, unit: tr.currency, format: '%n' )} #{tr.currency}"
            end

            t.column(I18n.t('activerecord.attributes.payments/deposit.amount')) do |tr|
              "#{number_to_currency(tr.amount, unit: tr.currency, format: '%n' )} #{tr.currency}"
            end
            t.column(I18n.t('activerecord.attributes.payments/deposit.fee')) { |tr| tr.fee }
            t.column(I18n.t('activerecord.attributes.payments/deposit.created_at')) { |tr| tr.created_at }
          end
        end
      end
      tab I18n.t('common.withdrawals') do
        paginated_collection withdrawal.player.withdrawals.includes(:payment_method).page(params[:page]).per(20), download_links: false, pagination_total: false do
          table_for collection do |t|
            t.column(I18n.t('activerecord.attributes.payments/withdrawal.user_id'), &:user_id)
            t.column(I18n.t('activerecord.attributes.payments/withdrawal.method_name'), &:method_name)
            t.column(I18n.t('activerecord.attributes.payments/withdrawal.account_id'), &:account_id)
            t.column I18n.t('activerecord.attributes.payments/withdrawal.paymentiq_amount') do |tr|
              "#{number_to_currency(tr.paymentiq_amount.abs, unit: tr.currency, format: '%n')} #{tr.currency}"
            end

            t.column(I18n.t('activerecord.attributes.payments/withdrawal.amount')) do |tr|
              "#{number_to_currency(tr.amount, unit: tr.currency, format: '%n')} #{tr.currency}"
            end
            # t.column(I18n.t('activerecord.attributes.payments/withdrawal.fee'), &:fee)
            t.column(I18n.t('activerecord.attributes.payments/withdrawal.created_at'), &:created_at)
          end
        end
      end

    end
  end
end
