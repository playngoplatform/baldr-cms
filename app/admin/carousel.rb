# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Carousel do
  permit_params :live,
                :object_id,
                :action,
                :start_date,
                :end_date,
                :thumbnail_ru,
                :thumbnail_en,
                :thumbnail_pl,
                :category_id,
                :mode
  menu priority: 100, parent: 'games', label: I18n.t('menu.carousel'), url: -> { carousels_path(locale: I18n.locale) }

  config.paginate = false
  config.filters = false

  includes :thumbnail_ru_attachment, :thumbnail_en_attachment, :thumbnail_pl_attachment

  scope :active, default: true do |scope|
    scope.where(live: true)
  end

  scope :disabled do |scope|
    scope.where(live: false)
  end

  scope :all, &:all

  controller do
    before_action do
      ActiveStorage::Current.host = request.base_url
    end
  end

  action_item :index, only: :index do
    link_to I18n.t('carousel_action.upload'), action: :upload
  end

  batch_action :disable do |ids|
    batch_action_collection.find(ids).each do |item|
      item.live = false
      item.save
    end
    redirect_to collection_path
  end

  batch_action :enable do |ids|
    batch_action_collection.find(ids).each do |item|
      item.live = true
      item.save
    end
    redirect_to collection_path
  end

  collection_action :upload do
    render 'active_admin/carousel/upload'
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), carousels_path(locale: I18n.locale)
  end

  collection_action :import_file, method: :post do
    file = params[:file].try(:read)
    if file
      data = JSON.parse(file)
      ActiveRecord::Base.transaction do
        data.each do |item|
          carousel = Carousel.new
          carousel.live = false
          carousel.action = item['action']
          carousel.object_id = item['object_id']
          carousel.category_id = item['category_id']
          carousel.mode = item['mode']
          carousel.start_date = item['start_date']
          carousel.end_date = item['end_date']
          carousel.thumbnail_ru_file_name = item['thumbnail_ru_file_name']
          carousel.thumbnail_en_file_name = item['thumbnail_en_file_name']
          carousel.thumbnail_pl_file_name = item['thumbnail_pl_file_name']
          carousel.save!
        end
      end
      flash[:notice] = I18n.t('message.carousel.upload_success')
    else
      flash[:error] = I18n.t('message.carousel.upload_error')
    end
    redirect_to action: :index
  end

  batch_action :export do |ids|
    data = []
    batch_action_collection.find(ids).each do |item|
      data << { object_id: item.object_id,
                action: item.action,
                end_date: item.end_date,
                position: item.position,
                mode: item.mode,
                thumbnail_ru_file_name: item.thumbnail_ru_file_name,
                thumbnail_en_file_name: item.thumbnail_en_file_name,
                thumbnail_pl_file_name: item.thumbnail_pl_file_name,
                category_id: item.category_id }
    end
    send_data data.to_json, type: 'application/json; charset=utf8;', disposition: "attachment; filename=carousels_#{Time.now.strftime('%d_%m_%Y__%H_%M')}.json"
  end

  index download_links: false do
    selectable_column
    toggle_bool_column :live
    column :category
    column :mode
    column :start_date
    column :end_date
    column I18n.t('activerecord.attributes.carousel.thumbnail_ru') do |g|
      if g.thumbnail_ru.attachment && g.thumbnail_ru.image?
        image_tag(g.thumbnail_ru.variant(resize: '75x75'))
      end
    end
    column I18n.t('activerecord.attributes.carousel.thumbnail_en') do |g|
      if g.thumbnail_en.attachment && g.thumbnail_en.image?
        image_tag(g.thumbnail_en.variant(resize: '75x75'))
      end
    end
    column I18n.t('activerecord.attributes.carousel.thumbnail_pl') do |g|
      if g.thumbnail_pl.attachment && g.thumbnail_pl.image?
        image_tag(g.thumbnail_pl.variant(resize: '75x75'))
      end
    end
    column :action
    column :object_id
    actions
  end

  show do
    attributes_table do
      row :live
      row :action
      row :object_id
      row :category
      row :mode
      row :thumbnail_ru do |g|
        if g.thumbnail_ru.attachment && g.thumbnail_ru.image?
          image_tag(g.thumbnail_ru.service_url)
        end
      end
      row :thumbnail_en do |g|
        if g.thumbnail_en.attachment && g.thumbnail_en.image?
          image_tag(g.thumbnail_en.service_url)
        end
      end
      row :thumbnail_pl do |g|
        if g.thumbnail_pl.attachment && g.thumbnail_pl.image?
          image_tag(g.thumbnail_pl.service_url)
        end
      end
    end
  end

  form do |f|
    f.inputs 'Carousels' do
      f.input :live, required: true
      f.input :action, required: true
      f.input :object_id, required: true, hint: I18n.t('activerecord.attributes.carousel.hint')
      f.input :category, required: true
      f.input :mode, required: true
      f.input :start_date, as: :date_time_picker
      f.input :end_date, as: :date_time_picker
      f.li "<label class='label'>#{I18n.t('activerecord.attributes.carousel.filename')}</label><span>#{f.object.thumbnail_ru.attachment && f.object.thumbnail_ru.image? ? f.object.thumbnail_ru.filename : '-'}</span>".html_safe
      f.input :thumbnail_ru, as: :file, required: true, hint: image_tag(f.object.thumbnail_ru.attachment && f.object.thumbnail_ru.image? ? f.object.thumbnail_ru.service_url : '')

      f.li "<label class='label'>#{I18n.t('activerecord.attributes.carousel.filename')}</label><span>#{f.object.thumbnail_en.attachment && f.object.thumbnail_en.image? ? f.object.thumbnail_en.filename : '-'}</span>".html_safe
      f.input :thumbnail_en, as: :file, required: true, hint: image_tag(f.object.thumbnail_en.attachment && f.object.thumbnail_en.image? ? f.object.thumbnail_en.service_url : '')

      f.li "<label class='label'>#{I18n.t('activerecord.attributes.carousel.filename')}</label><span>#{f.object.thumbnail_pl.attachment && f.object.thumbnail_pl.image? ? f.object.thumbnail_pl.filename : '-'}</span>".html_safe
      f.input :thumbnail_pl, as: :file, required: true, hint: image_tag(f.object.thumbnail_pl.attachment && f.object.thumbnail_pl.image? ? f.object.thumbnail_pl.service_url : '')
    end
    f.actions
  end
end
