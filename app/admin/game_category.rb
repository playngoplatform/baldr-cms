# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register GameCategory do
  permit_params :code, translations_attributes: %i[id locale name]

  menu priority: 3, parent: 'games', label: I18n.t('menu.game_category'), url: -> { game_categories_path(locale: I18n.locale) }

  config.filters = false
  config.sort_order = 'position_asc'
  actions :index, :new, :create, :edit, :update

  controller do
    def scoped_collection
      GameCategory.includes(:translations)
    end
  end

  index download_links: false do
    selectable_column
    translation_status
    column :code
    column :name
    column :position
    actions
  end

  form do |f|
    f.inputs do
      f.input :code, required: true
      f.input :position, required: true
      f.translated_inputs 'Translated fields', switch_locale: false do |t|
        t.input :name
      end
    end
    f.actions
  end
end
