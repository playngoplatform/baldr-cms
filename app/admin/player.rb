# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Player  do
  permit_params :id, :login, :risk_level, :note

  menu priority: 4, parent: 'games', url: ->{ players_path(locale: I18n.locale)}

  config.sort_order = 'login'

  config.batch_actions = false

  actions :index, :edit, :update

  includes :host

  scope :all, default: true do |scope|
    scope.all
  end

  scope :medium do |scope|
    scope.medium
  end

  scope :high do |scope|
    scope.high
  end

  scope :very_high do |scope|
    scope.very_high
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), players_path(locale: I18n.locale)
  end

  sidebar :actions, only: [:show] do
    ul do
      # if can? :read, Payments::Log
        li link_to I18n.t('common.payment_logs'), player_payment_logs_path(resource)
      # end
      # li link_to I18n.t('activerecord.models.transaction.other'), transactions_path(locale: I18n.locale)
    end
  end

  index download_links: false do
    column :id
    column :login
    column :risk_level do |m|
      I18n.t("risk_level.#{m.risk_level}")
    end
    column :host do |p|
      p.host&.name
    end
    actions do |m|
      # a translate('activerecord.models.transaction.other'), href: transactions_path(locale: I18n.locale, user_id: m.id)
      # if can? :read, Payments::Log
      #   a translate('common.payment_logs'), href: player_payment_logs_path(m)
      # end
    end
  end

  show do
    attributes_table do
      row :id
      row :login
      row :risk_level
      row :note
    end
    tabs do
      tab I18n.t('common.history') do
        # paginated_collection Backend::PaymentTransaction.by_user(resource.user_id).page(params[:page]).per(20), download_links: false, pagination_total: false do
        #   table_for collection do |t|
        #     t.column(I18n.t('activerecord.attributes.backend/payment_transaction.user_id')) { |tr| tr.user_id }
        #     t.column(I18n.t('activerecord.attributes.backend/payment_method.name')) { |tr| tr.pm.name }
        #     t.column(I18n.t('activerecord.attributes.backend/payment_transaction.mode')) { |tr| translate("mode.#{tr.mode}") }
        #     t.column(I18n.t('activerecord.attributes.backend/payment_transaction.account_masked')) { |tr| tr.account_id }
        #     t.column(I18n.t('activerecord.attributes.backend/payment_transaction.bonus_code')) { |tr| tr.params.fetch(:bonus_code, '') }
        #     t.column I18n.t('activerecord.attributes.backend/payment_transaction.paymentiq_amount') do |tr|
        #       "#{number_to_currency(tr.paymentiq_amount.abs, unit: tr.currency, format: '%n' )} #{tr.currency}"
        #     end
        #
        #     t.column(I18n.t('activerecord.attributes.backend/payment_transaction.amount')) do |tr|
        #       if tr.withdrawal?
        #         "#{number_to_currency(tr.amount, unit: tr.params.fetch(:currency, tr.currency) , format: '%n' )} #{tr.params[:currency]}"
        #       else
        #         "#{number_to_currency(tr.amount, unit: tr.currency, format: '%n' )} #{tr.currency}"
        #       end
        #     end
        #
        #     t.column(I18n.t('activerecord.attributes.backend/payment_transaction.created_at')) { |tr| tr.created_at }
        #   end
        # end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :id, required: true
      f.input :login, required: true
      f.input :risk_level
      f.input :note
    end
    f.actions
  end

  filter :login, filters: [:contains]
  # ActiveAdmin bug https://github.com/activeadmin/activeadmin/issues/5155
  # filter :user_id, filters: [:equals]
  filter :risk_level, as: :select, collection: Player.risk_levels.map{|k, v| [I18n.t("risk_level.#{k}"), v]}

end
