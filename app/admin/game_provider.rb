# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register GameProvider do
  permit_params :mode, :thumbnail

  menu priority: 4, parent: 'games', label: I18n.t('menu.game_provider'), url: -> { game_providers_path(locale: I18n.locale) }

  config.filters = false
  actions :index, :edit, :update, :show


  controller do
    before_action do
      ActiveStorage::Current.host = request.base_url
    end

    def scoped_collection
      GameProvider.includes(:thumbnail_attachment)
    end
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), game_providers_path(locale: I18n.locale)
  end

  index download_links: false do
    toggle_bool_column :mode
    column :code
    column :name
    column I18n.t('activerecord.attributes.game_provider.image') do |g|
      if g.thumbnail.attachment && g.thumbnail.image?
        image_tag(g.thumbnail.variant(resize: '100x50'))
      end
    end
    actions
  end

  show do
    attributes_table do
      row :mode
      row :code
      row :name
      row :thumbnail do |g|
        if g.thumbnail.attachment && g.thumbnail.image?
          image_tag(g.thumbnail.service_url)
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :mode, required: true
      f.input :code, input_html: { readonly: true, disabled: true }
      f.input :name, input_html: { readonly: true, disabled: true }
      f.li "<label class='label'>#{I18n.t('activerecord.attributes.game_provider.image')}</label><span>#{(f.object.thumbnail.attachment && f.object.thumbnail.image?) ? f.object.thumbnail.filename : '-'}</span>".html_safe
      f.input :thumbnail, as: :file, required: true, hint: image_tag((f.object.thumbnail.attachment && f.object.thumbnail.image?) ? f.object.thumbnail.service_url : '')
    end

    f.actions
  end
end
