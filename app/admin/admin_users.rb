# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register AdminUser do
  permit_params :username, :email, :timezone, :locale, :password, :password_confirmation

  menu priority: 100, parent: 'system', url: -> { admin_users_path(locale: I18n.locale) }

  controller do
    def update_resource(object, attributes)
      attributes.each do |attr|
        if attr[:password].blank? && attr[:password_confirmation].blank?
          attr.delete :password
          attr.delete :password_confirmation
        end
      end

      object.send :update_attributes, *attributes
    end
  end

  index download_links: false do
    selectable_column
    column :username
    column :email
    column :locale
    column :timezone
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  show do
    attributes_table do
      row :username
      row :email
      row :locale
      row :timezone
      row :roles do |user|
        user.roles.collect { |r| r.name.capitalize }.to_sentence
      end
      row :current_sign_in_at
      row :sign_in_count
      row :last_sign_in_at
      row :last_sign_in_ip
      row :created_at
    end
  end

  form do |f|
    f.inputs  do
      f.input :username
      f.input :email
      f.input :locale, as: :select, collection: %w[ru en], required: true
      f.input :timezone, as: :time_zone
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end
end
