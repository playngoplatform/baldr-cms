# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register EmailTemplate, as: 'Email' do

  actions :index, :new, :create, :edit, :update, :destroy

  permit_params :action, translations_attributes: [:id, :locale, :subject, :body]

  menu priority: 2, parent: 'content', url: ->{ emails_path(locale: I18n.locale)}

  config.sort_order = 'action'

  action_item :index do
    link_to I18n.t('email_action.upload'), action: :upload
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), emails_path(locale: I18n.locale)
  end

  collection_action :upload do
    render 'active_admin/email/upload'
  end

  batch_action :export do |ids|
    data = []
    batch_action_collection.find(ids).each do |item|
      translations = []
      item.translations.each do |t|
        translations << { locale: t.locale,
                          subject: t.subject,
                          body: t.body
        }
      end
      data << { created_at: item.created_at,
                action: item.action,
                translations: translations
      }
    end
    send_data data.to_json, type: 'application/json; charset=utf8;', disposition: "attachment; filename=emails_#{Time.now.strftime("%d_%m_%Y__%H_%M")}.json"
  end

  controller do

    def scoped_collection
      EmailTemplate.includes(:translations)
    end
  end

  index download_links: false do
    selectable_column
    translation_status
    column :action
    column :subject
    column :created_at
    column :updated_at
    actions
  end

  collection_action :import_file, method: :post do
    file = File.open(params[:file].tempfile)
    if file
      data = JSON.parse(file.read)
      ActiveRecord::Base.transaction do
        data.each do |item|
          template = EmailTemplate.new
          template.action = item['action']
          template.created_at = item['created_at']
          item['translations'].each do |t|
            I18n.locale = t['locale']
            template.subject = t['subject']
            template.body = t['body']
          end
          template.save!
        end
      end
      flash[:notice] = I18n.t('message.email.upload_success')
    else
      flash[:error] = I18n.t('message.email.upload_error')
    end
    redirect_to action: :index
  end


  filter :action, filters: [:contains]
  filter :subject, filters: [:contains]


  form do |f|
    f.inputs do
      f.input :action, required: true,
              label: I18n.t('activerecord.attributes.email_template.action'),
              input_html: { readonly: current_admin_user.has_role?(:admin) ? false : true }
      f.translated_inputs "Translated fields", switch_locale: false do |t|

        t.input :subject, label: I18n.t('activerecord.attributes.email_template.subject')
        t.input :body, as: :ckeditor,
                input_html: { ckeditor: { height: 300, language: I18n.locale } },
                label: I18n.t('activerecord.attributes.email_template.body'),
                hint: I18n.t('activerecord.attributes.email_template.template_hint')
      end
    end
    f.actions
  end

end
