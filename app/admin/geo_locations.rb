# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register GeoLocation do
  menu priority: 888, parent: 'system', url: -> { geo_locations_path(locale: I18n.locale) }
  permit_params :status, :country_code

  form do |f|
    f.inputs do
      f.input :status
      f.input :country_code, as: :country
    end
    f.actions
  end

  index do
    selectable_column
    toggle_bool_column :status
    column :country_code
    actions
  end
end
