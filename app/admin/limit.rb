# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Payments::Limit, as: 'Limit'  do
  permit_params :status, :level_id, :level_name, :currency, :min, :max, :fee, :min_with_fee

  actions :all, except: [:show]

  belongs_to :method, :parent_class => Payments::Method

  config.batch_actions = false

  config.sort_order = 'level_id'

  action_item :back_transaction do
    link_to I18n.t('transaction_action.back'), methods_path
  end

  index download_links: false do
    column :status
    column :level_name
    column :currency
    column :min_with_fee
    column :fee
    column :min
    column :max
    actions
  end

  filter :status
  filter :level_name, filters: [:contains]

  form do |f|
    f.inputs do
      f.input :status, required: true
      f.input :level_id, required: true
      f.input :level_name, required: true
      f.input :currency, collection: %w(DKK RUB EUR UAH), required: true
      f.input :min_with_fee
      f.input :fee
      f.input :min, as: :number
      f.input :max, as: :number
    end
    f.actions
  end

end
