# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register ExchangeRate, as: 'Rate' do
  permit_params :from, :to, :rate

  menu priority: 20, parent: 'finances', url: -> { rates_path(locale: I18n.locale) }

  config.batch_actions = false

  actions :index

  action_item :back, only: :show do
    link_to I18n.t('transaction_action.back'), rates_path
  end

  index download_links: false do
    column :from
    column :to
    column :rate
    column :updated_at
    actions
  end
end
