# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Banner do
  permit_params  :status,
                 :object_id,
                 :action,
                 :start_date,
                 :end_date,
                 :size,
                 :thumbnail_ru,
                 :thumbnail_en,
                 :thumbnail_pl

  menu priority: 110, parent: 'games', label: I18n.t('menu.banner'), url: -> { banners_path(locale: I18n.locale) }

  config.paginate = false
  config.filters = false

  includes :thumbnail_ru_attachment, :thumbnail_en_attachment, :thumbnail_pl_attachment

  scope :active, default: true do |scope|
    scope.active
  end

  scope :drafts do |scope|
    scope.draft
  end

  scope :all, &:all

  controller do
    before_action do
      ActiveStorage::Current.host = request.base_url
    end
  end

  action_item :index, only: :index do
    link_to I18n.t('banner_action.upload'), action: :upload
  end

  collection_action :upload do
    render 'active_admin/banner/upload'
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), banners_path(locale: I18n.locale)
  end

  collection_action :import_file, method: :post do
    file = params[:file].try(:read)
    if file
      data = JSON.parse(file)
      ActiveRecord::Base.transaction do
        data.each do |item|
          banner = Banner.new
          banner.action = item['action']
          banner.size = item['size']
          banner.object_id = item['object_id']
          banner.start_date = item['start_date']
          banner.end_date = item['end_date']
          banner.thumbnail_ru_file_name = item['thumbnail_ru_file_name']
          banner.thumbnail_en_file_name = item['thumbnail_en_file_name']
          banner.thumbnail_pl_file_name = item['thumbnail_pl_file_name']
          banner.save!
        end
      end
      flash[:notice] = I18n.t('message.banner.upload_success')
    else
      flash[:error] = I18n.t('message.banner.upload_error')
    end
    redirect_to action: :index
  end

  batch_action :export do |ids|
    data = []
    batch_action_collection.find(ids).each do |item|
      data << { object_id: item.object_id,
                action: item.action,
                size: item.size,
                end_date: item.end_date,
                position: item.position,
                thumbnail_ru_file_name: item.thumbnail_ru_file_name,
                thumbnail_en_file_name: item.thumbnail_en_file_name,
                thumbnail_pl_file_name: item.thumbnail_pl_file_name,
                category_id: item.category_id }
    end
    send_data data.to_json, type: 'application/json; charset=utf8;', disposition: "attachment; filename=banners_#{Time.now.strftime('%d_%m_%Y__%H_%M')}.json"
  end

  index download_links: false do
    # column :category
    column :thumbnail_ru do |g|
      if g.thumbnail_ru.attachment && g.thumbnail_ru.image?
        image_tag(g.thumbnail_ru.variant(resize: '75x75'))
      end
    end
    column :thumbnail_en_ do |g|
      if g.thumbnail_en.attachment && g.thumbnail_en.image?
        image_tag(g.thumbnail_en.variant(resize: '75x75'))
      end
    end
    column :thumbnail_pl do |g|
      if g.thumbnail_pl.attachment && g.thumbnail_pl.image?
        image_tag(g.thumbnail_pl.variant(resize: '75x75'))
      end
    end
    column :action
    column :size
    column :object_id
    column :start_date
    column :end_date
    actions
  end

  show do
    attributes_table do
      row :status
      row :action
      row :size
      row :object_id
        row :thumbnail_ru do |g|
        if g.thumbnail_ru.attachment && g.thumbnail_ru.image?
          image_tag(g.thumbnail_ru.service_url)
        end
      end
      row :thumbnail_en do |g|
        if g.thumbnail_en.attachment && g.thumbnail_en.image?
          image_tag(g.thumbnail_en.service_url)
        end
      end
      row :thumbnail_pl do |g|
        if g.thumbnail_pl.attachment && g.thumbnail_pl.image?
          image_tag(g.thumbnail_pl.service_url)
        end
      end

    end
  end

  form do |f|
    f.inputs 'Banners' do

      f.input :status, required: true
      f.input :action, required: true
      f.input :size, required: true
      f.input :object_id, hint: I18n.t('activerecord.attributes.banner.hint')
      f.input :start_date, as: :date_time_picker
      f.input :end_date, as: :date_time_picker
      f.li "<label class='label'>#{I18n.t('activerecord.attributes.banner.filename')}</label><span>#{(f.object.thumbnail_ru.attachment && f.object.thumbnail_ru.image?) ? f.object.thumbnail_ru.filename : '-'}</span>".html_safe
      f.input :thumbnail_ru, as: :file, required: true, hint: image_tag((f.object.thumbnail_ru.attachment && f.object.thumbnail_ru.image?) ? f.object.thumbnail_ru.service_url : '')

      f.li "<label class='label'>#{I18n.t('activerecord.attributes.banner.filename')}</label><span>#{(f.object.thumbnail_en.attachment && f.object.thumbnail_en.image?) ? f.object.thumbnail_en.filename : '-'}</span>".html_safe
      f.input :thumbnail_en, as: :file, required: true, hint: image_tag((f.object.thumbnail_en.attachment && f.object.thumbnail_en.image?) ? f.object.thumbnail_en.service_url : '')

      f.li "<label class='label'>#{I18n.t('activerecord.attributes.banner.filename')}</label><span>#{(f.object.thumbnail_pl.attachment && f.object.thumbnail_pl.image?) ? f.object.thumbnail_pl.filename : '-'}</span>".html_safe
      f.input :thumbnail_pl, as: :file, required: true, hint: image_tag((f.object.thumbnail_pl.attachment && f.object.thumbnail_pl.image?) ? f.object.thumbnail_pl.service_url : '')
    end
    f.actions
  end
end
