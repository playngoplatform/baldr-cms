# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Bonus do
  actions :index, :create, :new, :edit, :update, :destroy

  permit_params :status,
                :start_date,
                :end_date,
                :position,
                :thumbnail,
                :mode,
                :code,
                :fixed_amount,
                :page_id,
                :registration,
                translations_attributes: %i[id
                                            locale
                                            title
                                            description
                                            content
                                            meta_title
                                            meta_description
                                            promo]

  menu priority: 2, parent: 'content', url: -> { bonuses_path(locale: I18n.locale) }

  config.filters = false
  config.sort_order = 'position_asc'

  scope :active, default: true do |scope|
    scope.published
         .where('(start_date <= ? AND end_date >= ?) OR (start_date IS NULL AND end_date IS NULL)  OR (start_date IS NULL AND end_date >= ?) OR (start_date <= ? AND end_date IS NULL)', Date.today, Date.today, Date.today, Date.today)
  end

  scope :drafts, &:draft

  scope :archived do |scope|
    scope.published.where.not('(start_date <= ? AND end_date >= ?) OR (start_date IS NULL AND end_date IS NULL)  OR (start_date IS NULL AND end_date >= ?)', Date.today, Date.today, Date.today)
  end

  sidebar :actions, only: [:show] do
    ul do
      li link_to I18n.t('custom_actions.limits'), bonus_bonus_limits_path(resource)
    end
  end

  action_item :index do
    link_to I18n.t('bonus_action.upload'), action: :upload
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), bonuses_path
  end

  collection_action :upload do
    render 'active_admin/bonus/upload'
  end

  controller do
    before_action do
      ActiveStorage::Current.host = request.base_url
    end
  end

  collection_action :import_file, method: :post do
    file = params[:file].try(:read)
    if file
      data = JSON.parse(file)
      ActiveRecord::Base.transaction do
        data.each do |item|
          bonus = Bonus.new
          bonus.status = :draft
          bonus.page_id = item['page_id']
          bonus.start_date = item['start_date']
          bonus.end_date = item['end_date']
          bonus.position = item['position']
          bonus.code = item['code']
          bonus.mode = item['mode']
          bonus.registration = item['registration']
          bonus.fixed_amount = item['fixed_amount']
          item['translations'].each do |t|
            I18n.locale = t['locale']
            bonus.title = t['title']
            bonus.description = t['description']
            bonus.content = t['content']
            bonus.meta_title = t['meta_title']
            bonus.meta_description = t['meta_description']
            bonus.promo = t['promo']
          end
          bonus.save!
          item['limits'].each do |l|
            bonus.limits.create!(l)
          end
          Dir.mktmpdir do |dir|
            if item['thumbnail_filename']
              filename = "#{dir}/#{item['thumbnail_filename']}"
              File.open(filename, 'wb') do |f|
                f.write(Base64.decode64(item['thumbnail']))
              end
            else
              filename = "#{dir}/#{item['page_id']}.png"
              File.open(filename, 'wb') do |f|
                open("https://via.placeholder.com/400x200?text=#{item['page_id'].gsub(/[^0-9A-Za-z]/, '')}") do |url|
                  f.write(url.read)
                end
              end
            end
            bonus.thumbnail.attach(io: File.open(filename), filename: filename)
          end
        end
      end
      flash[:notice] = I18n.t('message.bonus.upload_success')
    else
      flash[:error] = I18n.t('message.bonus.upload_error')
    end
    redirect_to action: :index
  end

  batch_action :export do |ids|
    data = []
    batch_action_collection.find(ids).each do |item|
      translations = []
      item.translations.each do |t|
        translations << { locale: t.locale,
                          title: t.title,
                          description: t.description,
                          content: t.content,
                          meta_title: t.meta_title,
                          meta_description: t.meta_description,
                          promo: t.promo
        }
      end
      data << { start_date: item.start_date,
                end_date: item.end_date,
                position: item.position,
                thumbnail_filename: item.thumbnail.filename,
                thumbnail_content_type: item.thumbnail.content_type,
                thumbnail: Base64.encode64(item.thumbnail.download),
                limits: item.limits.map(&:to_hash),
                code: item.code,
                mode: item.mode,
                fixed_amount: item.fixed_amount,
                page_id: item.page_id,
                registration: item.registration,
                translations: translations }
    end
    send_data data.to_json, type: 'application/json; charset=utf8;', disposition: "attachment; filename=bonuses_#{Time.now.strftime('%d_%m_%Y__%H_%M')}.json"
  end

  index download_links: false do
    selectable_column
    translation_status
    column :status do |m|
      div class: "status_tag #{m.status}" do
        translate("bonus_status.#{m.status}")
      end
    end
    column :page_id
    column :title
    column :mode do |m|
      translate("bonus_mode.#{m.mode}")
    end
    column :code
    column :position
    column :start_date
    column :end_date
    actions do |m|
      link_to translate('custom_actions.limits'), bonus_bonus_limits_path(m)
    end
  end

  form do |f|
    f.inputs do
      f.input :status
      f.input :start_date, as: :date_time_picker
      f.input :end_date, as: :date_time_picker
      f.input :page_id
      f.input :mode, as: :select,
                     collection: [[I18n.t('bonus_mode.deposit1'), 'deposit1'],
                                  [I18n.t('bonus_mode.deposit2'), 'deposit2'],
                                  [I18n.t('bonus_mode.cashback'), 'cashback']]
      f.input :registration
      f.input :code
      f.input :fixed_amount
      f.input :position
      f.li "<label class='label'>Filename</label><span>#{(f.object.thumbnail.attachment && f.object.thumbnail.image?) ? f.object.thumbnail.filename : '-'}</span>".html_safe
      f.input :thumbnail, as: :file, required: true, hint: image_tag((f.object.thumbnail.attachment && f.object.thumbnail.image?) ? f.object.thumbnail.service_url : '')
      f.translated_inputs 'Translated fields', switch_locale: false do |t|
        t.input :title, required: true,
                        label: I18n.t('activerecord.attributes.bonus.title'),
                        hint: I18n.t('bonuses.template_hint')
        t.input :promo, required: true,
                label: I18n.t('activerecord.attributes.bonus.promo'),
                hint: I18n.t('bonuses.promo_hint')
        t.input :description, required: true, label: I18n.t('activerecord.attributes.bonus.description'),
                              hint: I18n.t('bonuses.template_hint')
        t.input :content, as: :ckeditor,
                          input_html: { ckeditor: { height: 300, language: I18n.locale } },
                          label: I18n.t('activerecord.attributes.bonus.content'),
                          hint: I18n.t('bonuses.template_hint')
        t.input :meta_title, label: I18n.t('activerecord.attributes.bonus.meta_title')
        t.input :meta_description, label: I18n.t('activerecord.attributes.bonus.meta_description')
      end
    end
    f.actions
  end
end
