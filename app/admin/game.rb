# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Game do
  permit_params :name, :category_id, :provider_id, :game_id, :is_mobile, :is_desktop, :jackpot, :popular,
                :enabled, :locked, :width, :height, :thumbnail, :external_id, :old,
                translations_attributes: %i[id locale image_alt meta_title meta_description]

  menu priority: 3, parent: 'games', label: I18n.t('menu.game_list'), url: -> { games_path(locale: I18n.locale) }

  config.sort_order = 'created_at_desc'
  config.paginate = false

  includes :translations, :category, :provider, :thumbnail_attachment

  # belongs_to :game_category, optional: true

  controller do
    before_action do
      ActiveStorage::Current.host = request.base_url
    end
  end

  scope :all, default: true do |scope|
    scope.all.includes(:translations)
  end

  scope :playngo do |scope|
    scope.where(provider_id: 1)
  end

  scope :locked do |scope|
    scope.where(locked: true)
  end

  scope :disabled do |scope|
    scope.where(enabled: false)
  end

  scope :new do |scope|
    scope.where(id: Game.recent).order(:provider_id, created_at: :desc)
  end

  scope :popular do |scope|
    scope.where(enabled: true).popular
  end

  action_item :index, only: :index do
    link_to I18n.t('game_action.upload'), action: :upload
  end

  batch_action :disable do |ids|
    batch_action_collection.find(ids).each do |item|
      item.enabled = false
      item.save
    end
    redirect_to collection_path
  end

  batch_action :enable do |ids|
    batch_action_collection.find(ids).each do |item|
      item.enabled = true
      item.save
    end
    redirect_to collection_path
  end

  batch_action :lock do |ids|
    batch_action_collection.find(ids).each do |item|
      item.locked = true
      item.save
    end
    redirect_to collection_path
  end

  batch_action :unlock do |ids|
    batch_action_collection.find(ids).each do |item|
      item.locked = false
      item.save
    end
    redirect_to collection_path
  end

  batch_action :make_new do |ids|
    batch_action_collection.find(ids).each do |item|
      item.old = false
      item.save
    end
    redirect_to collection_path
  end

  batch_action :make_old do |ids|
    batch_action_collection.find(ids).each do |item|
      item.old = true
      item.save
    end
    redirect_to collection_path
  end

  collection_action :upload do
    render 'active_admin/game/upload'
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), games_path(locale: I18n.locale)
  end

  collection_action :import_file, method: :post do
    file = params[:file].try(:read)
    if file
      data = JSON.parse(file)
      ActiveRecord::Base.transaction do
        data.each do |item|
          category_id = item['thumbnail_file_name'] ? GameCategory.find_by(code: item['category'])&.id : item['category']
          provider_id = item['provider'] ? GameProvider.find_by(code: item['provider'])&.id : item['provider_id']

          game = Game.new
          game.enabled = false
          game.locked = true
          game.name = item['name'].gsub(/[^0-9A-Za-z ]/, '')
          game.popular = item['popular']
          game.category_id = category_id
          game.provider_id = provider_id
          game.game_id = item['game_id'].gsub(/[^0-9A-Za-z -]/, '')
          game.is_desktop = item['is_desktop']
          game.is_mobile = item['is_mobile']
          game.created_at = item['created_at']
          game.height = item['height'] || 768
          game.width = item['width'] || 1024
          item['translations'].each do |t|
            I18n.locale = t['locale']
            game.image_alt = t['image_alt'] || item['name']
            game.meta_title = t['meta_title'] || item['name']
            game.meta_description = t['meta_description'] || item['name']
          end
          game.save!
          Dir.mktmpdir do |dir|
            if item['thumbnail_filename']
              filename = "#{dir}/#{item['thumbnail_filename']}"
              File.open(filename, 'wb') do |f|
                f.write(Base64.decode64(item['thumbnail']))
              end
            else
              filename = "#{dir}/#{item['name']}.png"
              File.open(filename, 'wb') do |f|
                open("https://via.placeholder.com/186x135?text=#{item['name'].gsub(/[^0-9A-Za-z]/, '')}") do |url|
                  f.write(url.read)
                end
              end
            end
            game.thumbnail.attach(io: File.open(filename), filename: filename)
          end
        end
      end
      flash[:notice] = I18n.t('message.game.upload_success')
    else
      flash[:error] = I18n.t('message.game.upload_error')
    end
    redirect_to action: :index
  end

  batch_action :export do |ids|
    data = []
    batch_action_collection.find(ids).each do |item|
      translations = []
      item.translations.each do |t|
        translations << { locale: t.locale,
                          image_alt: t.image_alt,
                          meta_title: t.meta_title,
                          meta_description: t.meta_description }
      end
      data << { name: item.name,
                # jackpot: item.jackpot,
                popular: item.popular,
                category: item.category_id,
                provider_id: item.provider_id,
                game_id: item.game_id,
                is_desktop: item.is_desktop,
                is_mobile: item.is_mobile,
                thumbnail_filename: item.thumbnail.filename,
                thumbnail_content_type: item.thumbnail.content_type,
                thumbnail: Base64.encode64(item.thumbnail.download),
                created_at: item.created_at,
                width: item.width,
                height: item.height,
                translations: translations }
    end
    send_data data.to_json, type: 'application/json; charset=utf8;', disposition: "attachment; filename=games_#{Time.now.strftime('%d_%m_%Y__%H_%M')}.json"
  end

  index download_links: false do
    selectable_column
    translation_status
    if  %w(all new disabled locked popular).include? params['scope']
      column :provider do |g|
        g.provider.name
      end
    end
    column :category_id do |game|
      game.category.name
    end
    toggle_bool_column :enabled
    toggle_bool_column :locked
    toggle_bool_column :is_desktop
    toggle_bool_column :is_mobile
    column I18n.t('activerecord.attributes.game.image') do |g|
      if g.thumbnail.attachment && g.thumbnail.image?
        image_tag(g.thumbnail.variant(resize: '75x75'))
      end
    end
    column :name
    toggle_bool_column :old
    toggle_bool_column :popular
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :provider_id do |game|
        game.provider.name
      end
      row :enabled
      row :locked
      row :category_id do |game|
        game.category.name
      end
      row :name
      row :game_id
      row :is_desktop
      row :is_mobile
      row :old
      row :popular
      # row :jackpot
      row :external_id
      row :width
      row :height
      row :params
      row :created_at
      row :updated_at
      row :thumbnail do |g|
        if g.thumbnail.attachment && g.thumbnail.image?
          image_tag(g.thumbnail.service_url)
        end
      end
    end
  end

  filter :enabled
  filter :locked
  filter :category
  filter :name, filters: [:contains]
  filter :popular

  form do |f|
    f.inputs 'Game Details' do
      f.input :enabled, required: true
      f.input :locked, required: true
      f.input :category, required: true
      f.input :provider, required: true
      f.input :name, required: true
      f.input :game_id, required: true
      f.input :is_desktop, required: false
      f.input :is_mobile, required: false
      f.input :width, required: true
      f.input :height, required: true
      f.input :old
      f.input :popular
      f.li "<label class='label'>Filename</label><span>#{(f.object.thumbnail.attachment && f.object.thumbnail.image?) ? f.object.thumbnail.filename : '-'}</span>".html_safe
      f.input :thumbnail, as: :file, required: true, hint: image_tag((f.object.thumbnail.attachment && f.object.thumbnail.image?) ? f.object.thumbnail.service_url : '')
      f.translated_inputs 'Translated fields', switch_locale: false do |t|
        t.input :image_alt
        t.input :meta_title
        t.input :meta_description
      end
    end
    f.actions
  end
end
