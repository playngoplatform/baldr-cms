# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Locale do
  actions :index, :edit, :update

  permit_params  translations_attributes: %i[id locale content]

  menu priority: 6, parent: 'system', url: -> { locales_path(locale: I18n.locale) }

  config.sort_order = 'id_asc'

  config.filters = false

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), locales_path(locale: I18n.locale)
  end

  action_item :index do
    link_to I18n.t('locale_action.upload'), action: :upload
  end

  collection_action :upload do
    render 'active_admin/locale/upload'
  end

  collection_action :import_file, method: :post do
    file = params[:file].try(:read)
    if file
      data = JSON.parse(file)
      ActiveRecord::Base.transaction do
        data.each do |item|
          locale = Locale.find_or_initialize_by(mode: item['mode'])
          item['translations'].each do |t|
            I18n.locale = t['locale']
            locale.content = t['content']
          end
          locale.save!
        end
      end
    else
      flash[:error] = I18n.t('message.locale.upload_error')
    end
    redirect_to action: :index
  end


  batch_action :export do |ids|
    data = []
    batch_action_collection.find(ids).each do |item|
      translations = []
      item.translations.each do |t|
        translations << { locale: t.locale,
                          content: t.content
        }
      end
      data << { mode: item.mode,
                translations: translations }
    end
    send_data data.to_json, type: 'application/json; charset=utf8;', disposition: "attachment; filename=locales_#{Time.now.strftime('%d_%m_%Y__%H_%M')}.json"
  end

  index download_links: false do
    selectable_column
    translation_status
    column :mode
    actions
  end

  show do
    attributes_table do
    end
  end

  form do |f|
    f.inputs do
      f.translated_inputs 'Translated fields', switch_locale: false do |t|
        t.input :content,
                as: :text,
                input_html: { class: 'jsoneditor-target', rows: 50, cols: 20 }
      end
    end
    panel t('activerecord.attributes.locale.hint') do
      render partial: 'active_admin/locale/urls',
             locals: { locales: resource.translations.map(&:locale),
                       api_url: API_URL
             }
    end
    f.actions
  end
end
