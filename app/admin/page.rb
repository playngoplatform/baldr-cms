# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Page do
  actions :index, :new, :create, :edit, :update, :destroy

  permit_params :name, :category, translations_attributes: %i[id locale title content meta_title meta_description]

  menu priority: 2, parent: 'content', url: -> { pages_path(locale: I18n.locale) }

  # sidebar :versions, partial: 'layouts/version', only: :edit

  config.sort_order = 'name'

  action_item :index do
    link_to I18n.t('page_action.upload'), action: :upload
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), pages_path(locale: I18n.locale)
  end

  collection_action :upload do
    render 'active_admin/page/upload'
  end

  scope :all, default: true, &:all

  scope :static, &:static

  scope :bonus do |scope|
    scope.where(category: %i[bonus_header bonus_footer])
  end

  controller do
    def scoped_collection
      Page.includes(:translations)
    end
  end

  # controller do
  #   def edit
  #     @page = Backend::Page.find(params[:id])
  #     @versions = PaperTrail::Version.where(item_id: @page.translations.ids)
  #     if params[:version]
  #       data = @versions[params[:version].to_i].reify
  #       @page.title = 'ЛАЛАЛА'
  #
  #       # I18n.locale = data.locale
  #       # @page.category = data.category if data.defined? :category
  #       # @page.name = data.name if data.defined? :name
  #       # @page.title = data.title if data.defined? :title
  #       # @page.content = data.content if data.defined? :content
  #       # @page.meta_description = data.meta_description if data.defined? :meta_description
  #       # @page.meta_title = data.meta_title if data.defined? :meta_title
  #
  #     end
  #
  #     edit! #it seems to need this
  #   end
  # end

  batch_action :export do |ids|
    data = []
    batch_action_collection.find(ids).each do |item|
      translations = []
      item.translations.each do |t|
        translations << { locale: t.locale,
                          title: t.title,
                          content: t.content,
                          meta_title: t.meta_title,
                          meta_description: t.meta_description }
      end
      data << { category: item.category,
                created_at: item.created_at,
                name: item.name,
                translations: translations }
    end
    send_data data.to_json, type: 'application/json; charset=utf8;', disposition: "attachment; filename=pages_#{Time.now.strftime('%d_%m_%Y__%H_%M')}.json"
  end

  collection_action :import_file, method: :post do
    file = params[:file].try(:read)
    if file
      data = JSON.parse(file)
      ActiveRecord::Base.transaction do
        data.each do |item|
          page = Page.new
          page.category = item['category']
          page.name = item['name']
          page.created_at = item['created_at']
          item['translations'].each do |t|
            I18n.locale = t['locale']
            page.title = t['title']
            page.content = t['content']
            page.meta_title = t['meta_title']
            page.meta_description = t['meta_description']
          end
          page.save!
        end
      end
      flash[:notice] = I18n.t('message.page.upload_success')
    else
      flash[:error] = I18n.t('message.page.upload_error')
    end
    redirect_to action: :index
  end

  index download_links: false do
    selectable_column
    translation_status
    column :name
    column :category if params['scope'] == 'all'
    column :title
    column :created_at
    column :updated_at
    actions
  end

  filter :name, filters: [:contains]
  filter :title, filters: [:contains]

  form do |f|
    f.inputs do
      if current_admin_user.has_role?(:admin)
        f.input :category, required: true, label: I18n.t('activerecord.attributes.backend/page.category')
      else
        f.input :category,
                as: :string,
                label: I18n.t('activerecord.attributes.backend/page.category'),
                input_html: { readonly: true }
      end

      f.input :name, required: true,
                     label: I18n.t('activerecord.attributes.backend/page.name'),
                     input_html: { readonly: current_admin_user.has_role?(:admin) ? false : true }
      f.translated_inputs 'Translated fields', switch_locale: false do |t|
        t.input :title, label: I18n.t('activerecord.attributes.backend/page.title')
        t.input :content, as: :ckeditor,
                          input_html: { ckeditor: { height: 300, language: I18n.locale } },
                          label: I18n.t('activerecord.attributes.backend/page.content'),
                hint: I18n.t('activerecord.attributes.page.template_hint')

        t.input :meta_title, label: I18n.t('activerecord.attributes.backend/page.meta_title')
        t.input :meta_description, label: I18n.t('activerecord.attributes.backend/page.meta_description')
      end
    end
    f.actions
  end
end
