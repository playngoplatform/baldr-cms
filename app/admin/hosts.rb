# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Host do
  permit_params :name, :enabled, :description, :ancestry

  menu priority: 888, parent: 'system', url: -> { hosts_path(locale: I18n.locale) }

  config.paginate = false
  config.batch_actions = false
  config.filters = false
  config.sort_order = 'created_at_asc'

  index download_links: false do
    toggle_bool_column :enabled
    column :name
    column :ancestry do |h|
      h.parent&.name
    end
    column :description
    column :players do |h|
      h.players.count
    end
    column :created_at
    actions
  end

  form do |f|
    f.inputs  do
      f.input :enabled
      f.input :name
      f.input :description
      f.input :ancestry, collection: Host.pluck(:name, :id), required: true
    end
    f.actions
  end
end
