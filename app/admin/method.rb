# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register Payments::Method, as: 'Method' do
  permit_params :status,
                :paymentiq_name,
                :position,
                :thumbnail,
                :fee,
                :request_account,
                translations_attributes: %i[
                  id
                  name
                  deposit_first
                  deposit_second
                  description
                  withdrawal_to_one
                  withdrawal_to_many
                  account_required
                ]

  menu priority: 4, parent: 'finances', url: -> { methods_path(locale: I18n.locale) }

  config.sort_order = 'position_asc'

  config.batch_actions = false

  config.filters = false

  actions :all, except: %i[new create destroy]

  scope :deposits, default: true do |scope|
    scope.deposit.where(status: true)
  end

  scope :withdrawals do |scope|
    scope.withdrawal.where(status: true)
  end

  scope :disabled do |scope|
    scope.where(status: false)
  end

  controller do
    before_action do
      ActiveStorage::Current.host = request.base_url
    end
    # def update
    #   return super unless params[:game]
    #   attrs = params.permit(game: [:id, :is_desktop, :is_mobile, :popular, :enabled, :locked])
    #   Game.update(permitted_params[:id], attrs[:game])
    # end

    def scoped_collection
      Payments::Method.includes(:translations, thumbnail_attachment: :blob)
    end
  end

  sidebar :actions, only: [:show] do
    ul do
      li link_to I18n.t('custom_actions.limits'), method_limits_path(resource)
    end
  end

  action_item :back_transaction, only: :show do
    link_to I18n.t('transaction_action.back'), methods_path
  end

  index download_links: false do
    translation_status

    toggle_bool_column :status

    column :name

    column :paymentiq_name

    column :mode do |m|
      translate("mode.#{m.mode}")
    end

    column :fee do |m|
      "#{m.fee * 100}%"
    end

    column :position

    column :thumbnail do |m|
      if m.thumbnail.attachment && m.thumbnail.image?
        image_tag(m.thumbnail.variant(resize: '116x48'))
      end
    end

    actions do |m|
      link_to translate('custom_actions.limits'), method_limits_path(m)
    end
  end

  show do
    attributes_table do
      row :id
      row :status do |m|
        translate("status.#{m.status}")
      end
      row :name

      row :paymentiq_name

      row :fee do |m|
        "#{m.fee * 100}%"
      end
      row :mode do |m|
        translate("mode.#{m.mode}")
      end

      row :position

      row :request_account

      row :thumbnail do |m|
        if m.thumbnail.attachment && m.thumbnail.image?
          image_tag(m.thumbnail.service_url)
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :status, required: true
      f.input :paymentiq_name
      f.input :fee, as: :number, hint: '0.1 = 10%'
      f.input :position
      f.input :request_account if f.object.deposit?
      f.input :thumbnail, as: :file, required: true, hint: image_tag(f.object.thumbnail.attachment && f.object.thumbnail.image? ? f.object.thumbnail.service_url : '')
      f.inputs 'Translated fields' do
        f.translated_inputs '', switch_locale: false do |t|
          t.input :name
          if f.object.request_account
            t.input :account_required, label: I18n.t('activerecord.attributes.payments/method.request_account')
          end
          if f.object.description.present?
            t.input :description,
                    label: I18n.t('activerecord.attributes.payments/method.description'),
                    as: :ckeditor,
                    input_html: { ckeditor: { height: 200, language: I18n.locale } }
          end
          if f.object.deposit?
            if f.object.deposit_first.present?
              t.input :deposit_first,
                      label: I18n.t('activerecord.attributes.payments/method.deposit_first'),
                      as: :ckeditor,
                      input_html: { ckeditor: { height: 200, language: I18n.locale } }
            end
            if f.object.deposit_second.present?
              t.input :deposit_second,
                      label: I18n.t('activerecord.attributes.payments/method.deposit_second'),
                      as: :ckeditor,
                      input_html: { ckeditor: { height: 200, language: I18n.locale } }
            end

          end
          if f.object.withdrawal?
            if f.object.withdrawal_to_one.present?
              t.input :withdrawal_to_one,
                      label: I18n.t('activerecord.attributes.payments/method.withdrawal_to_one'),
                      as: :ckeditor,
                      input_html: { ckeditor: { height: 200, language: I18n.locale } }
            end
            if f.object.withdrawal_to_many.present?
              t.input :withdrawal_to_many,
                      label: I18n.t('activerecord.attributes.payments/method.withdrawal_to_many'),
                      as: :ckeditor,
                      input_html: { ckeditor: { height: 200, language: I18n.locale } }
            end
          end
        end
      end
    end
    f.actions
  end
end
