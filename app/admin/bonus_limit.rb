# Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
ActiveAdmin.register BonusLimit  do
  permit_params :currency, :min, :max

  actions :all, except: %i[show destroy]

  belongs_to :bonus

  config.batch_actions = false

  action_item :back_transaction do
    link_to I18n.t('transaction_action.back'), bonuses_path
  end

  index download_links: false do
    selectable_column
    column :currency
    column :min
    column :max
    actions
  end

  form do |f|
    f.inputs do
      f.input :currency, collection: %w[DKK RUB EUR UAH], required: true
      f.input :min, as: :number
      f.input :max, as: :number
    end
    f.actions
  end
end
