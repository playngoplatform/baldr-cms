Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Plugins.
  PluginConfig = Struct.new(:cashier_module, :nets_module, :player_module)
  config.plugins = PluginConfig.new
  config.plugins.cashier_module = nil
  config.plugins.player_module = nil

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  config.action_controller.perform_caching = false

  # config.cache_store = :memory_store, { size: 64.megabytes }
  redis_sentinels = []
  if ENV['REDIS_HOSTNAMES']
    hostnames = ENV['REDIS_HOSTNAMES'].split(',')
    redis_sentinels = hostnames.map { |host| { host: host, port: 26379 } }
  end
  redis_url = "redis://" + ENV['REDIS_MASTER_GROUPNAME']
  redis_connection = Redis.new(url: redis_url, sentinels: redis_sentinels, role: :master, reconnect_attempts: 1)
  config.cache_store = :redis_cache_store, { namespace: ENV['BALDR_SESSION_KEY'], redis: redis_connection, expires_in: 1.day }
  config.session_store :cache_store, key: ENV['BALDR_SESSION_KEY']

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :production

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  # config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  # ActiveAdmin
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
  Rails.application.routes.default_url_options[:host] = Setting['api_url']

  Rails.logger = Timber::Logger.new(STDOUT)
end
