ActiveSupport::Notifications.subscribe('grape_logging') do |_name, _starts, _ends, _notification_id, payload|
  Rails.logger.debug(payload.to_json)
end