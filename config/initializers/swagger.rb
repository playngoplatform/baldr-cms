GrapeSwaggerRails.options.url = '/api/swagger_doc.json'
GrapeSwaggerRails.options.app_url = Setting.api_url

GrapeSwaggerRails.options.app_name = 'CMS'

GrapeSwaggerRails.options.hide_url_input = false
GrapeSwaggerRails.options.hide_api_key_input = true
