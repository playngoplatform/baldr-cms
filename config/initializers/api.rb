if Rails.env.development? && ENV["NGROK"]
  uri = URI.parse("http://ngrok:4040/api/tunnels/command_line")
  response = Net::HTTP.get_response(uri)
  result = JSON.parse(response.body)
  API_URL = result["public_url"]
  Rails.logger.info "**** NGROK: #{API_URL} ****"
else
  API_URL = Setting.api_url
end