if Rails.env.development?
  REDIS = Redis::Namespace.new("casino2-#{Rails.env}", redis: Redis.new(host: 'redis'))
else
  REDIS = Redis::Namespace.new("casino2-#{Rails.env}", redis: Redis.new)
end

Redis.current = REDIS