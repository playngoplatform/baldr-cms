Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  mount API::Base => '/'
  mount GrapeSwaggerRails::Engine => '/swagger'
  mount_griddler('/api/email/incoming')
  mount Ckeditor::Engine => '/ckeditor'

end
