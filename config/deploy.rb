lock '3.11.0'

set :application, 'api'
set :repo_url, 'git@github.com:casino-2go/casino-2.0.git'

set :log_level, :info
set :rbenv_ruby, '2.6.4'
set :assets_roles, [:app]
set :keep_assets, 2

append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', '.bundle', 'public/system', 'public/uploads'
append :linked_files, 'config/database.yml', 'config/storage.yml'

# cron
set :whenever_roles, -> { :app }

# Default value for keep_releases is 5
set :keep_releases, 3

# Default value for :pty is false
# set :pty, false

# set :config_files, %w{config/database.yml config/secrets.yml config/web_services.yml}

set :passenger_restart_with_touch, false
set :passenger_restart_with_sudo, true

namespace :deploy do
  # before 'deploy:check:linked_files', 'config:push'
  # after 'deploy:published', 'deploy:restart'
  after 'deploy:published', 'passenger:restart'

  after 'deploy:finished', 'translations'

  desc 'Update translations'
  task translations: [:set_rails_env] do
    on release_roles([:app]) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'seeds:locales'
        end
      end
    end
  end
  
end

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5
